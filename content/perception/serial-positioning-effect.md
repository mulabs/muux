+++
title = "Serial Positioning Effect"
template = "patterns.html"
[extra]
type = "comprehension"
icontype = "fas fa-dumbbell"
+++

### Problem summary

We have a tendency to recall the first and the last items in a series best

### Solution

How do you order list items?

Present important items at the beginning and end of a list to maximize recall and the likelihood that users will remember those items when the time comes to make a decision. Initial items are remembered more efficiently than items later in a list. Items at the end of a list are recalled more easily immediately after their presentation.

You will want to:

* Present important items at the beginning and at the end of a list to maximize recall – the probability that people will remember those items.
* If you want people to choose one item over another, present it in the end of a list if the decision is to be made immediately after its presentation. We tend to favor the last candidate presented to us.
* If the decision is to be made at a later time, present your preferred item at the beginning of the list.
* Focus on only showing information relevant to the current task in your user interface to minimize the load you put on your users’ cognitive capacity. Provide tools to guide your user toward their goals, helping them be more efficient and more accurate in their tasks.
* Add cues to things previously encountered in order to inititate recognition of the action and recall its meaning. Cues are most often graphical, but can also include sounds.
* Limit the amount of recall required to retain relevant information to complete a task or simply to retrieve information. Human attention is limited and we are only capable of maintaining up to around five items in our short-term memory.

### Rationale

When recalling items from a list, items at the beginning and the end are better recalled than the items in the middle.

Our ability to better recall items at the beginning of a list is called the primacy effect, whereas our ability to recall items at the end of a list is called a recency effect.

*Primacy effect:* Initial items on a list are stored in long-term memory more efficiently than items later in the list. The longer the time items are presented, the stronger the primacy effect is, as people then have more time to store the initial items in long-term memory.
*Recency effect:* The last few items are still in working memory and are readily available. The strength of the recency effect is unaffected by the rate of presentation, but is greatly affected by the passage of time and presentation of additional information. The recency effect further disappears when people think about other matters for thirty seconds after the last item in the list is presented.

### Discussion

Presenting long lists of information to users, significantly strains our limited resources and restricted memory systems – especially short-term memory, where only three or four items or chunks of information can be maintained at one time. Our ability to recall previously presented items is also severely impacted by events between initial processing and later recall.