+++
title = "Chunking"
template = "patterns.html"
[extra]
type = "comprehension"
icontype = "fas fa-drumstick-bite"
+++

### Problem summary

Information grouped into familiar, manageable units is more easily processed and remembered

### Usage

* Use when you want to guide users into perceiving information more easily
* Use when you have an excessive amount of information that is not readily understood in its full form
* Use to make it easier for users to recall or process information
* Do not use when the information must be searched, scanned, or analyzed.
* Do not use chunking as an argument for improved simplicity, legibility, or uncluttered page design.

### Solution

Group information into a limited number of units or chunks, so that information is easier to process and recall.

Make information easier to understand and remember by breaking it down into smaller groups, or chunks. Chunking helps accommodate our limited capacity for processing information and storage in short-term memory.

Chunk larger piles of information into smaller chunks to make it easier for the user to comprehend and get an overview. Deciding on how information is chunked together can help form the user’s perception of what is important and worthwhile to remember.

Chunking text into paragraphs with headers allow scanning for a particular subject. The choice of headers and what should go into which paragraph help form the reader’s perceived meaning of the text.

#### How far should I chunk – and what?
Every kind of information can be chunked. You should however restrict yourself from grouping in too many chunks. There has been some discussion around what the maximum number of chunks that can be processed by short-term memory. Recent research suggests that the magic limit is four plus/minus one whereas older literature suggests up to 7 plus/minus two.

##### So always 4 to 5 menu items?
Do not use chunking to improve simplicity or to unclutter the design of a web page. Chunking is only to be used as an argument to ease the way we process information. Design decisions about the number of menu items, power points bullets, or radio buttons can’t be justified through chunking but likely through other arguments.

##### When to chunk…
Arguing for using the principle of chunking in design should only regard “the limits on our capacity for processing information”. In other words, chunking is ideal when specific information must be memorised for later use or when an interface must compete against other stimuli for the attention of the working memory of the end user.

* *Group information into smaller chunks* Ease the cognitive load of complex tasks by grouping information into related features. By grouping items by similarity, you can far surpass the limits of storing single items. If 5 is the limit, then chunking into 5 groups of 5 dramatically expands the user’s capacity.
* *Make scanning easier* Presenting content in chunks makes scanning easier for users and in turn improves their ability to comprehend and remember it. Create meaningful and visually distinct groups of content that make sense in the context of the larger whole. Separate headings from paragraph text.
* *Auto-format inputs* Although chunking improves scannability, it can make typing more difficult. To circumvent this, consider letting input fields automatically chunk the input users are typing. Typical examples are for credit card expiration dates and phone numbers.

### Rationale

A chunk is a unit of information in short-term memory – a string of letters, a word, or a series or numbers. By chunking information into small bits we seek to accommodate the limits of our short-term memory.

The goal of chunking is to aid processing of information. Chunking helps the process by breaking longer strings of information into bit-size chunks that are easier to remember and grasp – especially when the memory is faced with competing stimuli.