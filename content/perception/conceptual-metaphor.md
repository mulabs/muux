+++
title = "Conceptual Metaphor"
template = "patterns.html"
[extra]
type = "comprehension"
icontype = "fas fa-atom"
+++

### Problem summary

We understand a new idea or concept by linking it to another

### Usage

* Use to make the unfamiliar familiar
* Use to explain abstract or completely new concepts
* Use to make what action to take seem obvious.

### Solution

Explain difficult concepts by making analogies.

* *Draw favorable analogies* Help users understand a concept and influence how it iis perceived by drawing a literal or implied analogy.
* *Make the unfamiliar familiar* Using conceptual metaphors to explain unfamiliar concepts with familiar ones, eases understanding something that might otherwise be too complex or too abstract for our mind to grasp.
* *Make the abstract concrete* Conceptual metaphors make abstract concepts more concrete and therefore bring them within our reach. Draw visual, functional, and structural similarities to make the point.
* *Make the obvious emotional* Link your product with a familiar concept to transfer its emotions to your product.

### Rationale

* Letting users grasp an idea by association can help ease understanding, influence how it is perceived, and adjust how we think and act.

* Explain difficult concepts by making analogies. Letting users grasp an idea by association can help ease understanding, influence how it is perceived, and adjust how we think and act. Although time and culture sensitive, conceptual metaphors have the power to create a sense of familiarity, trigger emotions, draw attention, and motivate action.