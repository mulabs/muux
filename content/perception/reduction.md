+++
title = "Reduction"
template = "patterns.html"
[extra]
type = "attention"
icontype = "fas fa-power-off"
+++

### Problem summary

Reduce complex behaviour to simple tasks, increasing the benefit/cost ratio and in turn influencing users to perform

### Usage

* Use when performing regular tasks of your system are complex and hard to comprehend in their entirety.
* Use to stand out from competition, which have not succeeded in making complex tasks simple.
* Use when users do essentially not care about advanced parts of the task.
* Use when tasks are not easily understood in their most complex form.

### Solution

Reduce otherwise complex functionality into something simple and easily understood. The process of hiding complexity implies removing possible areas of use in order to highlight others. Keep it simple. Make it easy. We prefer to use products and services that give us better return on the information we give them.

Reduction happens when the designer make informed and qualified guesses as to what users’ preferences are. Making design decisions that restrict a product’s usage to simple and few forms gives a product a direction. A direction that limits what a product can be used for.

The principle of reduction is about hiding complexity – making something very complex seem very simple. The process of hiding complexity implies removing possible areas of use in order to highlight others.

#### Keep it simple, make it easy.

*Reduction implied to navigation*
A wide navigation structure with fewer levels performs better than a narrow navigation structure with more levels.

*Reduction based on what other users did*
A common way of implementing the principle of reduction, is for a system to make decisions on behalf of the user based on what other users in similar situations decided.

Amazon reduces the task of finding an interesting book to buy to something simple by suggesting books that users with similar interests found interesting.

Google reduces meaningful search on the web to something simple by relying on what was highlighted by other websites via backlinks as being the most relevant for the user.

Hide complexity. Simplify activities into something simple and easily understood. The process of hiding complexity can imply removing possible areas of use in order to highlight others. Keep it simple. Make it easy.
Increase benefit/cost ratio. When benefits outweigh friction, the more likely it is that a behaviour will be performed.
Apply good defaults. Reducing complex behaviour into something more simple can imply providing fewer choices up front for the users. Make informed decisions based on the nature of their visit or what the majority of similar peers would decide. This will allow you to present a valuable result up front that can later be fine-tuned with more details.

### Rationale
We prefer to use products and services that give us better return on the information we give them.

As humans, we are cognitively lazy. We like to get the maximum benefit for minimum return. We prefer to use products and services that give us better return on the information we give it.

Reduce otherwise complex activities to a few simple steps (or ideally a single step). By making a behaviour easier to perform, the ratio between benefit and cost will increase and motivate people to engage in the behaviour more frequently. Additionally, simplifying a behaviour or activity may increase users’ self-efficacy (belief in own ability), to perform a specific behaviour, which in turn can help develop more positive attitudes toward the behaviour, get people to try harder to adopt the behaviour, and perform it more frequently.