+++
title = "Recognition over Recall"
template = "patterns.html"
[extra]
type = "comprehension"
icontype = "fas fa-landmark-dome"
+++

### Problem summary

We are better at recognizing things previously experienced than we are at recalling them from memory

### Usage

* Use when you want to maximize the data you receive about your users tastes and preferences when you ask them a question
* Use when you want to minimize the amount of cognitive energy spent by the users inputting data
* Use when you want to gain a broader insight into users’ preferences through recognition than what they usually tell recalling from memory.
* Use to let users tell more about their tastes and preferences in an easy and fun way.

### Solution

Instead of asking users to recall data from their memory, present a list of items which each represent a certain category of data. Instead of asking users to list things from memory, try complementing or replacing empty form fields with defined, random, and intelligent choices to choose or rate. Use visual imagery, auto-complete, and multiple-choice options.

If you’re interested in asking people to list things from memory, consider complementing – or replacing – empty form fields with defined, intelligent, or random choices that people can click on – or rate.

A classic move from using recall memory to recognition memory in user interface design was when modern GUIs (Graphical User Interfaces) slowly began replacing the older command-line interfaces known from DOS or the UNIX prompt. The effort associated with learning commands in the command-line interface made computers difficult to use. By presenting commands in menus in modern GUIs, the recalling commands from memory became obsolete and simplified the ease of use of computers.

#### Minimize need to recall
Minimize the need to recall information from memory whenever possible. Use easily accessible menus, multiple choice options, auto-complete suggestions, or visual imagery to aid decisions.

### Rationale
Recognition is triggered by context. We're quite good at it. With the radio on, we can sing the lyrics to thousands of songs.

Recall works without context. At this, we're terribly bad. With the radio off, our memories inevitably fade to black.

This imbalance is shared across our senses, and it's a huge factor in design.

#### Recognition vs Recall
It’s easier to click and choose from a variety of options than to write out those same things from memory. Recognition tasks provide memory cues that facilitate searching through memory why it is easier to recognize things than recall them from memory. It’s easier to provide a correct answer for a multiple-choice question than it is for a fill-in-the-blank question as the multiple-choice questions provide a list of possible answers. Open-ended short answer questions provide no such memory cues, why the range of possibilities is much greater.

*Recognition doesn’t involve origin, context, or relevance*
Recognition memory is much easier to access than recall memory. While recognition memory is obtained through exposure, recall memory is obtained through learning. Recognition does not necessarily involve memory about origin, context, or relevance while recall usually involves some combination of of memorization, practice and application. Furthermore recognition memory is retained for longer periods of time than recall memory – it is harder to recall the name of an acquaintance than it is to recognize it when heard.