+++
title = "Isolation Effect"
template = "patterns.html"
[extra]
type = "attention"
icontype = "fas fa-tent"
+++

### Problem summary

Items that stand out from their peers are more memorable

### Usage

* Use when you want to attract attention to one item more than all others.

### Solution

Make important information or key actions visually distinctive.

The Isolation Effect, also known as the Von Restorff Effect, proposes that one item that differs from multiple similar objects that are present, the one item that differs will be more likely to be remembered. When the item in question stands out less, the likelihood of it being remembered similarly decreases.

Inferred, people value a thing differently depending on whether it is placed in isolation and whether it is placed next to an alternative. One choice can be made to look more attractive, when placed next to an alternative, to which it distinctively outranks in some respect.

* *Guide through contrast* Meaningful and helpful contrast between items will make them stand out and ease the decision making of the user.
* *Use sparingly* Over-use of the Isolation Effect will devalue its presence and may lead to confusion and reduce aesthetics and the user’s ability to choose.
* *Reason the isolation* So you got the attention, but also make sure the the user can understand why you singled out that item. Show your rationale of why this product is more important than others you offer.

### Rationale

Being different is more memorable. Being positively remembered makes you stand out from the crowd. Create meaningful and helpful contrasts between products. Use colour, shape, position, and texture to accentuate contrast. We remember more positively, if we understand a meaningful rationale behind the accentuation.

We remember things that stand out. This is the reason why CTA (Call-to-Action) buttons stand out and look different from the rest of the actions buttons on the same page.

### Discussion

The Von Restorff effect was recognised by Hedwig von Restorff in 19331. She conducted a set of memory experiments around isolated and distinctive items, concluding that an isolated item, in a list of otherwise similar items, would be better remembered than an item in the same relative position in a list where all items were similar.

Taylor & Fiske further indicated that attention is usually captured by salient, novel, surprising, or distinctive stimuli. These may be used to enhance the von Restorff effect.