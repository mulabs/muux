+++
title = "Tunnelling"
template = "patterns.html"
[extra]
icontype = "fas fa-archway"
+++

### Problem summary

Guiding users through a process or experience provides opportunities to persuade along the way

### Usage

* Use when you want to close off detours from the desired behaviour
* Use when you want to focus on one goal more than many
* Use when you want to direct users attention into conducting one action
* Use when you want to design for conversion.
* Use when the user has made a decision to carry out an action and want to help him or her focus on that goal until it has been achieved.

### Solution

* Close off detours from the desired behaviour without taking away the user’s sense of control. Tunnel users through a decision-making process by removing all unnecessary functions that can possibly distract their attention from completing the process. The tunnel provides opportunity to expose users to information and activities and ultimately to persuasion.

* Lead users through a predetermined sequence of actions or events, step by step. When users enter a tunnel, they give up a certain level of self-determination – once they have entered the tunnel, they have committed to experiencing every twist and turn along the way.

When entering a tunnel, users are exposed to information and activities they might not otherwise have seen or engaged in otherwise. These information and activities provide opportunities for persuasion.

* *Ease the process*. For users, tunnelinng makes it easier to go through a proess like a workout program, spiritual retreat that controls their daily schedule, or even checking into a drug rehab clinic.
* *Control the user experience*. For designers, tunneling captures the audience, why they must accept or confront the logic of the controlled environment as content, possible pathways, and the nature of activities that users engage with are predetermined.
* *Provide consistency*. Tunneling is effective as people value consistency. Once committed to an idea or process, most people tend to stick with it, even in the face of contrary evidence – especially when the tunnel experience was freely chosen by the user.

### Rationale

Tunneling makes it easier to go through a process. For designers, tunneling controls what the user experiences – the content, possible pathways, and the nature of the activities. Tunnels are controlled environments in which users must accept the assumptions, values, and logic inflicted upon them.

Tunneling is effective as we value consistency. Once users commit to an idea or a process, most tend to stick with it. This is especially true in a tunnel situations that have been freely chosen2.