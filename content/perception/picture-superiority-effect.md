+++
title = "Picture Superiority Effect"
template = "patterns.html"
[extra]
type = "attention"
icontype = "fas fa-user-check"
+++

### Problem summary

We remember images much better than words

### Usage

* Use to boost recall of your message
* Use to grab the attention of your users
* Use to form advantageous associations with your product or service

### Solution

* *Use images to reinforce your message* Combining words with relevant images when conveying a message will significantly boost the percentage of people remembering the information they read.
* *Use Conceptual Metaphors* Use visual metaphors to effortlessly explain complex concepts or form advantageous associations with your product or service.
* *Not all images are created equal* Memorability of images depend on the user context, but in general we tend to remember human faces and images featuring both and object and a scene rather than just an image of an outdoor landscape. Images should focus on specific objects, but not so focused that the scene or setting disappears.

### Rationale

Pictures are recognized and recalled more quickly and easily than both written and oral language. The best results for learning comes from combining both words and images, as words allow us to explain complex or abstract concepts and images to encode those concepts for more efficient retention and recall. Images help grab attention, enhance comprehension, and help users remember your message.