+++
title = "Pattern recognition"
template = "patterns.html"
[extra]
type = "comprehension"
icontype = "fas fa-circle-nodes"
+++

### Problem summary

Even when there is no pattern, we seek ways to organize and simplify complex information

### Usage

Use to enable users to organize or label information – to make a game of arranging things

### Solution

We try to make sense of how information is grouped and presented.

* *Gamify the experience* Make a game of arranging things in a manner that sparks curiosity and encourages pattern-seeking behaviour. Consider playful ways to enable users to organize or label information and make a game of arranging things.
* *Provide affordance cues* Give cues as to how the user should interact with your experience. These cues tell you what to do with an object once you see it, such as a “push” or “pull” sign on a door.
* *Build a taste profile to reduce cognitive load* As you learn about the users’s knowledge, experience, and preference through the choices and action they make, it is possible through machine learning and simple categorization to build a data profile that can predict similar objects of preference. This can in turn help reduce friction as you will be able to present content that matches the preference of the user more accurately.

### Rationale

We seek ways to organize and simplify complex information as we try to make sense of how information is grouped and presented. We look for cues (affordances) in objects to figure out how we should interact with them and use our knowledge and past experience to make sense and meaning.
