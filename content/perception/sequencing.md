+++
title = "Sequencing"
template = "patterns.html"
[extra]
type = "comprehension"
icontype = "fas fa-code-merge"
+++

### Problem summary

Break down complex tasks into small and easily completed actions

### Usage

* Use to help users complete complex tasks
* Use to make more users complete a process
* Use to create a tunnel, focusing the users attention on just what is necessary to complete a task

### Solution

Break down complex tasks into small and easily completed actions, and set expectations as to how many steps are left before the entire sequence has been completed.

* *Split up complex tasks* Lower the cognitive load needed to complete complex tasks by breaking them down into small, easily completed actions that are steps in a sequence or simply a list of items that need to be completed to advance through the system.
* *Set clear objectives for subtasks* Each subtask should be specified in terms of objectives so that the whole area of interest is covered.
* Set expectations. Set expectations as to how many steps are left before the entire sequence has been completed.
* *Remove what is not needed* As you break down a complex task, take note of what users need to do, what the system can do, and what information the user needs. Sequencing into multiple steps allows for conditioning the content shown depending on earlier input, potentially helping to remove steps previously deemed necessary.

### Rationale

It is easier to take action when complex activities are broken down into smaller and more manageable tasks. Our working memory has limited capacity that can temporarily hold the information needed for reasoning and the guidance of decision-making.