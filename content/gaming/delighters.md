+++
title = "Delighters"
template = "patterns.html"
[extra]
type = "rewards"
icontype = "fas fa-face-kiss-beam"
+++

### Problem summary

We remember and respond favorably to new, unexpected, and playful pleasures

### Usage

* Use to make your product stand out from a crowded marketplace
* Use to form memorable experiences

### Solution

* *Form memorable impressions* Playful microcopy, a link to a fun video, or the gift of a compliment to a user, discovery of “easter eggs” such as coupons, virtual gifts, or a humorous image can help form favorable and memorable impressions that make your product stand out from the crowd.
* *Too much novelty is overwhelming* The highest value from novelty and surprise comes administering it in moderate levels. The rewarding effect of the novelty is overtaken by an aversive effect as novelty increases. There is such a thing as too much new!
* *Stand out from the crowd* In a crowded marketplace where users encounter similar products frequently, delight, novelty, and surprise can help overcome habituation and make your product stand-out form the crowd.

### Rationale

Novelty and surprise can transform something very normal and maybe even boring into a pleasant experience. It can overcome the habituation effect caused by people encountering many similar products every day and lead to increased product recall and recognition and word-of-mouth. Surprise is found to be positively related to satisfaction with a product.

### Discussion

According to the Kano model, delighters are not necessarily expressed in the requirements users are able to express themselves. They are, sometimes small, unexpected, surprises that bring a delightful and fun twist to the user experience. Sometimes they’re unconscious.

Delighters are the happy surprise that can make a difference, and an important source of satisfaction. If delighters aren’t there, dissatisfaction and frustration won’t follow: they’re not expected.

Usually in software development, delighters aren’t a priority, except if it is your product or company strategy. For instance the glass of champagne, when you arrive at the restaurant, the wifi on the plane or in your hotel room, or the mobile touch screen.

In the Kano model, exciters depicted as the keys to Innovation.