+++
title = "Hedonic Adaptation"
template = "patterns.html"
[extra]
type = "game design"
icontype = "fas fa-mug-saucer"
+++

### Problem summary

We return to a stable level of happiness despite major positive or negative events

### Usage

* Use to maximize the duration of user happiness
* Use to gradually release products or content to users to let it function like a anticipated reward.

### Solution

* *Slow it down by breaking it up* Slow down the fading process by breaking up the experience into smaller bits. Think of ways to keep users in a state of permanent and slight hunger; they will love you for it. Being asked, they will say that they want it all at the same time.
* *Too much good* Our expectations and desires rise in tandem to the levels of good or bad we are experiencing. Receiving a pay rise provide immediate happiness, but the feeling quickly fades as we get accustomed to it. Consider how you can keep users in state of permanent slight craving to keep expectations low and users happy.
* *Maximize the duration of happiness* What users think they want is not what will make them happy. Build anticipation and positive hype by gradually releasing products or content to users. By limiting access, each release will function as a much anticipated reward.

### Rationale
No matter how good or bad a new experience makes us feel, our feelings eventually revert back to normal, shifting adaption levels to the new baseline. Lottery winners are no happier than non-winners eighteen months later. Restricting pleasure increases the feeling of pleasure.