+++
title = "Levels"
template = "patterns.html"
[extra]
type = "design"
icontype = "fas fa-shield-halved"
+++

### Problem summary

Use levels to communicate progress and gauge users’ personal development

### Usage

* Use to adjust challenges exposed to users to their skill level
* Use to part up complex journeys into gradually more challenging levels

### Solution

Consider how you can partition your system into levels of increasing difficulty, powers, and features in order to keep users engaged, away from boredom, and provided with a sense of accomplishment.

* *Provide appropriate challenges* In videogames, a sequence of gradually more challenging levels help to maintain the balance between the rising skill level of users as they get accustomed to the game. Consider how you can gradually expose functionality requiring more expert knowledge.
* *Let users choose the difficulty level* Videogames often let players choose to play on “easy, medium or hard” modes so that they can quickly find the appropriate challenge. Consider allowing users access advanced modes or hide advanced functionality with progressive disclosure.
* *Reward users with completion* Break up a larger learning journey into smaller wins. First level is to complete the profile, second level is to create the first project, third is to invite collaborators.

### Rationale

As users progress, so does their skill level, requiring increasingly more difficult challenges. Consider how you can partition your system into levels of increasing difficulty, powers, and features in order to keep users engaged, away from boredom, and provided with a sense of accomplishment.