+++
title = "Investment Loops"
template = "patterns.html"
[extra]
type = "game design"
icontype = "fas fa-piggy-bank"
+++

### Problem summary

Let users invest their effort in setting themselves up for a future reward

### Usage

* Use to build habit forming products
* Use to get users to keep coming back
* Use to build addictive products
* Use to build habits

### Solution

Find ways for users to invest time, money, information, or effort into a product to set themselves up for future rewards. By sending a message to a friend, users set themselves up for receiving a reply – a future reward and trigger to get back into your product and conduct new behaviour. In this way a habit loop is formed. With the anticipation of a future reward to come, the likelihood of users coming back is greater.

* *Get people to invest* Let the user do work (investment) in the anticipation of future rewards (not immediate) that make the likelihood of users coming back for another pass in the loop more likely. Investments like this can help set up the trigger that is going to help bring users back int the loop. Consider what the simplest behaviour is the the user can do in anticipation of a reward.
* *Investments store value* For each pass in the loop, more value is stored in the product (user’s investments), effectively improving the product with use.
* *Map out triggers* Consider what external triggers need to be in place to get the user to the product, but equally important how can you setup triggers within your product (internal triggers) that gets your users coming back.

### Rationale

Setting up cycles of recurring triggers, actions, and investments will possibly set users up for coming back for more.