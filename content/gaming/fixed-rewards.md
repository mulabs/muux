+++
title = "Fixed rewards"
template = "patterns.html"
[extra]
type = "rewards"
icontype = "fas fa-dog"
+++

### Problem summary

Use rewards to encourage continuation or introduction of wanted behaviour

### Usage

* Use fixed rewards when users are to receive a reward when a specific goal has been reached as opposed to when a random event occurs.
* Use when there is little chance that users will get acclimated to receiving a fixed reward.

### Solution

Rewards is a mechanism for telling users that they have done well – that their actions have been judged favorably.

#### Fixed rewards
Fixed rewards are given out at a set time, amount, and type and are opposed to variable rewards, which feel more like random rewards.

In computer games, fixed rewards are given out when you complete a level or achieve some other kind of clearly defined goal. Variable rewards are usually given out when killing monsters.

In web applications fixed rewards are the most commonly used type of reward as they provide clear goals for users to strive for. At Hacker News, features such as voting on comments, or changing template colors are unlocked as you collect Karma points for your activities. At Stackoverflow.com, you receive a badge as you engage more and more in the community. Both provide clear set goals that users can strive for in order to climb up the ladder of status in the community.

The right reward at the right time and amount
Everyone likes to be told they are doing a good job, but it is essential for rewards to work that they are given out at the right time, in the right amount, and that it is the right rewards that is being given. Ask these questions for each opportune moment to determine what is right:

* What rewards is the system giving at the moment? Can it give out others as well?
* Are users excited when they get rewards or bored by them? Why is this?
* Do users understand the rewards they are given? Getting a reward you don’t understand is like getting no reward at all.
* Are rewards given out on a too regular schedule? Can they be given out in a more variable way ?
* How are rewards related to one another? Is there a way that they can be connected?
* How are the rewards of the system building? Too fast? Too slow? Or just right?

There is only one way to find out the right balance of time, amount, and kind of reward: through trial and error. Balancing rewards is often a question of “good enough”.

#### Types of rewards
There are several types of rewards that games and web applications can give. These are the most common.

* Praise
* Points
* Prolonged play
* A gateway – or unlocking features
* Expression
* Powers
* Completion

#### Rationale

Use rewards to encourage continuation or introduction of wanted behaviour in your users.

#### Discussion

In behaviourism, the rate or probability of a behaviour (“response”) is tried increased through stimulus (e.g. candy). The quality, or response strength, is assessed by measuring frequency, duration, latency, and accuracy.

##### Positive and negative rewards (and punishments)
There are two ways to strengthen behaviour through rewards: bring pleasure or excuse from pain – positive or negative rewards (also called reinforcements). Opposite of rewards are punishments, which does not necessarily remove what was first rewarded behaviour.

Positive rewards and punishments introduce stimuli to modify behaviour where negative rewards and punishments take away stimuli to modify behaviour. Rewards increase possibility of behaviour while punishments decrease possibility of behaviour.

##### Primary and secondary rewards
A secondary reward are the stimuli we have come to associate with the primary reward. When we see the message number notification appear on facebook, we associate that number with the feeling of receiving a message from our friends, which is the primary reward.