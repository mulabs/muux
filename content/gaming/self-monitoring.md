+++
title = "Self-Monitoring"
template = "patterns.html"
[extra]
type = "game design"
icontype = "fas fa-person-military-pointing"
+++


### Problem summary

Enable users to track the behaviour they want to change

### Usage

Use to make it easy for users to adjust their behaviour toward achieving a goal or outcome

### Solution

* *Make it easy to measure* Eliminate the boring part of measuring and tracking performance or status to make it easier for users to know how well they are performing.
* *Make correction easy* Help users adjust their behaviour in real time. Heart rate monitors vibrates when your heart beats too fast or too slow to let the user know whether to decrease or increase the level of exertion.
* *Infer consequences* Derive inferences about the broader context by combining several source of self-monitoring data and environmental data. An increased heart rate, little sleep, and no sweat might be a sign of mental stress.

### Rationale

Make it easy for users to track performance or status to help them achieve predetermined goals or outcomes. Letting users understand how well they are performing, willl increase the likelihood of continuing to produce the behaviour. Self-monitoring can help users learn about themselves and can be intrinsically motivating, but will almost always fail to get people to do things they do not want to do.

### Discussion

Measurement paradox. We tend to enjoy experiences less when tracking them. While measuring our performance may increase it, it also comes with the risk of decreasing how much we enjoy measured activities and how much we want to keep on doing them.