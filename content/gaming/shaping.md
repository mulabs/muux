+++
title = "Shaping"
template = "patterns.html"
[extra]
type = "rewards"
icontype = "fas fa-trowel-bricks"
+++

### Problem summary

Successively reinforce approximations to a target behaviour

### Usage

Use to help users perform overwhelming behaviour by breaking it down into smaller bits and gradually and successively approximate the target behaviour, step by step

### Solution

* *Break down target behaviour* When engaging in the desired behaviour (e.g. talking in front of a big crowd) is too overwhelming, then break it down into smaller bits that start small and progressively build up into more complex behaviour that gradually resembles the target behaviour.
* *Successively approximate* Start small (e.g. just standing on stake, then saying hello) to successively approximate and build up to the final desired behaviour.
* *Use to increase or decrease target behaviour* Introduce rewards to increase a behaviour and punishments to decrease a behaviour. A reward can both be positive (adding a favorable event or outcome) and negative (removing of unfavorable event or outcome). Similarly can punishments be both positive (add unfavorable event our outcome) or negative (remove favorable event or outcome).

### Rationale

Clearly define a target behaviour with your users. Then design a program that rewards any response that in some way resembles the target behaviour and gradually moves your reinforcement only to behaviour that is closer to the target behaviour until the final target behaviour is achieved, which then is the only behaviour to be rewarded.