+++
title = "Appointment Dynamic"
template = "patterns.html"
[extra]
type = "game rewards"
icontype = "fas fa-hourglass"
+++

### Problem summary

Force users to return at a set time to take a specific action and claim a reward

### Usage

* Use when you want to create a habitual loop where users keep coming back for more
* Use when you want to split up larger tasks into fewer to limit cognitive load
* Use to ask small first and larger, later

### Solution

Reward those who return and punish those who fail to re-engage on time.

* *Create traditions* Let the time and place to come back recur over fixed time schedules, i.e. every Tuesday, first day of the month, or similar. This will make it easier to remember and commit to – especially for groups and social contexts. “Happy hour” is a great example.
* *Penalize no-shows* Make it clear that not showing up at the set time will have negative consequences, just as showing up will have positive ones. In the game Farmville, your crops will vanish upon inaction.
* *Set a new appointment* Once the user comes back, make sure that it is very explicit when they need to be back again, before leaving.

### Rationale

The appointment dynamic provides reason and immediacy for the user to come back. Making the action required- and the window to take the action explicit can help create a sense of urgency as it becomes clear when the reward will be harvested or missed. Appointment dynamics are often tied to reward schedules that motivate frequent action.