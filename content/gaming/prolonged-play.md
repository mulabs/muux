+++
title = "Prolonged play"
template = "patterns.html"
[extra]
type = "game rewards"
icontype = "fas fa-bolt"
+++

### Problem summary
Reward users by prolonging their game time to allow for higher scores and measures of success

### Usage
Use promise of Prolonged play as a motivational factor for specific behaviour.

### Solution
Identify the central resource of your system. Odds are that the most valuable reward a user can receive is this. Can you get users to invite, refer, or do good deeds by extending the number of actions permitted by month, total storage space, or number of projects available?

Lurk users to perform a certain action with a motivational carrot. Define and set up goals that are easy to accomplish for users and communicate them well. Once users reach the goal(s) you have set up, reward them with some sort of resource that will prolong their play.

In many games, the goal is to risk your resources in order to gain points. In pinball, you risk your ball, and in Pac-Man you risk your lives. In games with this structure of lives, the most valuable reward a player can get is an extra life. In other games with time constraints, adding extra time is a prolonged game reward1.

In games, prolonged play allows for a higher score and a measure of success1. In web applications, it can motivate engagement, invitations, quality, or whatever behaviour you like.

Let’s sum it up:

* *Identify your central resources* Odds are that the most valuable reward a user can receive is the resource your product is built on. If your product provides cloud storage, then you can prolong play by doling out bonus storage space.
* *Use prolonged play as a carrot* Can you get users to invite, refer, or do good deeds by extending the number of actions permitted by month, total storage space, or number of projects available?
* *Balance your rewards* Prolonged play is just one type of reward. Generally, the more you can balance the types of the rewards you implement the better. Consider rewarding users with powers, completion, or by unlocking features among others.

#### Examples
Dropbox.com, a virtual drive for your computer in the sky, will add 250 MB to your account for every person you invite who sign up for their service. As you are set up with a fixed space-limit on your drive, 250 MB is a valuable resource to be rewarded with. It prolongs your play by making it possible for you to store more stuff on your dropbox drive.

forrst.com, a feedback-community of developers and designers, has a concept called Acorns, which you can use to promote your posts. When you sign up, you receive 1 free acorn to let you experience how it is to have a post promoted. You can prolong your acorn play by either buying new ones or earning new ones for good deeds in the forrst.com community.

### Rationale

For products based on resources, such as time, storage, and seats, topping up those resources can be an effective reward to motivate users toward action. Prolonged play is desirable as it allows for a higher measure of success, but it also taps into our natural human drive for survival.