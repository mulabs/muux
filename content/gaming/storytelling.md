+++
title = "Storytelling"
template = "patterns.html"
[extra]
type = "game design"
icontype = "fas fa-clapperboard"
+++

### Problem summary

Use the narrative qualities of storytelling to let the user engage in a perspective

### Solution

Stories can be explicit and simple narratives or implied in the words you use.

For effective use of storytelling in design, consider to:

* Create a plot, with conflict. Transform your users into heroes and obstacles into villains as you frame how they can overcome specific problems using your design.
* Make your users part of the story. Transfer the emotional power of the narrative to your users by letting them be actors in your story.
* Make your story episodic. Part the storyline, evolution of the user, or learning journey into episodic parts to continuously reward users with chapter endings, keep users in a state of craving more, and adjust for the users rising skill level as episodes or levels get harder.

It is easier for you to work on your product becoming part of an existing narrative and making sure it plays a positive role than than trying to create a new narrative itself from ground up.

### Rationale

Our understanding of the world is shaped by the stories we’re told. Consequently, we filter our decisions through stories, whether they are real or imagined.

Stories can be explicit and simple narratives or implied in the words you use. The most powerful stories are well-crafted visions that give significance to mundane tasks.

Stories excite. The most powerful stories are well-crafted visions that give significance to mundane tasks.

### Discussion

Some of the best theory on what constitutes a good story telling was laid down in 335 BC by Aristotle.

#### Aristotle’s Poetics
In Poetics (335 BC), Aristotle outlined 6 elements of drama that he believed a good story should include:

*Plot: What are users trying to achieve or overcome?*

The plot, or mythos, as Aristote called it, is concerned around about a person’s change in fortune (either from good to bad, or from bad to good), and is usually about overcoming some kind of obstacle or challenge. In relation to design the story plot is how we frame the struggles of our users and how we try to improve aspects of their lives. Consider what your users (character(s)) are doing? What are they trying to accomplish?

*Character: Who are your users?*

Character, or Ethos in old greek, is concerned around who your users are. Not just demographically, but who they are as people. What are their needs and desires? What are their traits, their personalities? What are their backgrounds, needs, aspirations, and emotions? How can users build up character, show off their merits, and stand as somebody to trust. Or how can users gain an empathic understanding of you and your product? Go beyond demographic data and appearances – for a fully fleshed out character, use insights into their needs, motivations, and emotions.

*Theme: How are obstacles reflected?*

The theme (called “Dianoia” by Aristotle) defines the overarching obstacle that needs to be crossed or the end goal of a journey. The theme helps connect the different elements of your product into a coherent narrative and universe. It will keep users engage and focused and help them understand the purpose of each element or feature of your product and how to interact through affordances.

*Language: In what tone do users interact?*

The dialogue or diction (called “lexis” by Aristotle) sets the tone. The tone with which you communicate with users has much to say in how they act and communicate either back or to peers. Consider whether your tone of voice should be formal or informal, serious or humoristic, superior or condescending, or elaborate or to the point? How much text is appropriate? What do you not say? Is anything implied in your copywriting?

*Melody and chorus: Is your user experience pleasant and predictable?*
Use design patterns to remove friction and avoid forcing users use cognitive energy on processing your user interface. Using design patterns, your experience will feel familiar and well known and act as the basic underlying melody of your experience.

The chorus is reoccurring element that unites the plot. It should feel familiar and by that provide pleasure. The chorus should be an integral part of the whole, and share in the action. The chorus helps stir emotion and motivate us to find a solution

*Spectacle: If not in place, something will feel wrong*

If the play has “beautiful” costumes and “bad” acting and “bad” story, there is “something wrong” with it. Even though that “beauty” may save the play it is “not a nice thing”. The spectacle (opsis) should be appropriate.

The decor and how the experience is presented should match the setting users expect from the narrative. You should of course make your design outstanding so users will remember it, but equally important is that the experience and context matches it.

This however, doesn’t mean that you should rid your experience from delight. Consider whether plot twists and unexpected behaviour should be part of your storyline. Can you create a “spectacle” that users will remember, and possible generate discussions and ideas.