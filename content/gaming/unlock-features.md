+++
title = "Unlock Features"
template = "patterns.html"
[extra]
type = "game rewards"
icontype = "fas fa-lock-open"
+++

### Problem summary

Utilize a user’s desire to explore by unlocking new features as a reward for specific behaviours

### Usage

* Use when you wish to distinguish new rookie users from experienced all stars
* Use when you want to retain users and keep them active
* Use when you want to keep users’ behaviour in a forward motion toward your goals
* Use when you want to convey a sense of unfolding a website as users explore it

### Solution

Reward contributors and curators for their good deeds as they add content to your system. Unlocked powers act as endowments that will lock your users into valuing your website more than before they signed up. In a social website setting, show off users with unlocked features to give rookies something to strive for.

Unlock features for your users as they explore and engage your system. The principle of unlocking features is widespread in computer games, where users are rewarded by being moved to new parts of the game (access to a new level, win a key to a locked door, etc.) upon achieving something specific. In web design, it is most often the contributor and curator role that is being rewarded as their activity is what makes or brakes a social website.

The relationship between the contributors and curators are often intertwined in that users with a proven track record of quality content are immediately promoted more than a rookie user or a user with a poor track record.

You can choose to either have a fixed currency for unlocking features (points) or unlock features as users reach specifically set goals.

*Reward good deeds.* Reward contributors and curators for their good deeds as they add content to your system.
Provide appropriate challenges. Dole out appropriate tools and challenges as the skill level of users increase with their continued use. An advanced feature might be too much to handle for a novice and can be liberating for an expert user to get access to.
Lock in users to your product. Unlocked capabilities act as endowments that will lock your users into valuing your website more than before they signed up. In a social website setting, show off users with unlocked features to give rookies something to strive for.

Example: Karma points at Hacker News
On Hacker News, users unlock features as they collect karma points. Karma points are earned for a popular post, successful curated content and the likes. The amount of points needed for certain features is victim for constant scrutiny and adjustment, as it has a clear connection to the quality and amount of contributions of the site. At some point, these were the karma point limits of unlocking features:

10 karma – Upvote comments
51 karma – Downvote comments
51 karma – Flag comments
200 karma â€“ Make polls
250 karma â€“ customize the top bar background color
Points are an effective way to communicate a clear one-way path up the status ladder. It’s also a great to keep score of users’ deeds. There are however other ways of quantifying the criteria for unlocking features.

Other ways of quantifying the criteria for unlocking features
You do not need to have a point system up and running in order to clearly communicate the path to unlocking features. Another take is to unlock features based on what type the user is. If a user is typically a commenter and active in that field, then unlock flagging and rating comments once the user has commented 20 times. If a user is contributing a lot of uploads, then unlock power rating on just uploads after 10 submissions.

Other unlock goals could be to recruit friends to join the website or share a link.

### Rationale

As your users get accustomed to your product, their skill level rises and what was first a challenging task becomes tedious. To match the growing skill level of your users and avoid boredom as users evolve from novice to expert users, unlocking more advanced features is a common strategy.

As users explore your website and start unlocking features, they invest time and effort into your website. The effects of Loss aversion and the Endowment effect will lock your users in to valuing your website more than before they signed up. The points and features you have earned are valued more when their value is judged as a loss than a gain. Points already earned are valued more than points to be earned. The status and history of having achieved a large set of features will lock in users to sticking with your site.

Furthermore, the higher status you have in a community and the more power you have with the features you unlock, the more Social proof there is that you are to be trusted. This social proof has been utilized by google, who is known to have invited job candidates based on users stackoverflow.com accounts.

### Discussion

There are several issues to be tackled in systems based on incentives for participation.

##### Gaming the system
There will always be users who will look for ways to exploit the system to their own personal or financial gain. This is why algorithms for self-regulation has been put in place in many systems. However, no matter how complex a system becomes in weeding out exploiters, there will always be some way to cheat the system.

Point systems promote a situation that contradicts the core purpose behind it: instead of encouraging good behaviour and helpfulness among its users, it entices some to behave in unscrupulous ways to get ahead of others2.

##### Drowning out new users
As a community gets older and its users earn more points, it gets increasingly harder for new users to reach the same status as long-time active users. The problem becomes apparent when contributions of old users are promoted and in turn awarded more points than a new user who might contribute even better quality content, but who lacks the power of the unlocked features of the old guard.

##### Acclimation to rewards
People have a tendency to get acclimated to rewards the more they receive them. What was rewarding an hour ago is no big deal now1.

One way of overcoming this is to gradually increase the value of the rewards as the user progresses up the ladder. It works even though you as a user can clearly tell that a designer implemented it like that and why.

Another way to keep people from getting acclimated to rewards is to make the rewards variable instead. Variable rewards are unpredictable and random. The element of a delightful surprise engage users for a longer time – even though the number of points given is the same on average.

##### The end of the road
What happens when all features are unlocked? Is the game then beaten? Or can you make your rewards variable ?

Unlocking features should not be used as the one and only way to motivate users toward engagement and action, but more to get them started and to spark their enthusiasm. Design your end state so that the experience you have built will be enjoyable even with all features unlocked.

If you insist on basing the motivation toward action on rewards, consider taking previously unlocked features away from users upon inactivity or misconduct. However, be aware that this might hurry the exit of an already fleeing user who might just have needed a positive reminder to be retained as an active user.