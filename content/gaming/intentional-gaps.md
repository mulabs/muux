+++
title = "Intentional gaps"
template = "patterns.html"
[extra]
type = "game design"
icontype = "fas fa-circle-notch"
+++

### Problem summary

Create intentional gaps that users can't help but try to fill

### Usage

* Use to allow users to see how their decision or action fits into the larger picture.
* Use to frame decisions around a predefined context (completing the sentence, fill in the gap)
* Use to reduce cognitive load to make choices obvious

### Solution

* *Create obvious gaps* Leave deliberate gaps that users will want to fill. By choosing what gaps to fill and which not to fill, you are framing decisions around a predefined context and allow directing users toward predefined decision paths that allow for persuasion along the way.
* *Finish the sentence* Make forms read like a sentence, letting users fill out the blanks. Making forms linguistically fluent, its context is easier to understand and inputs are more effortlessly selected.
* *Instill a sense of autonomy* Provide good momentum by laying out the first foundational blocks paving the way for a flying start. Give users the freedom to complete your intentional gaps with creativity to boost their sense of autonomy and control.

### Rationale

The feeling of seeing something out of order or incomplete creates a feeling of stress that we want to alleviate. This in turn motivates us to either remove or reduce the discomfort by filling in the missing gaps or fixing what is wrong. The more obvious and easy it is for users to complete the intentional gap, the more likely users are to engage.