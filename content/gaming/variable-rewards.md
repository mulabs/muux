+++
title = "Variable rewards"
template = "patterns.html"
[extra]
type = "Rewards"
icontype = "fas fa-dice"
+++

### Problem summary

Use random rewards to convey a sense of scarcity and unpredictability to entice users curiosity in discovering the pattern

### Usage

* Use variable rewards rather than fixed rewards when there is a chance that users will get acclimated to rewards the more they receive them

### Solution

The activity level of users is a function of how soon they expect a reward to be given. The more certain they are that something good or interesting will happen soon, the more activity they will produce. When users know a reward is a long way off, the motivation is low and so is user activity2. This reward schedule is called a fixed one, as users are rewarded again and again with a fixed ratio or interval. Variable ratios and intervals on the other hand randomize rewards around an average. The latter produce the highest activity in users.

* *Avoid extinction* As you stop providing a recurring reward the sudden lack will likely feel as punishment and cause anger and frustration.
* *Find the right balance* Are rewards building up too fast, too slow, or just right? If the user can’t catch up, he or she will become satiated, and the reinforced behaviour will go to extinction. Record awards provided, and observe the effect to strike the right balance. Balancing rewards is often a question of “good enough”.
* *Facilitate intrinsic rewards* Motivation coming from an pleasure of the activity itself is stronger and more sustainable than motivation coming from extrinsic rewards (although easier to administer). This is why it makes sense to use the extrinsic motivation from rewards to facilitate behaviour that will lead to intrinsic rewards such as mastery, recognition, and personal growth.

Read on to find out what variable reward ratios are and how they are different to fixed ratios and intervals.

Example: Lomography.com
Lomography.com sells retro analogue cameras on their website and in physical stores. To spark the enthusiasm of their lomographic society (fans taking pictures with lomography cameras), Lomography has created an online community for sharing pictures.

Community activity is backed by a “piggie bank” system where users can earn piggie points by having their photos selected as “photo of the day”, by submitting reviews of cameras and accessories, by winning rumbles and competitions, by translating content, and much more. Piggie points, which have an expiration date, can be used in the online shop to by cameras and thus translate into cold cash. The piggie bank system utilizes a mix of fixed and variables ratios and intervals.

At the retro camera company, Lomography, you can earn “piggie points” for your online activity, which translates to cold cash in the lomography online store. Piggy point rewards are given out both at fixed ratios and at variable intervals.

### Rationale

As humans, and animals, we react differently to certain patterns of rewards. behaviourism has studied these patterns2 and have come to the conclusion that variable reward schedules and contingencies motivate us more than fixed schedules and contingencies.

Contingencies are rules or sets of rules defining when rewards are given out. There are two fundamental sort of contingencies: ratios and intervals. Ratios schedules provide a reward after a certain amount of actions have been carried out – the more you do, the more you get. Interval schedules provide a reward after a certain amount of time has passed.

##### Fixed vs variable ratios
Rewards with a fixed ratio are given out again and again after completing the same action the same amount of times. It could be that you will receive 10 karma points every 5th time you reply to a comment or that you would increase your level every 10th time you uploaded a video.

The problem with fixed ratios is that users distinctly pause completing actions when they receive a reward, as they know receiving a new reward will take a while. This creates an opportune moment for the user to walk away. However, the break in rewards caused by fixed reward ratios might also give the user an opportunity to explore different aspects the system.

Variable ratios are rewarded after a specific number of actions have been carried out, but that number changes every time. A user might know to upload approximately 10 videos to rise in levels, but the precise number is randomly generated every time – everything has a chance of reward. Such variable ratios have proven to stimulate more activity than fixed ratios – even when on average the same amount of rewards are given out.

Variable ratios are free from the pause in activity generated from fixed ratios. It’s important to note that users do not know how many actions are required this time, just the average number from previous experience2.

Rewarding with fixed ratios produces a pause in activity after a reward has been given and a burst of activity just before being rewarded. While users typically respond at a higher rate in the fixed ratios bursts, variable reward ratios provide a more consistent rate free from pauses of the fixed ratios.

##### Fixed vs variable intervals
Instead of providing a reward after a certain number of actions has been completed, interval schedules provide rewards after a certain amount of time has passed. Users being rewarded in fixed intervals will pause activity once an award has been given and wander around for a while. They will return frequently to check if their reward has been “refilled” or has reappeared. Gradually, checks will become more frequent as the proper time nears.

Vimeo.com utilizes fixed reward intervals for its regular users, who are allowed to upload only 500 mb of video every week. After the fixed interval of one week, the user’s upload quota is refilled. As users reach their upload quota on vimeo.com, their activity will pause until its refilled next week. Vimeo hopes users will use the pause to consider buying a pro account with no upload quota.

With variable reward intervals, the period of time changes after each reward as with the variable ratios. As with variable ratios, variable intervals also produce a steady and continuous flow of activity – there is always a reason to be active.

### Discussion
Variable and fixed ratios and intervals can successfully be combined. Say that you need to upload 20 videos to become an elite member, where after you will have the chance of picking up a free pro-membership appearing at variable intervals.

##### Extinction
Extinction is what happens when you stop providing a reward and will feel as a punishment that cause anger and frustration in the user. behaviour learned on the variable schedules are much harder to extinguish, as users will never know if what they are experiencing is just an unusually long run of misses and that next reward will justify their patience.

##### Avoidance
The principle of avoidance uses the negative reward of decay, pain, or other forms of punishment to keep users active. An example could be to force users to revisit your site and log in once a week to avoid their “elite” status to be taken away.