+++
title = "Achievements"
template = "patterns.html"
[extra]
type = "rewards"
icontype = "fas fa-star"
+++

### Problem summary

We are engaged by activities in which meaningful achievements are recognized

### Usage

* Use when you want to show and guide users as to what is obtainable
* Use to let users gauge their progress – how much is left to discover and explore?
* Use to let users benchmark and compare their own progress with similar peers
* Use to establish social proof of the credibility and merits of another individual
* Use to establish social proof of what is easily achievable and what is not.

### Solution

* *Articulate what is possible* What challenges and desired behaviour do you have in place and how are they linked to achievements? Achievements can help anchor the ambitions we want users to embrace. In gaming, achievements are shown through points, badges, and levels; in other contexts through promotions, memberships, privileges, and acquisitions. Consider providing social proof what has been achieved by similar peers.
* *Provide appropriate challenges* Break down a larger goal into smaller and more easily obtainable wins that match the gradually rising skill level of your users as they go from one challenge to the next. A too hard challenge will leave users stressed, full of anxiety, and giving up. A too easy challenge will leave people bored and looking for something harder to try.
* *Provide feedback* When a list of possible achievements are publicly accessible, achieving of each provide feedback on the users progress as to how much is left. Furthermore, achievements can communicate what is possible and what directions a user can take.

### Rationale

We are motivated by achievements of personal or social significance that represent appropriate challenges matching our gradually rising skill level as we go from one achievement to the other. Achievements help anchor what goals users should aspire to achieve and provide social proof what is obtainable for peers similar to us. In this way they allow for social comparison and can help increase our self-efficacy.

#### *Why do achievements work?*

There are several reasons why achievements work and ways in which they can help your product:

* *Achievements anchor our expectations* When making decisions, we tend to rely, or anchor, heavily on the first information presented. This is called the anchoring effect. In this setting, achievements anchor what goals we should aspire to achieve. A too low goal might lead to ambitions not matching user capabilities and end in boredom where a too high and seemingly unobtainable goal will leave users stressed and full of anxiety. In this way, achievements can help communicate and anchor expectations about what are reasonable goals to aim for.
* *Having goals increases self efficacy* Seeing proof of others who obtained trophies and other achievements communicates the idea that obtainment is possible2. It stirs our belief in our own efficacy – our ability to do something if we just try it. Of course, this requires that the goal seems appropriate to the skillset of your users and where they are in their learning journey.
* *Completing goals are rewarding* Completing a goal is a reward in itself and is a key motivational factor to drive people toward action. Especially when users are close to completion or reaching their goal.
* *Goals creates commitment* When goals are clearly articulated and broken down into tasks, it increases chances that users will reach them as the commitment is more clear. Can you further get users to publicly commit to the mission of achieving a goal, the power of the commitment gets stronger.
* *Achievements provide feedback* When a list of possible achievements are publicly accessible, achieving of each provide feedback on the users progress as to how much is left. Furthermore, achievements can communicate what is possible and what directions a user can take.
* *Achievements trigger social proof* Both in the sense of showcasing the credibility and merits of an individual user, but also what achievements or goals are more obtainable than others. Social proof is our tendency to assume the actions of others in new or unfamiliar situations.
* *Achievements trigger social comparisons* We seek objective information about our performance. When that information, or a suitable context to evaluate it in, is not there, we will seek to compare ourselves to meaningful others. Achievements, trophies, and badges earned by other users is a convenient way to do this kind of benchmarking; if I can see that similar peers earned an achievement, I am more motivated to try it.