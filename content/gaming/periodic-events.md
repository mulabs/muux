+++
title = "Periodic events"
template = "patterns.html"
[extra]
type = "game design"
icontype = "fas fa-mountain-sun"
+++

### Problem summary

Construct recurring events to build up anticipation, a sense of belonging, comfort, and a sustained interest

### Usage

Use to engage users into coming back to your experience time and time again.

### Solution

Consider ways to create shared recurring experiences users can look forward to or reminisce about. Examples are weekly tips, Black Friday sales, and monthly report cards.

* *Create a tradition* Recurring traditions and ceremonies creates an experience users can look forward to or reminisce about and can help foster a sense of belonging and comfort. Examples are weekly tips, monthly report cards, throwback Thursdays, and even Black Friday sales.
* *Make the experience shared* Consider how all users or groups within a system can enjoy shared recurring experiences to build a sense of belonging. How can you facilitate the users share the experience with each other?
* *Build a narrative* Tying a narrative structure around periodic events will provide meaning and and motivating anticipation of future events and help direct users toward appropriate action when the periodic event happens.

### Rationale

Anticipation of an upcoming event can in itself be motivating and can provide a limited period of time, where users prepare themselves potential action or commitments. Events temporarily create special places that are different from everyday places, which provides a great occation for taking action, breaking patterns, starting new habits, or changing behaviours or attitudes.