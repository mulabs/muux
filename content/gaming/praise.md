+++
title = "Praise"
template = "patterns.html"
[extra]
type = "game rewards"
icontype = "fas fa-award"
+++

### Problem summary

Use explicit statements, graphics, a sound effect, or similar indicator to reward a job well done.

### Usage

* Use when your users need guidance in what is right behaviour.
* Use when you want to enforce certain user behaviour over other kinds of behaviour
* Use to let users know they are on the right path
* Use to keep up the momentum of your users

### Solution

Praise is a form of feedback that fall into the category of rewards. The opposite of praise if blame or derogation. Where blame or derogation are tools for negative feedback, praise is a positive feedback form.

Consider every moment in the experience you have created for your users and ask yourself the following questions for every one of them in order to figure out if praise is right for just that moment:

* What do users need to know at the moment
* What do users want to know at this moment
* Will the feeling you want your users to feel be enforced by praise at this moment?
* What do the users want to feel in this moment? Is there an opportunity to use praise to create a situation where they can feel that?
* Can praise be used to enforce the correct behaviour?
* What are your users’ goal at this moment? Will praise help them toward this goal?

Praise is interpreted simply by users: the system has judged you, and it approves.

### Rationale

Guide your users toward your preferred behaviour by praising it when it is conducted.