+++
title = "Appropriate challenge"
template = "patterns.html"
[extra]
type = "design"
icontype = "fas fa-spoon"
+++

### Problem summary

The user needs appropriate challenges to remain engaged

### Usage

* Use when challenging users with too hard tasks will leave them stressed and full of anxiety
* Use when experienced users might be bored by too easy and mundane tasks.
* Use when the same system need cater to both experienced and unexperienced users.

### Solution

To keep users in flow we need to give them Appropriate challenges. If a challenge is too hard, the user is going to feel stress and anxiety. If the challenge is too easy, the user is going to feel bored. Both boredom and anxiety tend to lead to disengagement from the activity that was previously rewarding.

To design for appropriate challenges is to keep a careful balance between neither producing anxiety nor boredom. To keep users in the flow channel. It’s a careful dance keeping balance between the difficulty curve and the learning curve.

Users are most engaged, if they follow a roller-coaster pattern through the Flow Channel. It’s more fun in other words, if challenges sometimes seem too difficult – until our rising skill level flattens the curve. The ensuing mastery is also fun as we dominate the challenge or breeze through a task. But before it becomes old, the challenge level must rise again.

* *Simplify it for beginners* Exposing a complicated or advanced user experience to a beginner will overwhelm them and possibly scare them away. Reduce complexity by hiding advanced options from direct sight so that novice users can concentrate on completion and immediate success.
* *Allow advanced options* As users grow in skill level, they will want to explore more advanced options. Consider either showing shortcuts to more advanced features or designing an expert-only experience.
* *Design for changes over time* Users evolve. Consider the contexts and skill-levels of your users and design appropriate challenges and experiences which suit them. 
* *Design for a sequence of events* that progressively require an increased skill level.

### Rationale

Users are most engaged (in flow) when the difficulty of a challenge is matched with their skill level and is neither too hard or too easy. A too hard challenge will leave users stressed and full of anxiety. A too easy challenge will bore them. As a user’s skill level rises, a hard challenge becomes easy. Keep users in flow by ensuring a careful balance between the difficulty curve and the learning curve.

*Consider the context of your users and design*

appropriate challenges and experiences which suit it. That is, design for changes over time. This is a radical break from the the standard usability approach, where everything is concentrated around making things as easy as possible. By designing for changes over time, you also concentrate on making things harder (more advanced) to do as users progress, to suit their growing skill level.

We’re in flow when we experience a task so positively that we do not allow ourselves to be diverted by distractions that don’t support the challenges and goals we are pursuing.

To keep users in flow we need to give them appropriate challenges. If a challenge is too hard, the user is going to feel stress and anxiety. If the challenge is too easy, the user is going to feel bored. Both boredom and anxiety tend to lead to disengagement from the activity that was previously rewarding.

To design for appropriate challenges is to keep a careful balance between producing neither anxiety nor boredom, to keep users in the flow channel.

When you think about appropriate challenges in your design, you are most often designing for a sequence of events that progressively require an increased skill level. In video games, the events are often represented by levels; in e-learning, by lessons within courses. To complete a challenge, it is necessary that the requisite learning takes place.