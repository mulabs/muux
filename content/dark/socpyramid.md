+++
title = "Social Pyramid"
template = "patterns.html"
[extra]
icontype = "fas fa-cubes"
+++

Requires users to recruit other users to use the service. This is a method commonly used in social media applications and online games. Users can invite their friends to use the service and are incentivized with benefits from the platform