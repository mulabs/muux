+++
title = "Sneak Into Basket"
template = "patterns.html"
[extra]
icontype = "fas fa-basket-shopping"
+++

Adds items not chosen by the user to the user’s online shopping cart, often claiming them to be a suggestion based on other purchased items. This may cause the user to unintentionally buy these items if they do not notice them prior to checkout.