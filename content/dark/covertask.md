+++
title = "Covert Ask"
template = "patterns.html"
[extra]
type = "sneak"
icontype = "fas fa-gift"
+++

Presenting an offer that asks for the consumer to provide certain data in exchange for a reward (e.g. provide their email for $25 discount), and then asks the consumer for more information (e.g. their phone number).