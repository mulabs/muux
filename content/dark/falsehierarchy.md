+++
title = "False Hierarchy"
template = "patterns.html"
[extra]
icontype = "fas fa-layer-group"
+++

Gives one or more options visual or interactive precedence over others, particularly where items should be in parallel rather than hierarchical. This convinces the user to make a selection that they feel is either the only option, or the best option.