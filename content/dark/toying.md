+++
title = "Toying with Emotions"
template = "patterns.html"
[extra]
icontype = "fas fa-dice"
+++

Any use of language style, color, or other similar elements to evoke an emotion in order to persuade the user into a particular action. Toying with emotion can manifest as cute or scary images, or as enticing or frightening language.
Confirmation Shaming is a specific example.