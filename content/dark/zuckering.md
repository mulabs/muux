+++
title = "Privacy Zuckering"
template = "patterns.html"
[extra]
icontype = "fa-brands fa-facebook"
+++

Tricks users into sharing more information about themselves than they intend to or would agree to. This includes the selling of user’s information to third parties that is included in the Term and Conditions or Privacy Policies of websites.