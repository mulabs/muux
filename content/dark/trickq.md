+++
title = "Trick Questions"
template = "patterns.html"
[extra]
icontype = "fas fa-wand-magic-sparkles"
+++

Including a question that uses confusing wording, double negatives, or otherwise confusing or leading language to manipulate user interactions. Appears to be one thing, but is actually another. One common example of this tactic is the use of checkboxes to opt out rather than opt in, often paired with confusing double negatives.