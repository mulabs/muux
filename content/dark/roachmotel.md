+++
title = "Roach Motel"
template = "patterns.html"
[extra]
icontype = "fas fa-bug"
+++

Describes a situation that is easy to get into, but difficult to get out of. This usually occurs when a user signs up for a service easily, but closing their account or canceling the service is difficult or impossible. A typical pattern of interactions requires the user to call a phone number in order to cancel their account, where they will be further pressured to maintain their account.