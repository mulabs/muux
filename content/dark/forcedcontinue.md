+++
title = "Forced Continuity"
template = "patterns.html"
[extra]
icontype = "fas fa-mpney-bills"
+++

Continues to charge the user after the service they have purchased expires. This pattern takes advantage of users’ failure to check up on service expiration dates, either for a free trial or for a limited-time use of a paid service, by assuming upon service expiration that the user either wants to continue the paid service or upgrade to the paid version of the free trial, and charges the user for either respective option and continues the service.