+++
title = "ConfirmShaming"
template = "patterns.html"
[extra]
type = "sneak"
icontype = "fas fa-face-sad-tear"
+++

The act of guilting the user into opting into something. The option to decline is worded in such a way as to shame the user into compliance.