+++
title = "Price Comparison Prevention"
template = "patterns.html"
[extra]
icontype = "fas fa-money-check"
+++

Seeks to dampen the effect of market competition by making direct price comparisons between products and services difficult. Tactics could include making important product information un-copyable,so as to prevent users from pasting such information into a search bar or another site.