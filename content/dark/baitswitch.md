+++
title = "Bait & Switch"
template = "patterns.html"
[extra]
icontype = "fas fa-fish"
+++

As the name suggests, this pattern occurs when a user takes an action expecting one outcome, but instead something else happens.
