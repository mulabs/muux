+++
title = "Gamification"
template = "patterns.html"
[extra]
icontype = "fas fa-gamepad"
+++

Situations in which certain aspects of a service can only be "earned" through repeated (and perhaps undesired) use of aspects of the service. One common instance of gamification is "grinding", a term used in many video games to describe the repeated process of killing monsters to gain experience(XP) points in order to level up the user’s character.