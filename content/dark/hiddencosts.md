+++
title = "Hidden Costs"
template = "patterns.html"
[extra]
icontype = "fas fa-money-bill-wave"
+++

Provides users with a late disclosure of certain costs. In this pattern, a certain price is advertised for goods or services, only to later be changed due to the addition of taxes and fees, limited time conditions, or unusually high shipping costs.