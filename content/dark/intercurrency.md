+++
title = "Intermediate Currency"
template = "patterns.html"
[extra]
icontype = "fas fa-bitcoin-sign"
+++

Where users spend real money to purchase a virtual currency which is then spent on a good or service. The goal of this pattern is to disconnect users from the real dollar value spent in order to cause the user to interact differently with the virtual currency. This may result in users spending the currency more liberally than they would with fiat currency. This pattern usually manifests as in-app purchases for mobile games.