+++
title = "Leaderboard"
category = "social"
[extra]
type = "reputation"
icontype = "fas fa-chart-line"
+++

### Problem summary

Users want to know who are the very best performers in a category or overall

### Usage

* Use when your community is highly competitive, and the activities that users engage in are competitive in nature. Examples of competitive communities are player-versus-player contests, stock games, etc.
* Use when you want to enable player-to-player comparisons
* Do not use when the activities that users engage in are not competitive in nature. Examples are writing content, giving feedback, sharing photos, etc.

### Solution

List a fixed number of competitors ranked by score from highest to lowest.

Allow ranked users in a highly competitive community to know who are the very best performers in a category or overall. Leaderboards can grow too stable and elite and thereby discourage rookie users from participating. Use with caution and only if the primary purpose of the community is competition, as introducing leaderboards can easily lead to gaming and non-constructive community behaviours.

### *Multiple views*

Consider providing multiple views of a leaderboard across time (all-time, weekly, daily) and category (most points, most shares, etc.). All-time views are usually stable and sometimes (too) stagnant, why you should consider making the default view weekly or daily to showcase the “latest movers”1.

*More design tips*

* *Show where the user is placed*. Show the users standing and who are immediately above and below them. This will allow users to always have the possibility of climbing without seeing users they will never catch up with.
* *Leaderboards should be contextual*. Users should be compared to similar users, who are active and involved at the same level or time.
* *Leaderboards among friends*. Consider showing users leaderboards containing their friends or other local users. The impulse to compete against people you know is much stronger than to compete against strangers online.
* *Leaderboards should be updated continuously*. Stale data or rankings that change slowly can discourage users so make to always give users a sense of progress and gratification.