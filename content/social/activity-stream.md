+++
title = "Activity Stream"
category = "social"
[extra]
type = "interactions"
icontype = "fas fa-person-swimming"
+++

## Problem summary

The user wants to get an overview of recent actions in a system that are interesting from his or her perspective.

### Usage

* Use when you want to allow your users to stay in touch across the web.
* Use to keep users up-to-date with their contacts activities
* Use when you want to prompt your users to respond to other user’s actions
* Use when you want to expose and promote the functionality of your system by guiding people by the actions of others.
* Do not use when your system does not have user activity as one of its key elements.

### Solution

Provide an overview of recent activity that is relevant to the user.

Allow users to catch up on recent updates with little time and effort invested. Activity streams are most often used to aggregate recent actions by individual or multiple users from the perspective of one user. They provide links to further explore the actor, the subject, or the activity itself.

The Activity Stream is a live feed, created by aggregating social activities in one place, for a user and their contacts. Social activities can vary greatly depending on the system. Popular activities are uploads (photos, videos, audio, and other files), comments, new friendship/follower relationships, bookmarks on del.icio.us or ma.gnolia, music on last.fm, posts from blogs, or even items in the feeds of facebook, friendfeed, and twitter. Every action a user does can be gathered into one stream.

An activity stream can either aggregate the actions of a single user or the actions interesting to a single user. The first is about only one user and the latter abut multiple users from the perspective of one user. Aggregating actions of a single user is often used on profile pages, where all actions the profiled user has done is aggregated into one place. Aggregating actions interesting to a single user aggregates all actions from the user’s friends and who he or she follows into one stream.

The details of an activity stream
Generally, the anatomy of an activity are one of these2:

Actor |verb| (object) [context]
Anders |tweeted| (Testing, testing) [via Tweetie]

Actor |verb| (object) {Indirect object} [context]
Anders |tweeted| (Testing, testing) {to Christian} [via Tweetie]

Aggregated activities
When multiple similar activities happen, they can beneficially be aggregated into “story”. A list like this…

David changed his profile picture
Thomas changed his profile picture
Ashley changed her profile picture
…can be converted into this:

David, Thomas, and Ashley changed their profile pictures
Verbs
Common verbs used in activities are: Likes, followed, commented, tagged, bought, posted, shared, and uploaded.

## Rationale

Activity streams allow for engagement. They expose users to the possible actions that can be taken on a site. At a glance, users can see what other people are doing and start experimenting themselves. In this sense, activity streams is an alternate form of navigation and discovery.

Activity streams are real-time, and thus put a focus on what is going on right now: They have timely relevance.

They allow users to stay in touch across the web in an open and emergent fashion.

As activity streams consists of many small bits of information. Bits of information that can be filtered, searched, and automated. They are a combination of quantitative small stories with qualitative attributes. However, the stories can be qualitative in nature like updating a status in facebook and the attributes can be quantitative such as the number of likes. Content that has value to the user can be produced by combining smaller bits of information, which in isolation does not have value.

Similarly, the large quantities of data can be used to predict what is more important for one user based on his or her past behaviour.

## Discussion

Avoiding negatives
It seems as though most social services using activity streams have avoided allowing negative actions. On facebook, for instance, you can “Like” an activity, but not “Dislike” it.

When you become friends with a new person, your activity stream is updated with a story, but not when you remove a friend.

Keeping it relevant
One of the biggest issues when designing activity streams is to figure out what is relevant to the user. Activity streams are built upon large quantities of data which allows you to display a vast amount of different activities. The challenge lies in finding the threshold of relevancy.

A good activity stream is custom-built for the culture of its web site. The perfect activity stream centers in on what is relevant to the user, and filters everything away. On an abstract level, it subtracts relevant conversations from everything that happens in a noisy room.

Activity streams attract momentary action
Activities in the stream are transitory, bringing a sense of immediacy to social media, as new activities bump current activities out of view in a relatively short amount of time. New activities attract momentary attention, and thus also call for immediate action. The problem is however that we do not always have time for immediate action, and by the time we do have time for action, the activity has slipped away to make room for new real-time updates. Activities in the activity stream are geared towards the present moment and the recent past 3.

Should activity streams be made searchable, permalinked, or catalogued in order to make them timeless, or can we do with the fact that the current state of the system represents the sum of all activities in the stream?

Make it easy to scan
Each activity type varies in design as they each describe different kinds of content. Make sure that different activity types have a similar look, so that it is easy for the eye to scan them.

When we judge if something is relevant to us, the actor plays a big role. We are most likely more interested in reading about our closest friend than our mother in law. This is why most activity streams have a separate column for only thumbnail avatars depicting an activity’s main actor.