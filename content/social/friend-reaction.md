+++
title = "Friend Reaction"
category = "social"
[extra]
type = "interactions"
icontype = "fas fa-hands-clapping"
+++

### Problem summary

The user wants to express their emotions in a simple way.

### Usage

* Use when you want to let users rate content without having to worry about the degrees to which they like it.
* Use when you want to provide an easy and informal way for users to provide information about their likes and preferences.

### Solution

Let users express their immediate emotions in a simple way.

Simplify rating controls by making them binary choices of emotional consent rather than fine-grained ratings of stars or scores. Use reaction activity to discover what content might be more relevant for your users.

### Rationale

Eliminating the fine-grain of stars and rating scores, this makes rating things easier for users as well as interpreting them.