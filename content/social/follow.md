+++
title = "Follow"
category = "social"
[extra]
type = "interactions"
icontype = "fas fa-bookmark"
+++

### Problem summary

The user wants to track and keep up to date with activity on topics or themes, not just people!

### Usage

* Use when you want to let users personalize the experience of your application to specific topics, themes, or people

### Solution

Allow users to select items that they want to stay up to date with. Let users follow topics, themes, channels, events, or people and show related updates in the user’s Activity Stream. Contrary to the Friend design pattern, users do not have to worry about how many of their friends are using the same service or if their friends share the same taste.

Users can select items (objects) which they want to stay up to date with. The most common object to follow is other people (friends), but other popular objects are channels, artists, and interests.

As a consequence of following, users can keep track of- and receive updates from the objects follows. Typically, updates are shown in users’ Activity Stream or used to suggest new, related, and undiscovered objects similar to what is already followed.

### Rationale

Users can gain access to a lot of varied content by “following” the activities and recommendations of other users and this pattern allows them to do so without having to worry about how many of their actual friends are using the application.

Content shared with followers on sites like Google+ and Pinterest makes the content curation community possible and users can choose to follow topics, events, themes or even people to get fresh content built by and around the channel being followed. For the same reason friend lists will become an increasingly important UI design pattern, so will the Follow pattern.

### Discussion

Contrary to the Friend design pattern, users do not have to worry about how many of their friends are using the same service or if their friends share the same taste.