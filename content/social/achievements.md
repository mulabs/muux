+++
title = "Achievements"
category = "social"
[extra]
type = "reputation"
icontype = "fas fa-champagne-glasses"
+++

### Problem summary

Some users respond to opportunities of winning and collecting awards that in turn can be displayed to other community members in order to increase engagement.

### Usage

* Use when you want to make your users explore parts of your product or service that might not otherwise appeal to them.
* Use when you want to make your users use all parts of your product or service and in a manner you intended.
* Use when your users need guidance in exploring your offerings.
* Use when you would like to promote certain parts of your product or service.
* Use when you would like to focus the user’s attention to certain parts of your product or service – for instance in the occasion of an event.
* Use when you would like to give your users an opportunity to achieve a sense status and accomplishment within a community.
* Use when you would like to give your users an opportunity to differentiate themselves from the community or integrate more with the community.

### Solution

Reward users for certain kinds of behaviour, for reaching specifically defined goals within the community

Some users respond to opportunities of winning, earning, and collecting awards that can be displayed to other community members. Construct a consistent family of collectibles that is achieved through mastering a healthy mix of difficulties. Unlock new achievements as easier ones are accomplished.

#### Families of collectibles
Let your users specialize in certain types of behaviour and show it off to fellow community members. If there are too many users for a single top 10 of the entire site, then create multiple top 10 lists that celebrates different specialities.

If you’re running a photography website and are rewarding users for uploading great photographs, consider creating specialized awards for different types of photographs instead of just having a best-of-the-best award. It takes different sets of skills to respectively create a great portrait shot and a great action shot. So why not just reward each type?

Similarly you could award users with a series of different awards focusing on giving feedback: “Best comment in this month”, “Highest rated reply in this month”, or “Elite reviewer”.

#### It should be attractive to be rewarded
The Yahoo Design Pattern Library talks about “fetishizing” the awards1. Users should strive to receive a trophy, badge, or achievement – both because it is a challenge to be awarded, but also because it just looks great!

Give people something to drool over; let people know what can be won. Let them know what are available for them, what they have to unlock, and what they have achieved already.

Attractive awards work better. Develop attractive trophies with beautifully designed icons. Some games use actual ‘game-pieces’ to represent each achievement.

Users should be proud to show their awards off. They should be proud to put them on display and be able to play around with their “show room”. If there are enough achievements to play with, you can enhance the user’s experience by letting him or her customize his or her “show room”.

*Combine easy successes with hard challenges*
Make some achievements very easy to accomplish, so that the user won’t loose momentum. Combine these with harder challenges that require more time and effort to achieve.

In games, it is often seen that new achievements are “unlocked” as easier ones are accomplished. Being awarded an achievement that is not readily obtained by a newcomer adds to the user’s feeling of status in- and belonging to the community.

#### Types of achievements
There are several types of achievements to reward your users with:

* *Temporal award*. This type of achievement celebrates the winners of one-time events. It includes a time-frame or interval: weekly-, monthly-, or yearly awards are good examples. This kind of trophy is valid, even years after it was awarded: once earned it is never lost. An example could be “Best photography 2009” or “Most points in August 2009”. These are useful for praising consistent top-performers and for giving a wider number of users an opportunity to earn a reputation9.
* *Top 10% trophy*. This trophy can be achieved just as fast as it can be lost. Whether this type of achievement belongs to a user depends on the status quo of the system. It represents the top X percent or number of community members in the moment, why users needs to maintain his or her activity in order to keep the trophy.
* *Participation trophy*. This kind of achievement is often used to link real-life activity with a member profile on a website. An example of such a trophy could be “Participated in the 2009 Christmas party” or if used for an online event: “Participated in the World Photo Challenge”.
* *Elite acknowledgment*. It gives a sense of arousal to know that you are better than somebody else. Being assigned “elite” status sparks motivation to further engage oneself into a community, as it does for the members still striving to achieve such status. Furthermore, elite members come out as more authoritative when they speak up, why it can be argued that they should reflect the community’s general opinion, so that they can serve as role models for newcomers.
* *Privileged member acknowledgment*. Certain members are so deeply involved in the community or maintenance of the site, that they are bound to be rewarded with a privileged member acknowledgment. Examples are “admin”, “staff photographer”, “moderator”, “staff”, or just “VIP”.
* *User-to-user awards*. This is not an achievement, but as they are often shown together with earned collectible achievements, they have been added to the list. These are small awards created by users (or pre-fabricated by the community) and represents gifts a user can give to another user.

#### What are you awarding?

What is your website about? And what object are you awarding? Are you awarding the user’s actions or his or her creations? Are you awarding the user or his or her objects?

At amazon.com and ebay.com, awards are used to indicate the trustworthiness of a user giving a review, selling or buying. At the Danish car-website vmax.dk, it is each car that can be awarded and not in particular the user. The owner of each car then gets to display his or her achievements for cars owned.

### Rationale

Achievements reflect a user’s reputation in a community. They are “both a history of one’s actions within that community, and a value judgment about the worth of those actions”.

Collectible achievements gives the user a feeling of ownership and belonging. It gives the user an opportunity to build an online presence that can be shown and admired by fellow community members – thus providing a sense of status in the community.

As the user collects achievements, he or she invests time in the community and builds up a history. This history with your site creates a barrier for the user to leaving, as what has been built up will be lost upon quitting. Translating the investment into visible collectible achievements helps the user to build and emotional bond the community that will reward you plentifully in traffic and loyalty.

### Discussion

#### Quality
To be awarded with an achievement the user must complete a series of tasks that defines the criteria of success. But how do you define a successful completion of a task?

Enforce some notion of quality in what it takes to complete a task. In games it could be to earn 1000 credits within 20 seconds, or to have conducted a certain action in a specific number of locations.

Award quality participation over repetitive activity1. Rather than rewarding for the 20th time a game has been played, reward for the 20th time a game has been won within one month.

A well-designed reputation system links the services that you offer your users to a set of goals that you have for your community2. Are the achievements you are giving out rewarding the behaviour you want – does it reward users for behaving like good citizens or just to keep activity going? Promoting poor quality activities will slowly kill your community8.

When you design your “Collectible Achievements” and how they work, be careful that you design them in line with the mindset of your community. Is your community collaborate rather than competitive? Do you want to promote some behaviour over other? Who in your community will be intrigued by your the achievements you offer and start collecting? Are you promoting the right behaviour or could your achievements actually end up promoting anti-social behaviour (or is that what you want?)? Will you possibly push parts of your community away (is that OK?)?

The design of your achievement system is closely related to defining what is “good” and “bad” behaviour in your community. By defining that an achievement is worth “10 points” and another is worth “20 points”, you have quantified and defined what is better behaviour. If you neglect to reward certain behaviour, that behaviour is inherently defined as “bad”.

Keep track of how your community reacts to the system of achievements you setup. Are they used and achieved in the manor you intended? A community needs constant nurturing and fixing – the specifications of each achievement are easy knobs to use to adjust and fix.

#### Get people started
To encourage people to try new and novel features you can reward users for their “first” achievements. For instance: “First video uploaded”, “First contributed document”, or “First review submitted”. Such achievements should however be worth less than more challenging goals.

#### Is it mine forever?
Can the achievement be taken away if your score goes down? Or do you want to keep it time specific? (For instance: Best seller in august 2009)

In prospect theory, it is found that people have a tendency to strongly prefer avoiding losses to acquiring gains. They are loss aversive7. Having built up a lot of achievements in a community gives the user a history with that site and creates a barrier to leaving: too much have been built up. A sense of belonging have been emotionally attached to the achievements.

#### Fraud
In large systems where it is impossible to monitor the activities of all users, it can be a good idea to implement a “report abuse” feature, so that members of the community can rat each other out, if they feel that somebody is manipulating the system in a malicious way.

Communities are all about feeling respected by fellow community members and the system as well. It’s about fairness. Unfortunately, fairness is a hard factor to build into a system built upon bits and bytes.