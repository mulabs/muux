+++
title = "Friend"
category = "social"
template = "patterns.html"
[extra]
type = "interactions"
icontype = "fas fa-address-card"
+++

### Problem summary

The user wants to form a mutually agreed connection with another person.

### Usage

* Use when you want to let users create a closed circle of people with whom they want to interact and share content with

### Solution

Let users form mutually agreed connections with each other.

The mutual agreed two-way connection of friending is in contrast to the one-way Follow pattern, and implies a connection of a more personal type. Having an online friend relationship often gives each party access to the exchange of more actions and information between them.