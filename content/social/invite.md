+++
title = "Invite"
category = "social"
[extra]
type = "interactions"
icontype = "fas fa-envelope"
+++

### Problem summary

The user wants to experience the application with their friends.

### Usage

* Use when the value experienced by the user will increase once the ability to interact with friends is possible
* Use when you want to provide an easy way for users to invite friends.

### Solution

Provide a way of letting users share an experience with similar others. Make the process of inviting other people to use the application simple and easy to complete.

We are more likely to agree to a request or invitation to perform an action from people we know and like than from strangers. Providing users with a way of connecting with and sharing the app with friends give them a better, more immersive experience even if just in terms of more content.

The Invite friends pattern is often built into an onboarding process or even as the Blank slate design.

### Rationale

If an experience is enjoyable or useful enough, users will want to share it with friends and similar others. If users aren’t connected to the experience already, they will need to be invited through an invitation.