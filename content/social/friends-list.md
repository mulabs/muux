+++
title = "Friends List"
category = "social"
[extra]
type = "interactions"
icontype = "fas fa-address-book"
+++

### Problem summary

The user wants to keep track of and engage a subset of their friends on the site in a meaningful way.

### Usage

* Use when you want to provide a way of exploring users of your system, often in combination with their user profile page
* Use when you want to give users an opportunity to find frequent contacts
* Use when you want to provide a way for users to find each other
* Use when you want to provide a way for users to interact with each other
* Use when you want to let users control who they share information with.


### Solution

Show a user’s connections or friends in a manageable list. This pattern is often combined with the Follow pattern.

### Rationale

Friend lists can be used to help users engage with your web application in a better way by keeping up with how other people they know are using the application.

Friend lists also come in handy when the users want to control who they share with. Whether it’s one-on-one communication or keeping track of someone’s tastes and preferences, the way users explore their blossoming friend groups will become increasingly contextual, requiring friends to become a more integral part of the content-consumption experience.