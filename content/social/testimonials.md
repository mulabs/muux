+++
title = "Testimonials"
category = "social"
[extra]
type = "reputation"
icontype = "fas fa-quote-left"
+++

### Solution

A formal statement testifying to the quality and trustworthiness of a product.

### Rationale

We tend to trust online reviews and testimonials as much as recommendations from people we know. Whether testimonials are presented with text, video, or include a picture of the sender’s face, they offer social proof giving your value claims more legitimacy.