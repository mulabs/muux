+++
title = "Chat"
category = "social"
[extra]
type = "interactions"
icontype = "fas fa-message"
+++

### Problem summary

The user wants to interact privately with other individuals or groups from within the system.

### Usage

* Use when you want to allow users of your system to contact other users of the system.
* Use when you need to provide a private person-to-person form of communication between users.
* Use when you want to provide a way for users to interact privately with other individuals or groups.

### Solution

Allow users to interact with groups, individuals, or the system through text messages. Elements of this design patterns often includes several screens to be designed:

* *Inbox*. A screen the lists the most recent messages received from other users
* *Outbox / Sent*. A screen that lists the most recent messages sent to other users
* *New message*. A screen that provides a form for the user to enter his or her message in and in turn click a “Send” button.

### Rationale

Chat as an interface is as old as the command line. Combining chatbots with the social interactions of direct and group messaging, chat as an interface can comprehend use cases as varied as paying your gas bill, deploying code, or building intimate social relations..