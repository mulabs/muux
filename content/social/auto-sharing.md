+++
title = "Auto Sharing"
category = "social"
[extra]
type = "interactions"
icontype = "fas fa-share"
+++

### Problem summary

The user wants to easily share their activity with their social networks.

### Usage

* Use when you want social sharing to be an integrated part of the flow.
* Use when you would like to lower the friction and make it easy for people to share their story on social networks.
* Do not use when information shared is personal or sensitive – when the user would be reluctant to share the content in the first place.

### Solution

Lets Allow users to quickly and easily share particular interactions with their social networks. Web applications like Tumblr, Spotify and Vimeo all have granular sharing settings, which allow users to automatically post updates to their networks based on their activity. These updates can be posted within the application or even shared with external social channels like Facebook or Twitter.

### Rationale

Auto-sharing help users engage with their friends and family in everyday activities like listening to a song or reading an article on an external website.

Furthermore, auto-sharing is a great way to build awareness and engagement within the application itself. For interactions like uploading a photo to Carousel or a video to Vimeo, this pattern makes it even easier for users by eliminating an extra step in the process which they are most likely going to take.

