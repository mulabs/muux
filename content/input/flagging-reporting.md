+++
title = "Flagging and Reporting"
category = "input"
type = "community"
[extra]
icontype = "fas fa-flag"
template = "patterns.html"
+++


### Problem summary

The user wants to mark inappropriate content for moderation

### Usage

* Use when your site is based on user generated content where you can’t possible moderate everything yourself because of the vast number of updates.

### Solution

Let users report content for moderation

### Rationale

For sites based on user-generated content and user interaction, flagging and reporting is a vital design pattern. Let users help discover content necessary for administrators to review for removal or categorization. Users are most often happy to help with the overwhelming task of surveilling and managing user-generated content produced in a community.
