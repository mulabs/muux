+++
title = "Expandable Input"
category = "input"
template = "patterns.html"
[extra]
type = "forms"
icontype = "fas fa-expand"
+++


### Problem summary

The user wants to experience a main interface with as much screen real estate and with a minimum of distractions

### Usage

* Use when you want the main focus to be on the main interface rather than its input controls
* Use when you want to keep distractions out of the main interface

### Solution

Expand the size of input fields as they come in focus or are filled with content

Design your controls in two modes: expanded and contracted. As the user taps a contracted control, it expands to its larger size. This can help keeping secondary functions out of the way until the user is in need of them.

### Rationale

Expandable inputs can help unclutter user interfaces by staying out of sight until needed. For multiple purpose user interfaces, it can be helpful to let optional actions, such as searching, posting, or commenting, attract a minimum of attention.