+++
title = "Steps Left"
category = "input"
template = "patterns.html"
[extra]
type = "process"
icontype = "fas fa-list-check"
+++


### Problem summary

The user is about to go through the process of filling in data over several steps and is in need of guidance.

### Usage

* Use when a user goal can be split into a series of smaller steps that is easier displayed on separate screens.
* Use when the steps of a process is so long that the user might get the impression that it will go on forever without the guidance of steps.
* Do not use when there is only 1 or 2 steps in submitting data to the website.
* Do not use when the process of filling out data is easily foreseeable.

### Solution

Add a navigation block describing the steps involved in submitting data to the system. The block should always appear on the page. As the user progresses through the process, the navigation block is updated accordingly. The current step is highlighted, giving a clear indication to the user how far they have come and how much further there is to go.

Remove unnecessary distractions like extra navigation, advertisements, and the likes.

### Rationale

The Steps Left pattern is used when it is critical to maintain the user’s focus throughout the process of filling in data to the system. This is for instance critical in e-commerce websites, where the checkout process is often guided by this pattern. In e-commerce websites, the checkout process is the most critical part of the site, as this is the part that captures the customer’s money. The Steps Left pattern provide the user with a great overview of how far in the process the user has gone: it provides a visible end to the process, which the user can aim for.

This pattern is similar to the Wizard pattern most commonly found in desktop applications, which guide the user step by step.
