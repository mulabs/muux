+++
title = "Wiki"
category = "input"
template = "patterns.html"
[extra]
type = "community"
icontype = "fas fa-book-open"
+++


### Problem summary

You want to create a repository for your website or application where users can produce and manage information while collaborating on public content.

### Usage

* Use when you want the user to be able to contribute to a page by adding or editing the content of the website.
* Use when you have a collection of documents you want to keep updated over time.
* Do not use when publishing editorial content: content that stems from the publishers of the page that is not to be tampered by anybody else. For instance the terms and agreements of a website or other legal information.
* Do not use for content that ages – that is relevant for a certain time period only (news, blog posts, etc.)

### Solution

A wiki is a page concept itself, and not just a pattern that functions as a part of a website. The format however represents enough value in itself to represent a design pattern and not just a page concept.

A wiki page can be edited by anyone. Anyone can modify information and add new pages to the document collection. All pages are under version control, and can easily be rolled back to earlier versions. A wiki allows users to easily create, edit and link web pages together.

A wiki enables documents to be written collaboratively, in a simple markup language using a web browser. A single page in a wiki is referred to as a “wiki page”, while the entire body of pages, which are usually highly interconnected via hyperlinks, is “the wiki”. A wiki is essentially a database for creating, browsing and searching information. [Wikipedia.org](http://en.wikipedia.org/wiki/Wiki)

### Rationale

Wikis are often used to create collaborative websites, power community websites, and are increasingly being installed by businesses to provide affordable and effective Intranets or for use in Knowledge Management. 