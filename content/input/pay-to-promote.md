+++
title = "Pay to Promote"
category = "input"
template = "patterns.html"
[extra]
type = "community"
icontype = "fas fa-rectangle-ad"
+++


### Problem summary

The user wants to pay to prioritize own content above the regular content feed in order to gain increased reach and traction.

### Usage

* Use when you want to provide users and businesses a chance to skip the content prioritization algorithm to get in front of the desired audience.
* Use when you want to provide users an opportunity to elevate content on feeds for a fee

### Solution

Let users pay to promote their content

On social platforms like Quora, Twitter, OKCupid, and LinkedIn, users can post content. Allow users to boost visibility of their own content by paying money. This form of advertising allows users to gain traction while maintaining a look and feel native to the platform.

Sites like Quora and Facebook allow users to boost their posts by paying money, which in return gives them greater visibility in the content feed above the regular non-paid content.

Dating-sites like OKCupid allow users to boost their profile in views. LinkedIn does the same albeit as part of a paid membership plan rather than by individual content like on Facebook.

### Discussion

The Pay to Promote pattern is part of the native advertisement concept, which blends regular content with paid content. The two look the same with the only difference of the paid kind having a “Sponsored” label or “Advertisement” label.
