+++
title = "Live Preview"
category = "input"
template = "patterns.html"
[extra]
type = "forms"
icontype = "fas fa-magnifying-glass"
+++


### Problem summary

The user wants to check how changes in form fields affect an end result as quickly as possible.

### Usage

* Use when you want to provide the user with a real-time preview of what he or she is creating
* Use when it is hard for the user to comprehend how the final output will be without having a preview to reference
* Do not use when the input is straightforward and the resulting output does not depend on a specific layout

### Solution

Let users preview consequences of an action before committing to it.

Update a preview of what modifying a form will result in throughout the entire interaction with the form. Instead of waiting for the user to submit the form, the changes are shown immediately in a preview. Each user event of significance results in a browser-side processing.

### Rationale

Previews make it easier for users to decide whether or not to commit to a change and thus invite safe exploration and playful creativity. Show feedback immediately in live previews to further spark fun, play, and exploration.

The result is increased interactivity. The user does not need to wait for page reload on a form submit to find out whether data was inputted correctly into the form. The feedback is immediate.
