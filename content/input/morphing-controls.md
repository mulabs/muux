+++
title = "Morphing Controls"
category = "input"
template = "patterns.html"
[extra]
type = "forms"
icontype = "fas fa-volume-high"
+++


### Problem summary

The user wants to only be presented with controls available to the current mode

### Usage

* Use when some controls don’t need to be displayed at the same time
* Use when you want to declutter the screen with unnecessary and unavailable controls
* Use when your user experience contains several modes (playing/pause, on/off)

### Solution

Information presented and actions available in a user interface element depend on its mode. When a video is paused, the play command is available, but pause is not.

Design affordance in each mode toward the most common or wanted interactions and emphasize asymmetry, incompleteness, or something being wrong to push users toward changing modes.

Be sure to keep a consistent look between each state of the control that morphs. Font and text size should stay the same, but colors may differ.

Morphing Controls work well with binary actions, such as:

* On/Off
* Like/Unlike
* Follow/Unfollow

