+++
title = "Autosave"
category = "input"
template = "patterns.html"
[extra]
type = "forms"
icontype = "fas fa-cloud"
+++


### Problem summary

The user wants to keep their data safe and saved while focusing on working without having to remember to do so.

### Usage

* Use when you want to help the user focus on producing great content without worrying about whether their data is safe and saved.

### Solution

Prevent accidental data loss by automatically saving user input at fixed intervals or at events of interest.

Consider at what frequency it makes sense to auto-save inputted content for your application, and at what events it makes sense to trigger an auto-save. The most obvious event is clicking the save-button, but the event of moving the focus to another field might also be interesting to observe.

#### The save button

To better guide users as to what state their document is in, consider changing the label of the save button from “Save”, when the form contains uncommited changes to “Saved” when the current form represents what has been saved in storage.

### Rationale

Let users worry about creating great content rather than about loosing it. Removing the save button entirely can create fear, so consider keeping it around to make users feel safe. Leave an unobtrusive trace of conducted user actions and consider complementing with the Undo pattern.

### Discussion

When implementing auto-save functionality, consider not removing the “Save” button. Users tend to panic, when there is no “Save and close” button. Even though the “Save” button does nothing or only closes the window, it provides confidence and reassurance in users that their data is safe. It’s an issue of trust. Did the system hear me? Am I sure that I am sure?
