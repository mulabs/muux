+++
title = "Undo"
category = "input"
template = "patterns.html"
[extra]
type = "forms"
icontype = "fas fa-rotate-left"
+++


### Problem summary

The user wants to revert a mistaken input

### Usage

* Use when you want to provide users with more confidence and willingness to play around
* The more costly it is to lose data, the more important it is to provide undo.
* Whenever there is an opportunity to lose work, the program should allow undo actions.
* Never use a warning when you mean undo.

### Solution

Allow users to easily reverse their own actions

### Rationale

Users aren’t perfect – they tend to make mistakes.

Promote safe exploration and playfulness by providing confidence that mistakes aren’t permanent. Multi-level undo lets users incrementally construct and explore work paths quickly and easily. The more costly it is to lose data, the more important it is to provide the opportunity to undo.

### Discussion

A great side effect of utilizing the Undo pattern, is that it provides users with more confidence and more willingness to try new and different things. By promoting trial and error, users are able to learn and master your application more quickly.

