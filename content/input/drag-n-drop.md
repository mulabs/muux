+++
title = "Drag 'n Drop"
category = "input"
template = "patterns.html"
[extra]
type = "forms"
icontype = "fas fa-fill-drip"
+++


### Problem summary

The user needs to perform operations on one or more objects by moving them from one place to another.

### Usage

* Use when you want to let users to perform more complex tasks through direct manipulation – through a What-you-see-is-what-you-get (WYSIWYG) approach.
* Use when you want to avoid forcing the user to go to another page in order to re-arrange the layout

### Solution

Let users pick up and rearrange content by dragging it across the screen

### Rationale
Instinctively, many users try dragging and dropping objects in user interfaces. This conceptual metaphor with clear ties to the physical world provides a level of direct manipulation few methods can match. It is seen as one of the most effective ways to rearrange items in a list, move objects from one place to the other, or even upload files.

### Discussion

As Drag and drop is not fully accessible you may want to consider supporting alternate ways to re-arrange your content modules.


