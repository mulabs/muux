+++
title = "Rule Builder"
category = "input"
template = "patterns.html"
[extra]
type = "forms"
icontype = "fas fa-clipboard-question"
+++


### Problem summary

The user needs to, often repeatedly, conduct a search query based on a custom set of rules

Example
* binding a remove rule button to each rule and of providing an input for setting the matching criteria for the rules

### Usage

* Use to build a search query based on several conditions
* Use to save a segment rather than static selection
* Use to help users build dynamic lists, making the selected rules static rather than the actual search result
* Use when a search query involves an unknown number of undefined conditions
* Do not use when users can find what they need using only a text input.
* Do not use when the type of search query requires the same types of variables to be chosen every single time.

### Solution

Let the user build a dynamic list of rules to narrow down matching results from a dataset. Each rule is represented by a separate line or box and divided from each other vertically.
Any or all

The user must specify whether a rule is needed (AND) or is optional (OR). A common approach to simplify the implementation of this pattern is to provide an option to choose whether all rules chosen should be matched – or just any of the rules.

A more nuanced approach is to allow each rule to be a required match or just an optional match.
Treat each rule type differently?

As the user choses the kind of rule he or she wants to impose, those rules can have very different impacts on what corresponding data needs to be entered for the rule to give meaning.

This is why many rule builders provide smart mini-forms that vary depending on what kind of rule is selected. One rule might impose a text search, wherein options like “contains”, “does not contain”, “matches”, or “do not match” make sense. Other rules could be to select an option from a dynamic list or to provide numeric- or range inputs.
Adding and removing rules

The smartest feature rule builders provide is to dynamically add and remove rules.

The “Add” button is typically located either directly under the rules and remains so as new rules are added or removed, or it located alongside the remove rule button. In the case of the latter, this allows for new rules to be inserted directly beneath a desired rule.

Removing a rule is most often allowed at the beginning or end of the line the rule is presented on.

### Rationale

A rule builder allows a user to specify unique conditions to discover and group items across one or more datasets.

The results returned by an active rule can be dynamic in nature as its related datasets change over time. An active rule can trigger a set of actions to be performed when the conditions, criteria and values of the rule are met. Rules sets can be grouped into discrete units and linked together with condition logic to create highly complex rule conditions.
