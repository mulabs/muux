+++
title = "Keyboard Shortcuts"
category = "input"
template = "patterns.html"
[extra]
icontype = "fas fa-arrow-down-up-across-line"
+++


### Problem summary

The user wants to perform repetitive tasks faster

Information about available keyboard shortcuts for the Dropbox.com online web interface is displayed by pressing "?"

### Usage

* Use when your application contains repetitive tasks that involves switching modes from keyboard to mouse.

### Solution

Allow users to trigger actions faster with keyboard commands.

Typically keyboard shortcuts are made for commands that are part of frequent or repetitive user tasks.

When adding shortcuts to your application, keep away from using existing system shortcuts or shortcuts that already used elsewhere in another context in the same application. Avoid repurposing shortcuts that users have already adapted into their workflow1.

You might want to consider adding keyboard shortcut information to menu items and button and icon tooltips, if available.

### Rationale

Ease access to repetitive tasks by providing skilled users with keyboard shortcuts to their associated actions. Reduce the total time spent, the steps needed, and mental energy wasted to complete a task without making it harder on novice users.

Keyboard shortcuts accelerate exposition of program function to users through keypresses rather than mouse clicks. This can greatly help speed up task completion time as the user does not need to switch modes from using they keyboard to the mouse; hands can stay on the keyboard.

### Discussion

* *Keyboard shortcut strategies*
* *Mnemonics*

Assist user recall memory by creating mnemonic associations with letters used in key combinations. However, as there are only so many keys available, a common strategy is to use the middle or end letter of a function rather than the initial letter.

On Macs, O is used for “Open File” while L is used for “Open Location”.

* *Speed*

Finally, placing keys of command pairs close to each other, even though there is no mnemonic association can make sense as well. C is often used for “Copy” while V is used for Paste.
