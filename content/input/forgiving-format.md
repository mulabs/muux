+++
title = "Forgiving Format"
category = "input"
template = "patterns.html"
[extra]
type = "forms"
icontype = "fas fa-sort"
+++


### Problem summary

The user needs to quickly enter data into the system, which then in turn interprets the user's input.

### Usage

* Use when the more explicit select boxes, radio buttons, and checkboxes make entering data a too complicated process for the user to engage with the system.
* Use when the time it takes to complete a task proportional exceeds the importance of the goal the user wants to accomplish.
* Use when the input you want to collect is relates to one topic. For instance a physical location or an event with a given start time.
* Use when the expected user input can be somewhat easily interpreted by a computer program.
* Do not use when the user can possibly ask or input anything. Only use for a narrowly defined purpose.

### Solution

Allow users to enter text in their own format and syntax, and let the system interpret it intelligently

Let users focus on getting things done rather than typing in things correctly. Lower the barrier for users to interact by allowing a broad spectrum of formats and syntaxes to be inputted. Consider nudging users to provide more easily interpreted information by paying attention to how you ask for input.

Transfer the problem inputting data from a user interface problem to a programming problem. Behind the scenes, an interpreter checks for different word patterns, and converts them into a formatted value.

### Rationale

Using the forgiving format pattern saves space and decreases the barrier for the user to interact with the system.

Depending on how widely defined the input topic is, it can be increasingly hard for the backend program to interpret the input field. The success of this pattern has much to do with how information requested – how the user is prompted.
