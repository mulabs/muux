+++
title = "Completeness Meter"
category = "input"
template = "patterns.html"
[extra]
type = "process"
icontype = "fas fa-bars-progress"
+++


### Problem summary

The user wants to complete a goal but needs guidance in when it is reached and how to reach it.

### Usage

* Use when you want to keep the user on track when completing a specific goal.
* Use when you want to ensure that your users complete a set of minimum tasks that make up their presence.
* Do not use when the end-goal is dependent of a series of sequential tasks.
* Do not use for critical goals, but rather for goals that would be nice to have reached. The idea of this pattern is to make the user perform a few more tasks than he or she would normally do.

### Solution

Let users gauge progress toward reaching an end goal. Divide the end goal into smaller sub-tasks, and increase the percentage of completeness as each task is completed.

Divide and end-goal into several sub-tasks. The end-goal can be arbitrarily defined, such as “Completeness of your profile” or “Elite member”. As each sub-task is completed, the percentage of completed tasks goes up – reaching 100% when the goal is finished.

It is often seen that along with stating the progress of the goal (for instance: “34% done”), one or more links or hints to how the progress can be improved is also provided. This will help keep the user on track and immediately move to the next task once one has been completed.

There are several approaches to presenting and celebrating an end-goal state. One option is simply to indicate that all tasks have been completed (as in “Your profile is complete!”) along with a “100%” mark. Another is to award the user with a collectible achievement: a badge, trophy, or similar award that he or she can decorate his personal profile with and show off to his or her friends.

A third way to celebrate completing the goal and its sub-tasks is to announce it in his or her profile feed, or even on a centralized site-wide feed.

### Rationale

Extrinsically motivate users by triggering their desire for achievement, curiosity, and completion by providing a feedback loop that lets users gauge their progress toward reaching an end goal.

This pattern uses a set of psychological drivers that pushes the user to move forward towards the end goal.

One is curiosity. We are curious to find out what happens when we reach 100%. Will I be rewarded or will my profile look different?

Another is the feedback loop. As the user completes sub-tasks, his or her progress moves towards 100%. A clear link between completing tasks and reaching the end goal has been established.

### Discussion

As humans, we feel inclined to complete the goals we have decided to start. Most often we choose goals for ourselves based on what we want to spend our time on, afterwards we put our minds into it.

The Completeness Meter pattern is an attempt to present such a goal to the user in order for him or her to decide on completing it. By presenting easily completed sub-tasks, it is possible to convince and persuade the user into spending time he or she in other circumstances wouldn’t have.

