+++
title = "Input Prompt"
category = "input"
template = "patterns.html"
[extra]
type = "forms"
icontype = "fas fa-keyboard"
+++


### Problem summary

The user needs to enter data into the system

### Usage

* Use when the label of an input field does not fully explain what should be filled into it or when using such a label feels like over-explaining the interface.
* Use when an example text or question answers what should be filled into an input field just as well as a label.
* Use when you want to save the space that a label otherwise takes up.
* Use in combination with a label, to further explain what kind of input is needed.

### Solution

An input field is pre-filled with example text or a question that prompts the user with what to do or type.

The Input Prompt pattern is most successfully used with dropdown lists and text fields. As dropdown lists have a fixed set of choices, words like Select or Choose are used for prompts. For text fields, the prompting string often begins with a call to action: Enter, Type, Search. End the string with the noun the input is describing, for instance Enter city or Enter an address.

Text fields use the Input Prompt pattern combined with scripting to remove the prompt text from a field, when the user’s focus is set. Once the user enters the input field to type in content, the prompting text is removed and replaced with nothing so that the input field is free for the user to fill out.

### Rationale

When a user fills out a form it is most often with the purpose of filling it out as quickly as possible to get on with the service offered. This is why the user often just scans through form fields and labels without giving the labels much of a glance. By using input prompts, immediate attention is drawn to what the user needs to fill in. The user can’t miss it. Although you must beware of removing labels entirely, as the input prompt is removed once focus has been set to the text field.

Input prompt is often used for small forms that are key to the core functionality of a site as inserting the label inside the text field itself helps save space. For more elaborate forms, there is often more than enough room available to explain each input field.

