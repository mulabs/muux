+++
title = "Vote to Promote"
category = "input"
template = "patterns.html"
[extra]
type = "community"
icontype = "fas fa-ranking-star"
+++


### Problem summary

The user wants to promote a specific piece of content in order to democratically help decide what content is more popular.

### Usage

* Use when you want your users to democratically decide what is interesting content.
* Use when you want your users to democratically submit content to your website.
* Use when you want to trust your users subjective opinion
* Do not use when your website has a small community. A large user base and a strong community is important so that a sufficient amount of votes can be generated and meaningful comparisons can be made.

### Solution

Let users participate in content curation by letting them promote quality content.

Use the power of your community to help curate what is more popular. Display a voting mechanism next to each candidate item. As users click, their vote is counted in favor of promoting that item. Consider providing an embeddable and stand-alone voting mechanism that third-party publishers can include on their site.

### *4 Mechanisms working together*

This pattern consists of a number of mechanisms that work together:

* Voting mechanism. Provide a mechanism whereby users can vote for or against each item of content on your website. A user gets one vote and can change that vote at a later time. When a user casts a vote on an item, this information should be provided back to the user as feedback. Let the user see his or her prior votes and in some cases allow the user to change those votes.
* Display number of votes an item has received. This will give your visitors a clear indication of how popular an item is and allow for comparison with other items.
* Sum up popular items. Provide lists of popular content summed up on a main page.
* Favor popular items. Favor popular items in search results, when browsing tags, and showing related information.
* Content submission mechanism. You can let users submit content in several ways.

#### Let users submit

*Provide a webpage with a submission form**

The most basic and traditional way to let your users submit content is via a form on a webpage that you host. After content has been submitted, your users can freely vote on the submitted content’s quality.

#### Make voting embeddable

*Provide a widget for the user to to place on his or her website* 

If the type of content your users are submitting, you can provide a widget to your users to place on their own website. This will allow third-party publishers to submit content directly from their own website. The widget is really a javascript include code, that will add the address of the webpage, if the webpage has not been added to your site yet.

### Rationale

The Vote To Promote pattern promotes community participation and can potentially help pick up and promote the newest and hottest content around. By using your community to judge what is more popular, you avoid the need to hire paid professional reviewers.

### Discussion

Using the Vote To Promote pattern on your website brings the user to the center of your site. It is used to implement a democratic control over the content of your website with the following pros and cons.

* Is democratic control the best mechanism for promotion if all users do not share the same opinion among each other
* Is democratic control the best mechanism for promotion if all users do not share the same opinion as your site or product
* Is democratic control the best mechanism for promotion if all users do not possess what you believe is good taste.

Each user votes from their perspective of what defines good quality: their opinion is subjective. This raises the question whether the numeric number of votes on each item can really be compared and if the item with the most positive votes really is the item with the highest quality.

This dilemma raises the question whether some users should have more authority than others: if some users’ votes should count more than other users’ votes. This might be necessary if you want your site to retain the definition of quality that you like. Popular content is not necessarily the same as quality content.

Consider a number of measures to prevent users from misusing the system:

* Limit user’s activity. Limit the number of votes a user can cast for a given time-period.
* Watch for malicious activity against specific users. A user might get in a malicious mood and start demoting another user’s content. Limit the number of times a user can demote another user’s content.
* Some votes count more than others. Consider giving votes from users to their friends less value than to strangers.
* Some content is more important than others. Consider promoting original content rather than citing content.

#### Wisdom of crowds

When the amount of content submitted each second is larger than what your staff can handle, you will have to rely on what is referred to as the “wisdom of crowds” to decide what is good content and what is not (spam, untrue stories, “lame stuff”, bad categorizations, etc.). This could possibly work, if each user evaluated the quality of an item of content isolated from what other users have already voted.

However, it is not always the case that voting is done in isolation. Instead, crowd members communicate and affect each other’s qualitative judgment towards the lowest common denominator of opinion. The reason for this is the mix up of using measurable values when judging quality. The crowd has no wisdom: it will always be affected by the lowest common denominator of what others have voted.
