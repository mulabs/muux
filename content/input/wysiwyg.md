+++
title = "WYSIWYG"
category = "input"
template = "patterns.html"
[extra]
type = "forms"
icontype = "fas fa-blog"
+++


### Problem summary

The user wants to create content that contains rich media and formatted text but does not the knowledge or time to write HTML.

### Usage

* Use when you want to give the user a clear indication of what their content will look like when it is published. WYSIWYG is an acronym for What-You-See-Is-What-You-Get.
* Use when the users of a site are not comfortable with formatting text using HTML codes or a markup language such as textile or markdown.
* Use to lower the barrier of letting your users add formatted content to your site.
* Use when you want an easy way to integrate media into the formatted content of your site that does not require HTML or any other syntax knowledge.
* Use when the user is inclined to spend time correcting small details on their content. WYSIWYG allows users to see formatted results as they edit, this increases their confidence and allows for quick and good looking results.
* Do not use if you want to keep the HTML syntax clean. Many WYSIWYG editors are known to produce bloated and untidy HTML code.
* Do not use if you want the editor to function in all browsers. WYSIWYG editors rarely support all browsers, and if they do, it is often only the latest releases of the browsers.

### Solution

There are many javascript libraries available online that will convert a `<textarea/>` HTML element into a fully functioning WYSIWYG editor. The editor displays a work area that is both input and the final formatted output. The content is stored as HTML in a database.

Editors can be customized to your user’s needs. You can disable unnecessary functions. You might choose to not allow image inserts, tampering with font color or size – or even force the user to only use a predefined list of CSS classes.
###Rationale

WYSIWYG (What You See Is What You Get) was initially introduced in word processors such as WordPerfect and Microsoft Word. It was then a revolutionary way to write documents, where the editor on the screen mimicked the result in print.

Recently, WYSIWYG editors were introduced to forms on the web. Previously, long text was inserted into `<textarea/>` fields, with no formatting options what-so-ever. WYSIWYG editors now allow the input to mimic what will be seen on screen.