+++
title = "Good Defaults"
category = "input"
template = "patterns.html"
[extra]
type = "forms"
icontype = "fas fa-dove"
+++


### Problem summary

The user needs to enter data into the system, where some input values are most likely to match default values.

A typical use of the Good Defaults design pattern is at travel sites. Here, data like dates, trip type, and price range is pre-selected to what most other users typically select. Good Defaults provide ease of use and a quick start to searching for your next trip.
### Usage

* Use when the user has to choose among many options, where some are most likely to match the default values chosen by other users.
* Use when it is possible for the system to make qualified guesses regarding what the user might choose.
* Use when it seems a complicated task to fill out the form without the default: when the number of required choices hinders the user from finishing filling out a form.
* Do not use for input fields that are important for the user to think about. For instance for signing up to a newsletter or accepting the terms and agreements of a website.

### Solution

Pre-fill form fields with best guesses at what the user wants.

Drop down boxes and text fields are prefilled or preselected with reasonable default values. The default values are intelligent guesses as to what the user would possibly select.

When appropriate, reduce the cognitive load on users by pre-filling forms with default values. Use contextual information to make intelligent guesses as to what the user would most likely select. Do so only when you are reasonably sure your users would agree with your default values – otherwise, you will create extra work. Pre-filling controls to your own benefit rather than your users’ will most often backfire.

### Rationale

By providing default values in often complex forms with many choices, you save the user from the hassle of selecting all the relevant choices. Filling out a long form can sometimes be enough reason for the user to go somewhere else, where the process is easier.

The default values might not be right, but at least you provided the user with an example that he can change with as much effort as he would have put in if there was no example.