+++
title = "Feedback Loops"
template = "patterns.html"
[extra]
icontype = "fas fa-door-open"
+++

### Problem summary

We are influenced by information that provides clarity on our actions

### Usage

* Use when you want to enable users to react immediately on feedback from previous actions.

### Solution

Allow people to play interactively with information so they can adjust their behaviour and future actions toward reaching a greater goal. Use numeric data to show progress and translate data into analogous visual information.

Provide measures toward letting users know how what they are doing is affecting the system. This in turn allows users to adjust their behaviour and future actions toward reaching a greater goal.

### Rationale

Every action creates an equal opposite reaction. When reactions loop back to affect themselves, a feedback loop is created.

### Discussion

There are two types of feedback loops: positive and negative. Where positive feedback amplifies system output, resulting in growth or decline, negative feedback dampens output, stabilizing the system around an equilibrium point. In game design, Fixed rewards are often used as a basis for providing positive and negative feedback loops.

* *Positive feedback loops* Positive feedback loops are effective for creating change, but generally result in negative consequences if not moderated (or dampened) by negative feedback loops.

* *Negative feedback loops* Negative feedback loops are effective for resisting change and thus acts as a dampening effect keeping positive feedback loops to go out of control. Negative feedback loops typically act as stabilisers.

#### Things are connected
Changing one variable in a system will affect other variables in that system and other systems. A designer must not only consider particular elements of a design, but also their relation to the design as a whole and the greater environment1.

Consider positive feedback loops to act as systems for change, and include negative feedback loops to prevent runaway behaviours that lead to system failure. Consider negative feedback loops to stabilize a system, but beware that too much negative feedback can lead to stagnation.

The four distinct stages of a feedback loop:

* *The evidence stage:* A behaviour must be measured, captured, and stored. The individual needs to know where he or she stands. You can’t change what you don’t measure.
* *The relevance stage:* The information must be relayed to the individual, not in the raw-data form in which it was captured but in a context that makes it emotionally resonant. Through information design, social context or some other proxy for meaning, the right incentive will transform rational information into an emotional imperative. The user needs to know if he or she did good or bad.
* *Consequence:* Even compelling information is useless if we don’t know what to make of it – if it doesn’t tie to into some larger goal or purpose. This is why we need consequence. The information must illuminate one or more paths ahead. People must have a sense of what to do with the information and any opportunities they will have to act on it.
* *Action:* There must be a clear moment when the individual can recalibrate a behaviour, make a choice, and act. Then that action is measured, and the feedback loop can run once more, every action stimulating new behaviours that inch us closer to our goals. The individual has to engage with all of the above and act – thus closing the loop and allowing new action to be measured.