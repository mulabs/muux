+++
title = "Trigger"
template = "patterns.html"
[extra]
icontype = "fas fa-tags"
+++

### Problem summary

Place cues on our regular paths to remind and motivate us to take action

### Usage

* Use to remind or motivate users to take action

### Solution

* *Spark motivation* In situations where users have the ability, but lack the motivation, increasing motivation is your go-to strategy. Highlighting fear, inspiring hope, or inducing a sense of belonging are well-tested strategies to spark motivation.
* *Make hard things easier* In situations where users have a high motivation, but lack ability, facilitating behaviour is your weapon of choice. Where training people is hard, costly, and takes time, simplifying tasks is a more effective strategy in leveling required ability with actual ability. A third option is to scale back the ambitions of a target behaviour to something smaller. Starting small, first, will provide momentum for something bigger, later.
* *Provide a signal* In situations where users have both high motivation and high ability to perform a target behaviour, a signal, for instance a simple reminder, is enough. A signal could be a traffic light, request, call to action, a cue, email, tweet, or other distraction.

### Rationale

Without an appropriate prompt, behaviour will not occur – even if we are both motivatated and able. Timing is key. When we are ready to perform a behaviour, a well-timed trigger is a welcome distraction. A trigger is successfull when we notice it (so we can act on it), when we associate it with the target behaviour, and when it comes at a time where we are both motivated and able to perform the behaviour. Triggers can be external (an alarm sounding) or in form of an internal cue (walking through the kitchen triggers opening the fridge).

Triggers cue the user to take action in the context they are in. Triggers might be notifications, tweets, emails, text messages, links, or other distractions. Offline triggers should also be considered. Triggers can be set (alarms), brought home (printed reminder sheet), or follow an action (ask someone a question).