+++
title = "Simulation"
template = "patterns.html"
[extra]
icontype = "fas fa-chalkboard-user"
+++

### Problem summary

Give people first-hand insight into how inputs affect an output

### Usage

* Let users explore and experiment in a safe and non-threatening environment
* Use to show the link between cause and effect clearly and immediately
* Use to persuade in subtle ways, without resorting to preaching

### Solution

* *Simulate cause-and-effect* Clearly and quickly show cause-and-effect relationships to allow users to explore and experiment without being overly instructional.
* *Simulate an environment* Create situations that reward and motivate people for a specific behaviour as you allow users to rehearse and practice a target behaviour in a safe setting. Simulating an environment can help you facilitate role-playing, adopting another person’s perspective, and control exposure to new or frightening situations.
* *Simulate context* Make the impact on normal life clear by creating a simulation of a context. Although it might be costly and complicated, simulating the context itself will make the persuasion less dependent on imagination or disbelief and individual implications clearer.

### Rationale

Simulation enables users to observe the link between cause and effect in real time. The rules used to simulate outcomes are based on assumptions and as such have persuasive power. Simulation can serve as a great learning tool, as it enables users to try out behaviour they wouldn’t dare in the real world.