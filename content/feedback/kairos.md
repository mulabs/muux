+++
title = "Kairos"
template = "patterns.html"
[extra]
icontype = "fas fa-ice-cream"
+++

### Problem summary

Communicate to users in situations that are the opportune moments for change

### Usage

* Use when users respond better to feedback in some situations over others.
* Use when users are more likely to change behaviour or make critical decisions in some situations over others.
* Do not use when feedback has the same effect whether it is delivered now or later.

### Solution

Kairos is “a passing instant when an opening appears which must be driven through with force if success is to be achieved”. Kairos are situations of change.

Kairos occur in many situations – some are random but others come in patterns. One such pattern is when a user exits one group to join another. Such situations are potentially valuable as they represent moments where users are open and receptive to making a deal or making a behaviour change.

From an advertising and e-commerce perspective, karios situations are great open opportunities to tailor specific messages and offers to users.

Kairos are moments to strike! Your chance of success in delivering your message to the user is far better when you utilize Kairos than just any time.

Kairos comes in patterns:

* *Patterns of change* A good place to look for patterns are when larger changes occur. Look for “patterns of change”. Direct your attention when users are ready to buy their first car, move away from home, buy their first home, buy their first furniture, return their first furniture, when their first baby is born, and when their baby is moving away from home. These are events happening only once for the individual, but all the time for users when seen as a community.
* *Patterns of recurring behaviour* We have a tendency to conduct the same behaviour over and over again. We make the same mistakes again and again, shop the same places, and repeat the same decisions. It’s just easier and more comforting than spending energy on making a rational informed decision – or changing behaviour. These tendencies form “patterns of recurring behaviour” that can act as opportunities for you to tailor messages and offers that make it easier for users to conduct their regular habits.

#### Map it out
Map out what situations of change and recurring behaviour that you can possibly support and build experiences to support them.

* *Map patterns of change* Map patterns of opportune moments across your user base. Typically such moments are in situations of change or transition. These moments are potentially valuable as they represent moments where users are open and receptive to making a deal or changing a behaviour.
* *Map patterns of recurring behaviour* We tend to conduct the same behaviour over and over again. We make the same mistakes, shop the same places, and repeat the same decisions. Consider these moments opportunities to tailor messages and offers making it easier for users to conduct routine.
* *Map seasonal patterns* Seasons and traditions come in patterns where we consider a fresh start. Be it new years resolutions or the summer holiday where we reconsider our life.

### Rationale

Kairos is ancient greek for “the right, critical, or opportune moment” for action. It is the passing instant when an opening appears, which must be driven through with fource if success is to be achieved. Relevance, recent events, and who the audience is plays a role in determining the right moment to act. Map them ot.

Kairos represents situations of change. Situations of change are potentially valuable because they represent moments where users are open and receptive to making a deal or changing a behaviour.

By striking at the exact right time, the chances of successfully being effective in delivering your message an in turn create sales, change behaviour, or in other way influence users’ decisions are much larger than striking at any other time.

Utilizing Kairos is a step away from broadcasting your message to everybody all the time toward tailoring your message to the individual at the right time. The same message is relevant to different people at different opportune moments.