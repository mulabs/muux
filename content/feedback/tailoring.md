+++
title = "Tailoring"
template = "patterns.html"
[extra]
icontype = "fas fa-shirt"
+++

### Problem summary
Adapt the offerings of a system to match individual users’ needs and abilities

### Solution
Tailor information to users individually. Content will be more persuasive if it is tailored to the individual needs, interests, personality, or usage context.

* *Boost credibility* Tailoring the user experience has been proven to lead to increased perception of credibility. A website is seen as more credible when it acknowledges that an individual has visited before.
* *Personally tailor information in real time* Provide information that matches the personal needs, interests, personality, or goals of the user. The more relevant the message is to the individual the higher persuasive power it will have.
* *Tailor for the context* The more you can utilize the context and intention of the individual, the more relevant your message can be to not only the person, but also the context.

### Rationale
Tailored information is more effective in changing attitudes and beliefs than generic information. Make life simpler for users by showing only what is relevant to them. Tailor to individual needs, interests, personality, usage context, or other factors relevant to the individual.

Users are persuaded either through a central route, with full cognitive attention, or through a peripheral route, relying on heuristics

### Discussion

*Harmony strategy*

Cue social density to users, and thus subtly promote harmonious actions that support the group’s goals.

*Group Opinion strategy*

Provide users with the opinions of users similar to them at moments when users must make important decisions.