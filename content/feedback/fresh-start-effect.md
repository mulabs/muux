+++
title = "Fresh Start Effect"
template = "patterns.html"
[extra]
icontype = "fas fa-shower"
+++

### Problem summary

We are more likely to achieve goals set at the start of a new time period

### Usage

* Use when users are in need of an extra boost of motivation
* Use when users have the ability, but lack the motivation
* Use to find the right time to get users to take action (Kairos)

### Solution

* *Frame time markers as opportunities* Frame certain points in time as opportunities for a fresh start. Consider whether you can frame other time scales than just the new year coming. If applicable, use efforts of previous attempts as an anchor to beat.
* *Consider decay* Research for how long fresh-start feelings persist. The fresh start effect in relation to labor day might decay faster than for a new year.
* *Consider frequency* The simpler the behaviour required to take action is, the more likely it is that users will be motivated for behaviour change.

### Rationale

Time based landmarks such as a new week, month, year, national holiday, school semesater, or an anniversary, marks the passage of a mental accounting period assigning past imperfections to the previous period. In turn, this motivates to big-picture thinking and aspirational behaviours for the coming period of time and can hellp overcome willpower problems that keeps us from achieving our goals.