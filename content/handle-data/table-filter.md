+++
title = "Table Filter"
category = "handle-data"
[extra]
type = "tables"
icontype = "fas fa-table-cells"
+++

### Problem summary

The user needs to categorically filter the data displayed in tables by  columns.

### Usage

* Use when you have a very large data set of results that is too large to show in one page
* Use when one or more table columns can easily be summarized into categories to filter by.

### Solution

Provide dropdown inputs that present the categories by which the user can filter the data set by. Once the user selects a category and clicks “Filter” or something similar (when the user submits the form), only the row that belong to the selected category are displayed.

Optionally, multiple filters can be added. If this solution is chosen, you must be aware to update the categories of each dropdown box accordingly when one category is selected – as the selecting values in one category might reduce the options left in another.

### Rationale

Adding filters to your tables lets the user reduce the amount of items shown. Filters help narrow down search results, letting the user find more accurate results.