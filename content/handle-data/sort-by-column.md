+++
title = "Sort By Column"
category = "handle-data"
[extra]
icontype = "fas fa-chart-simple"
+++


### Problem summary

The user needs to be able to sort the data in a table according to the values of a column.

### Usage

* Use when there are many rows in table (above 10), and it is difficult to single out one row and its relation to other rows
* Use when the table uses pagination and data spans multiple pages.
* Use when you want to be able to compare rows in a table – for instance numbers.
* Do not use if the amount of rows are few and the table is easy to search or scan.

### Solution

Extend the table functionality so that each column table heading is a link. When the heading is clicked, the rows in the table are ordered ascending by the specific column’s values. If the same label heading is clicked again, the order is reversed: the rows in the table are ordered descending by the specific column’s values.

When the rows of a table have been sorted by a specific column, an arrow is often displayed beside the column’s heading indicating the direction the rows have been sorted in. The column’s heading is often presented in another font color or font weight (bold / regular) to indicate ordering has been performed.

### Rationale

The pattern provides an easy way to organize and compare data in a table. Furthermore, the pattern is also well known from desktop applications dealing with rows of data.