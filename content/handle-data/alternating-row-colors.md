+++
title = "Alternating Row Colours"
template = "patterns.html"
[extra]
type = "tables"
icontype = "fas fa-film"
+++

### Problem summary

The user needs to visually separate similar looking rows in a table apart, in order to match the values of each row.

### Usage

* Use when you have several columns in a table
* Use when the content of each column is often of similar nature
* Use when there are multiple lines for each row
* Use when the height of each row varies

### Solution

To differentiate table rows from each other, a different shade is used as background color for every second row. Keep the difference between the two colors to a minimum to preserve a gentle feeling. The colors should be similar in value and low in saturation – the one should be slightly darker or lighter than the other. It is often seen that one of the two colors is the background color of the page itself.

### Rationale

The purpose of the shading in every second row is only to provide an visual aid for the every users to follow a row from the left to the right and back again – without confusing one row with another. The purpose is not to drastically change the design of the table.

A side effect of shading every second row with an alternating color is however that the table will stand out from the rest of the page. The shading in this way boxes in the table.