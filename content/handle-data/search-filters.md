+++
title = "Search Filters"
category = "handle-data"
[extra]
type = "search"
icontype = "fas fa-magnifying-glass-location"
+++

### Problem summary

The users needs to conduct a search using contextual filters that narrow the search results.

### Usage

* Use when the search results for a query are very numerous and reviewing them would be very time consuming.
* Use when search results can be categorized into filters: the search must be contextual.
* Do not use when your search is not easily categorized into filters.

### Solution

Refine search results in real time using one or more filters.

Present everything available, and then encourage the user to progressively remove what they do not need by applying one or more filters. With immediate feedback, the experience is used from a monologue to a conversation. Only use this pattern when it helps simplify the search experience.

Present the user with a list filter categories, and let the user filter these by inserting input in text boxes, choosing options in dropdown boxes or even through checkboxes or radiobuttons. Whenever the user makes a change to any of the input fields, the results are automatically updated.

### Rationale

With a search, you start off with nothing and potentially end up with nothing. Counter to this approach is filtering, where we present everything available, and then encourage the user to progressively remove what they do not need1. – Pete Forde

Using the live filter pattern moves the search from a monologue to a conversation. The user can progressively remove what they don’t need step by step and receive feedback immediately.

When you weigh your decision to use this filter, consider whether the pattern complicates or simplifies search. If it does anything else than simplify finding the correct search result, choose another solution.