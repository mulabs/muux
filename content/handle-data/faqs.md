+++
title = "FAQ's"
category = "handle-data"
[extra]
type = "format"
icontype = "fas fa-file-circle-question"
+++

### Problem summary

The user has questions concerning a site and its related services

### Usage

* Use when users typically has the same concerns and questions about a service.
* Use to collect answers to common questions from your support department or other type of feedback mechanism.

### Solution

Provide a space where users can get answers to common questions.

Organize Frequently Asked Questions (FAQ) into a separate and routinely maintained section on your site. Keep questions short, limited, scannable, searchable, and well organized using the language of your users. Allow users to quickly assess whether an answer applies to their particular situation and provide clear actions to get started with a solution.

Focus on information
When it comes to FAQ pages, your ultimate goal is to let users find the information they are looking for as easily and quickly as possible. Your main design goal is to present its content in the most efficient and effective way possible. Information comes first – don’t let design decisions overshadow content.

Designing longer FAQs
The longer your FAQ page is, the more attention you need toward making it easy for users to find the answers they are looking for. There are several tools available:

Organize questions in categories. The categories you choose is up to you, however, your goal should be to make it easier for users to find the information they are looking for. Make them logical and without too many questions within them. Consider finding the right categorization through card-sorting.
Let users search questions. Allowing users to search questions will help users browse through possibly hundreds of questions fast. However, the words your users choose might not be the ones you chose yourself. Browse through the search log of your FAQ page to discover with what words your users think and what wording it makes sense for you to use yourself.
Prioritize the most frequently asked questions first. A 80/20 rule typically applies to FAQs: 80% of queries can be answered by 20% of your documented answers.
Card sorting
A good tool to help you find a user-centered categorization is through card sorting2. Card sorting will help design and evaluate your information architecture by letting users organize topics into categories that makes sense for them as well as labelling them.

Let users ask new questions
If a user can’t find the answer to their question within your FAQ, it’s likely they will want to ask you directly. Provide a way to let them ask a free-form text question and be sure to be able to answer them quickly.

Few people go to the FAQ front page directly
Most people search for their answer through search engines like Google. Design each answer page so that you include enough context as if it was a landing page. Align words and phrases used in the answer with what people might search for on Google. Reassure users that they’ve landed on the right page that will help them get answers to their question: place keywords that will trigger users to read more as they first scan the page.