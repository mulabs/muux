+++
title = "Copy Box"
category = "handle-data"
[extra]
type = "format"
icontype = "fas fa-rectangle-list"
+++

### Problem summary

Users need to easily view and copy preformatted text.

### Usage

* Use when you want to display formatted text, which can be easily copied without losing its formatting
* Use when need to display mono space demo code.
* Use when you wish to present information that the user needs to can copy (and later paste into another application)

### Solution

Create a box that has its own style in regards font color, font style, and background color that distinguishes itself from the rest of the page.

To preserve indentation and general formatting of the text box so that it looks like a terminal window or text editor, use a mono-space font: a font that has a fixed width typeface (glyphs have the same width).

As inserting text in raw HTML renders without line breaks and strips repeating spaces, you need to put special tags around the text you want to preserve the original formatting with. One solution is to use the `<pre>` around the text you want to preserve formatting with – another is to put the text inside a `<textarea>`.

### Rationale

If you just paste ascii formatted text into a HTML file, the user’s browser will format the text and display it all in one line and regardless of how the code looks like in the HTML file as special tags are used in HTML to define line breaks `<br>`.