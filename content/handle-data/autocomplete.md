+++
title = "Auto-Complete"
category = "handle-data"
[extra]
type = "search"
icontype = "fas fa-spell-check"
+++


### Problem summary

The user needs recognition aided search when performing search tasks that are difficult to remember or easily mistyped.

### Usage

* Use to assist with ambiguity-issues, when an item can be entered in multiple ways
* Use when the type of information entered can readily be matched with a specific piece of information in the system. E.g. A user may not recall how to spell “Amorphous” but is able to input the first 3 letters and find the word from the autocomplete list.
* Use when autocomplete suggestions can be pulled from a set of data that is manageable in size.
* Use when input speed is an important goal
* Use when input accuracy is an important goal
* Use when the number of items would be too large or inconvenient to display in a standard drop down box.
* Do not use if you want to provide the user an overview of all options available.

### Solution

Suggest possible matches for a search as users are typing.

The Autocomplete pattern is a predictive, recognition based mechanism used to assist users when searching. An autocomplete search field presents items which match the users input as they type. As the user types in more text into the search field, the list of matching items is narrowed down.

The list of matching items must allow users to select items using input devices such as keyboard arrow navigation, touch and mouse click. This allows the user to quickly select the term without having to type out the entire term. The patterns name comes from the notion that the system completes your search. Limit the number of matching items to display when working with large dataset. A standard limit is 10 matching items.

Find a maximum number of matching items to display when the matching data set is in the hundreds, thousands, or millions. A standard limit seems to be to present a maximum 10 matching items.

Order matching items by relevance with the most relevant or likely match at the top of the list. This will allow the user to quickly select his or her match.

Some autocomplete implementations group matching items into categories. On apple.com for instance, matches are organized by groups.

### *Implementation details*

The autocomplete pattern is used in combination with a standard input text box that is labelled to match the user’s expectation of what field will be searched against1.

As the user types in data, a list of suggested items that match the inputted data is displayed. As more text is inputted, the displayed list is updated to matching the updated query – narrowing down matching items.

The list of suggested items is most often displayed directly beneath the input text box and has a width that matches the width of the text box.

It may be appropriate to highlight what part of a suggested item that matches what has been inputted. Example: “Amorphous”.

Allow the user to cancel the suggested items list by pressing the ESC key. Pressing the ESC key causes the suggested items list to close, however typing in more characters after pressing the ESC key will restart the autocompletion behaviour.

### Rationale

The Autocomplete pattern allows faster input, reduces the number of keystrokes needed, prevents typing errors, and provides feedback on the validity of what is being entered. It also allows designers to include longer lists for users to choose from without taking up extra screen real estate.

Autocompletion and search suggestions save the user keystrokes by matching a user’s query with potential matches that are displayed as the query is being typed.

It reduces the number of keystrokes and thus allows for faster data input. Tiresome, long, and complicated queries such as email addresses or airport names, can be found and selected with only a few keystrokes.

Additional formatting of a search suggestion can help remove ambiguity. If I am searching for an airport in London, extra formatting can tell me whether I am selecting Heathrow or Standsted airports.

Autocompletion provides a feedback loop that continually lets the user narrow in on the correct choice.

The cognitive burden of remembering an exact text-phrase is made easier, as the user can use the autocomplete pattern to only type in details that he or she remembers. If a user is searching for an email that he can only remember the domain name of, he or she can simply enter the domain name where-after all emails with that domain name is presented for selection.

The autocomplete pattern relies on the principle of recognition over recall. Instead of having to recall a full and exact text query, the user can start typing in parts of the query he or she recalls, and in turn rely on recognition to select the best match.

### Discussion

When matching on large sets of data, it is critical for the matches presented to be close to what the user is looking for.

Google solves this problem by ranking matches by the amount of times each query has been searched for. Apple solves the problem in Spotlight (a desktop application for searching items on the computer) by ranking matching items by the amount of times an item has been selected when that exact query was entered.