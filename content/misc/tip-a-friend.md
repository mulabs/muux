+++
title = "Tip a Friend"
category = "misc"
template = "patterns.html"
[extra]
type = "frequency"
icontype = "fas fa-bullhorn"
+++

### Problem summary

The user wants to share something of interest with a peer.

### Usage

* Use when the user might want to spread the word about something on your site, but find it difficult to shorten
* Use when the user might think something on your site is interesting for other people.
* Use when you want users to share content on your website.

## Solution

Add a link with a similar text to “Tip a friend”, “Send this to a friend”, “Share this with a friend”, that leads to a form to be filled out with the user’s data as well as a private message. The result of the form could be a mail sent out to the user’s friend with a condensed formed version of the content or simply a link to the original content.

## Rationale

The Tip A Friend pattern is a function that facilitates the user’s need to easily spread the word about content. It can be useful if the information of interest is formatted in a way, that makes it hard to copy-paste into an understandable mail. The website can then help format the mail by setting up the info in a nice and readable format.

The usefulness of this pattern when just letting the user send a blank mail with a link to the content in question, can be debated. The users need for this kind of functionality is often not justified.