+++
title = "Coupon"
category = "misc"
[extra]
type = "shopping"
icontype = "fas fa-bag-shopping"
+++

### Problem summary

You want to attract users to purchase products using an incentive.

### Usage

* Use when you want to create special attention around a specific product or service
* Use when you want to give your customers an incentive to commit to a purchase of one of your products or services
* Use when you want to advertise a discount to a specific group of people or advertise to a target group of users by for instance posting a coupon code in a forum or printing a coupon code in a magazine.

### Solution

Create a field specifically for entering a coupon / promotional code. Entering a code gives the customer a certain amount of discount depending on what code has been entered. On the merchants side, a number of different codes can be constructed in order to 1) measure where your customers come from and have heard of you and 2) allow different discount rates for different groups.

### *Types of coupon offers*

Coupons give access to a variety of discounts and offers. The most common are:

* *Percentage Based Discount*: The most common coupon type is the one giving access to percentage based discounts.
* *Dollar Value Discount*: Coupon offers based on the dollar value of a product makes people feel like they’re wasting money if they don’t use it. In some studies, redemption of dollar based offers vs. percentage based offers can be as much as 175% greater1.
* *Free Shipping*: Coupons that give access to free shipping is often provided in confunction with a minimum order size to help increase the average order value.
* *Free Gift*: Coupons providing a free gift to your order require customers to purchase at least one other item and often of a minimum value

### Rationale

Using coupons codes to attract customers to buy a merchants product builds on the customer’s assumption that the offer is short lived, why action must be taken soon in order not to loose the psychological gain created by receiving the coupon code. As losses are mentally valued with greater weight than gains, the customer will be aversive towards losing the gain given and act on it while time is. Check out the Value Function as proposed by Tversky and Kahneman, 1991: Loss Aversion in Riskless Choice.

Another benefit of Coupon codes its traceability. Marketeers can branch out different codes to find out which campaign generated the most leads.