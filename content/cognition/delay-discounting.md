+++
title = "Delay discounting"
template = "patterns.html"
[extra]
type = "other biases"
icontype = "fas fa-carrot"
+++

### Problem summary

We tend to choose smaller immediate rewards over larger, later rewards

### Usage

* Use to drive users toward action now rather than later
* Use to break up a larger conversion into small immediate rewards

### Solution

* *Immediacy is key* Focus on the value you can deliver to your prospects right now – or relatively soon, at least. Offer smaller and more immediate rewards over greater ones that people have to wait for.
* *Immediate rewards drive behaviour* Why does free shipping for orders above a specific amount drive us to buy more? We value the immediate satisfaction of not paying for shipping today over the delayed satisfaction of greater savings tomorrow. Similarly, allowing customers to delay payment will push them to convert as the reward of not paying today outweighs the reward of not having to pay at some point down the line.
* *Our willingness to choose decays* There is an exponential factor of decay in our willingness to choose something now. Our desire for relative immediacy diminishes substantially over time.

### Rationale

We have a preference for rewards that arrive sooner rather than later and discount the value of the later reward by increase in delay. Consider offering the convenience of now over offering a marginally cheaper alternative later – customers will have a tendency to choose a premium service that prioritises faster delivery.