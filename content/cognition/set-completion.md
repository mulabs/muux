+++
title = "Set Completion"
template = "patterns.html"
[extra]
type = "other biases"
icontype = "fas fa-puzzle-piece"
+++

### Problem summary

We desire collecting all pieces of a set more the closer it is to being complete

### Usage

* Use to motivate people toward completing a set of actions.
* Use to pave the way for user interactions
* Use to push users to explore everything that your product has to offer – in all of its corners.

### Solution

People really don’t like to keep things incomplete.

* *Grouping logic matters* A badly designed set can prove annoying or demotivating.
* *Keep sets to a manageable size* If the number of tasks required to completeness is prohibitively high, people may decide not to engage at all to avoid anticipated dissatisfaction with partial completion. Find out, on average, how many people usually complete, and then make your set slightly larger than that.
* *Add multiple levels of sets* What happens after completion of one set? Consider celebrating the completion and suggesting one or more follow-up paths to embark on.

### Rationale

People are irrationally but effectively motivated by the idea of completing a set, even if it means working harder or spending more money—with no additional reward other than the satisfaction of completion and the relief of avoiding an incomplete set.

Even without other reward than the completion in itself, logically grouping items or tasks together as part of a “set” motivates people to reach perceived completion points. Logically grouping tasks into sets and keeping sets manageable in size allows for achievable goals and small successes.

Consider how you can have multiple levels of sets and what kind of things or information suggests logically grouped sets and set completion. This principle of completion also applies to other things left incomplete such as puzzles, pictures, or sentences.