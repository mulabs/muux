+++
title = "Scarcity"
template = "patterns.html"
[extra]
type = "scarcity"
icontype = "fas fa-gem"
+++

### Problem summary

If something is promoted as being scarce, we perceive it as more desirable and more valuable

### Usage

* Use when you want to convey a feeling of exclusivity
* Use when you want users to rush their decision
* Use when you want users to infer value in something
* Use when you want to encourage purchasing or other behaviour.
* Use when you want to precent users to take time to think about their decision, and instead push them into making a decision, immediately.

### Solution

Scarcity can be effective in several ways.

* *Time-based scarcity* Time based scarcity is a well used tactic in the retail world. Think of holiday sales, coupons that run out at the end of the month, and ‘for a limited time only’-offers. Time can be used to convey scarcity just as stock scarcity can.Especially web shops try to invoke stock scarcity effects when they tell us that only limited amounts of an item are available. Time-based scarcity invokes a feeling of urgency. We better hurry up and make our purchase before the item is all sold out. This in turn leads to a decision that we might not have made if we had better time to evaluate alternatives.

* *Stock scarcity* Google used stock scarcity well when they launched their web mail application, Gmail, in private beta. Due to technical constraints, they could not open op for 2 gigabyte storage for everyone (10 megabyte was the norm back then), why they had roll out the service slowly through invitations. This turned out to do Gmail well. The scarcity effect was in full bloom. If you were lucky enough to become a member, you could invite 2 or 3 friends yourself. The service spread fast virally with help of its scarcity and recommendation system.

* *Restrictions on information (or merely scarce)* We have a tendency to want what has been banned and therefore to presume that is is more worthwhile. We typically react to attempts to censor or otherwise constrain our access to information. We desire and favor banned information more after it has been banned than before the ban.This type of scarcity even works beyond bans. We will basically find a piece of information more persuasive if we think we can’t get it elsewhere.

### Rationale

Scarcity is often used to encourage purchasing behaviours. It prevents customers from taking the time to think and instead pushes them into making a decision immediately. Limit resources, durations, or intervals to make users value an asset more (whether it is a potential purchase or an action which they can carry out).

Scarcity is the condition where there is an inadequate amount of something to please everybody. The feeling of a shortage can be invoked simply by indicating that there is not enough for everybody. If something is scarce, it will seem more desirable and more valuable to us.

According to Cialdini, the influence scarcity has on us comes from two things:

1.  our weakness for shortcuts and the fact that
1.  we hate to loose the freedoms we already have.

*Our weakness for shortcuts*

When things are difficult to possess we typically connect it to being better than those that re easy to possess. We use an items availability to quickly determine its quality and value. By following the scarcity principle we usually and efficiently make correct and quick decisions.

*We hate to loose the freedoms we already have*
As opportunities become less available, the need to retain those opportunities make us desire them significantly more. As we loose freedom and choice, we put more value in the them. Because we hate to loose the freedoms we already have, we react by making quick decisions. We react against interferences of removing access to items by wanting and trying to possess the item more than before.

### Discussion

Optimal conditions for scarcity to work
Newly experienced scarcity is the most powerful. People see a thing as more desirable when it has recently become less available than when it has been scarce all along.

Furthermore, scarcity created by social demand is significantly stronger than scarcity by mistake. This highlights the importance of competition in the pursuit of limited resources. We not only want the same item more when it is scarce – we want it most when we are in competition for it. Retailers have tried taking advantage of this tendency by teaching us about products that are in popular demand and that we should hurry to buy. This is not only playing on the power of social proof – that we think the product is good because other people think so – but also that we are in direct competition with other people for the item.

A product doesn’t have to be good or of high quality for scarcity to work. We desire a scarce product not because we want to experience it, but because we want to possess it.

Make a decision today or loose out forever
Simply put, people want more of those things they can have less of. The scarcity principle focuses on forcing people into making a decision, within a small window of time, by emphasizing the future unavailability of something. If you give people all the time in the world to make a decision, they will either take up every minute of that time, or never make a decision at all.

By implementing scarcity techniques, you are essentially forcing users to take action. People will act more quickly under pressure from the Scarcity principle, as they are fearful they will lose the opportunity forever. Scarcity forces us to prioritize a decision, so that it becomes more important, more desirable, and more urgent than other decisions, that are unlimited in nature.

It’s not enough simply to tell people about the benefits they will gain, if they choose your product or services, you will also need to point out what’s unique about your proposition, and what they stand to loose, if they fail to consider your proposal.

The successful application of the Scarcity principle in design and marketing campaigns,
is preventing users to take time to think about their decision, and instead try to push people into making a decision, immediately.

By playing on scarcity to push decisions, you are utilizing the power of Loss Aversion.

*Origins of the term Scarcity*

The term scarcity was first used in French in the 1300s. Since, it became a basic principle in micro economics, examined by Adam Smith in the 1700s. In regards to psychology, Robert Cialdini popularized its persuasive effects in his seminal book on influence and persuasion.