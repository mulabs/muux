+++
title = "Priming Effect"
template = "patterns.html"
[extra]
type = "biases"
icontype = "fas fa-memory"
+++

### Problem summary

Decisions are unconsciously shaped by what we have recently experienced

### Usage

* Use to guide users to the right choice.
* Use to instil relevant emotions in your target audience that will bring forth emotions relevant for a future decision

### Solution

* *Use metaphors* Conceptual metaphors refer to information that can unconsciously help bring specific decision outcomes to mind.
Trigger relevant emotions. Use imagery or video to create associative priming with the subsequent expected experience.
* *Use visual imagery* Colors, pictures and videos all have the power to unconsciously bring up cues that might be replicated at a later point in the user experience.

### Rationale

Exposure to a word, sign, picture, or meaning anchors the idea and allows us to more quickly recognise related options. After being primed in one direction, our instinctive preference thereafter will be in a related direction. Being semantically primed eases mental processing of that information at a later stage, creating a sense of cognitive fluency and ease of use.

### Discussion
Types of priming:

* *Conceptual priming* When related ideas are used to prime the response (e.g. ‘hat’ may prime for ‘head’).
* *Semantic priming* When the meaning created influences later thoughts. Semantic and conceptual priming are similar and the terms are sometimes used interchangeably.
* *Non-associative semantic priming* Related concepts but one is less likely to trigger thoughts of the other (e.g. ‘Sun’ and ‘Venus’).
* *Perceptual priming* Based on the form of the stimulus, for example where a part-picture is completed based on a picture seen earlier (like the camel example above).
* *Associative priming* When a linked idea is primed (e.g. ‘bread’ primes the thought of ‘butter’).
* *Masked priming* When word or image is presented for a very short time but is not consciously noticed.
* *Repetitive priming* Repetition of a word or phrase leads to influencing later thoughts.
* *Reverse priming* When people realize they’re being primed and overcorrect in the other direction.