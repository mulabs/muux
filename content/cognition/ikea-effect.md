+++
title = "IKEA Effect"
template = "patterns.html"
[extra]
type = "loss aversion"
icontype = "fas fa-building-user"
+++

### Problem summary

We place a disproportionately high value on products we helped create

### Usage

* Use to boost customer satisfaction by involving end users in part of the building or assembly process
* Use to make users more resistant to change from a particular project due to the sunk cost effect of having poured effort into a product, service, or project.

### Solution

* *Increase customer satisfaction* Actively involve users in not only designing, marketing, and testing of products, but also building them, to maximize their satisfaction. Add an aspect of customer-owned creation into your existing product. The biggest challenge lies in convincing users to engage in the kinds of labour that will lead them to value products more highly.
* *Find appropriate challenges* Labour leads to love only when that labor is successful. Be careful to create tasks that are not too difficult as to lead to an inability to complete the task. Start with low effort personalization options such as adding customization options and progress from there.
* *Can lead to change aversion* We tend to over-commit to efforts in which we have previously invested just as we over-value products we have been part of building. This leads to our tendency to be less likely to abandon ideas or projects that we have previously put labour into.

### Rationale

Allow customers to be involved in the creation of your product to establish a powerful emotional connection between the two. We will pay more for something we have put work into than if we bought it ready-made. Customers are willing to pay a premium for products they have customized. The effect is not simply due to the amount of time spent on the creations, as dismantling a previously built product will make the effect disappear.