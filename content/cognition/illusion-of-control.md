+++
title = "Illusion of control"
template = "patterns.html"
[extra]
type = "other biases"
icontype = "fas fa-screwdriver"
+++

### Problem summary

We have a tendency to believe that outcomes can be controlled, or at least influenced, when they clearly cannot.

### Usage

* Use when you want to motivate the user to strive for a goal

### Solution

Provide your users with a sense of control in order to let them feel confident and assured that they their actions have a positive influence on their goals.

### Rationale

As humans, we have a desire to believe that we are in control and thus have a tendency to believe that outcomes can be controlled, or at least influenced, when they clearly cannot.

We believe in the illusion that we can control factors outside our reach; that actions we take to influence them are directly linked to how things turn out.

If we tell ourselves that we are not being loved as we are not good enough, we believe we can get the love we want by striving for being good enough. We tell ourselves that things will turn out for the better if we live a good life and worship our god(s). We tell ourselves that somebody is not attracted to us because we are overweight, have weird teeth, or have too little breasts, why manipulating these factors will change the opinion of others. We tell ourselves that we are in control over facebook’s privacy settings although they are too confusing to let us actually be in control. We tell ourselves that the reason we did not get a certain job was because we are not smart enough why we study harder.

The illusion of control can act as a motivational factor for conducting certain behaviour that is either unsafe or that we believe will bring us closer to a goal.

Optimistic self-appraisals of our capabilities that are not completely apart from what is possible can be advantageous. On the other side can judgments far fetched from the truth turn out to be self-limiting.

The illusion of control gives us a sense of power and in turn happiness. When we believe we are in control, we do not have to rationally analyze facts in order to make a decision. The illusion of control drives us forward and can help escalate commitment.

Illusory beliefs about control help us strive for reaching goals, but are not contributing to sound rational decision-making. Instead they can cause insensitivity to feedback, hinder learning, and make us more risk seeking as subjective risk is reduced by the illusion of control.