+++
title = "Endowment Effect"
template = "patterns.html"
[extra]
type = "loss aversion"
icontype = "fas fa-shield-dog"
+++

### Problem summary

We place greater value on objects we own over objects we do not, especially if sentimental value has been placed in them

### Usage

* Use when you want to retain regular users
* Use when you want to let users place more value in your product than the one of a competitor

### Solution

The endowment effect can be described as the divergence between willingness to buy and willingness to sell. We place higher value on objects we own over objects we do not, especially if sentimental value has been placed in them. Ownership creates satisfaction.

### Rationale

When we get something, we adjust to our level of ownership, which then becomes the baseline by which we judge future gains and losses. Possession feels like ownership: consider giving away free trials or make users invest time in your service.

In a university study, students were parted into two equally sized groups. One group was given mugs (“seller”) and the other (“chooser”) nothing. When asked, the sellers in average wanted $7.00 for the mug, whereas the choosers were only willing to pay $3.50. The mug was evaluated as a gain by the choosers and as a loss by the sellers.

As users sign up, start build their account, and making it theirs, the endowment effect comes into play. Amazon does it well by letting its users make wish lists, make gift organizers, rate and recommend products, make lists, award users with â€œ#1 reviewerâ€ badges, and more. As users interact with Amazon and gain ownership over the Amazon website, the endowment effect makes them place higher value on conducting transactions on amazon.com vs barnesandnoble.com.

Another successful website to use the endowment effect to retain users is the online radio station: last.fm. When users listen to music at last.fm, they can choose to “love” what they hear and thereby indicate to last.fm that they are interested in hearing something similar again. Last.fm remembers what music you love and constructs a metric of your music taste that it uses to deliver music that caters to your preference. The website has managed to let the user take ownership in the site with the music profile build up with consistent use. The has resulted in many preferring to listen to music over the Internet via last.fm than over their iPod or through the CD collection.

### Discussion

The difference between our Willingness to Pay and our Willingness to Accept was first noted in the 1960s, although the term Endowment Effect was coined by Richard Thaler in 1980 in reference to how we as humans tend to under-weigh the opportunity costs and inertia as things we own (endow) become more highly valued than goods we do not.