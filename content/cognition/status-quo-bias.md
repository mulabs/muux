+++
title = "Status Quo Bias"
template = "patterns.html"
[extra]
type = "loss aversion"
icontype = "fas fa-scale-unbalanced"
+++

### Problem summary

We tend to accept the default option instead of comparing the actual benefit to the actual cost

### Usage

* Use when you want the user to choose the default option you want them to
* Use when you are making changes to your offering and want to keep users as your customers.

### Solution

Frame the option you would like your user to choose as the default option and make the cognitive load to understand alternative options too big to comprehend at a glance.

When the user has a number of options to choose from, you can help him or her on the way by framing one of the options as the default option.

When the user has a number of options to choose from, you can help him or her on the way by framing one of the options as the default option. Let’s say you run a subscription based web application with several plans, each with its own price and list of benefits. A popular plan is going to be dropped in favor of one with a higher price and better list of benefits, which leaves the users to either downgrade or upgrade; loosing old features or gaining new. Moving everybody to the new plan and framing it as being the same with new features positions the status quo reference point to the bigger plan, with the result that most people will “stay”.

Facebook utilized the status-quo bias in December 2009, when they changed their user privacy policy. They changed a user’s privacy settings into having a default setting which the user had to opt-out from. The complicated and non-transparent nature of the new policy kept users from changing them.

### *Apply the status-quo bias*

* *Limit choice* More choice is not always better. Having many choices might grab our attention, but too many can overwhelm us to the point where we are likely to not choose (or buy) at all. Complexity delays choice, further increasing the fraction of consumers, who will adopt the default options.
* *Pre-select your wanted response* When the user has a number of options to choose from, you can help him or her on the way by framing one of the options as the default option. Understanding and compariing multiple options takes its toll on our cognitive load.
* *Beat the bias* If you want people to change, show them the cost of staying the same, as well as potential gains. Paint a clear and visual contrast between their current state and desired future state. Consider telling a before-and-after hero story to identify with highlighting challenges relevant to the audience.

### Rationale

When we are uncertain of what to do or feel overwhelmed by the number of options to choose from, we tend to stick with our previous choices or choices that has been made for us – even if the alternatives might be better. As making decisions grows increasingly more complex, the harder it becomes to use our emotional heuristics to shortcut decision making to approximate rational thinking.

Simply stating what options are more popular is often enough to influence a decision that sticks. Simply pre-filling a form with default options is often enough to make people choose them. Alternative options that seem too cumbersome to comprehend will make people stick with the status quo.

As making decisions grows increasingly more complex, we have a harder time using our emotional heuristics to shortcut decision making to approximate rational thinking. Instead we tend to accept the default option instead of comparing the actual benefit [gain] to the actual cost [loss].

### Discussion

Once we have acquired a gain, we change the way we evaluate what has been acquired as our reference point is now new. This is the root of the status quo bias. If the status quo serves as a reference point, the features you loose might be valued more when they are given up than when they were being acquired.

Adding to the status quo bias is that surprising fact that people tend to still accept the easiest way out even when the decision is important and the stakes are large – and even when they are told the default decision is not optimal1.

More choice is not always better. Complexity delays choice, further increasing the fraction of consumers, who will adopt the default options.

Lots of choices will grab our attention, but too many will overwhelm us to the point where we likely won’t choose (or buy) at all.