+++
title = "Need for closure"
template = "patterns.html"
[extra]
type = "other biases"
icontype = "fas fa-route"
+++

### Problem summary

We have a desire for definite cognitive closure as opposed to enduring ambiguity.

### Usage

* Motivate your users by providing subtle cues to completion.
* Use when you want to motivate users to act.
* Do not use to motivate your users if what you want to motivate them to is cumbersome, enduring or cannot be achieved though small successes.

### Solution

Motivate your users to act by providing subtle cues to completion. As humans, we have an innate motivation to seek away from ambiguity towards closure. By providing small tasks that plays on our need for closure, you can provide small successes for your users and let them feel as they are working their way out of ambiguity and doubt towards completeness and certainty

* *Encourage completion* Encouraging people to strive for completeness by achieving goals or collecting items until they have a full set. The compulsion to collect, to be complete, drives people to action.
* *Highlight lacking closure* Highlight unfinished business and provide clear and effortless ways of alleviating the pain to drive action.
* *Provide a path toward completion* Make it effortless for users to understand what options needs to be taken to reach a state of completeness.

### Rationale

As humans, we strive toward satisfying our goals and all feel we have a need for fulfilling them. This need for not leaving things be, is a key motivator for acting on our dreams. It is important for us to perceive a movement towards closure.

The tension arising out of the need for closure is called frustration, the closure is called satisfaction2. Once satisfaction has been reached, the imbalance is removed and thus disappears.

The feeling of ambiguity or uncertainty motivate us to find answers. The perceived cost of lacking closure, such as missing deadlines, drives us forward toward action. This makes us freeze on early judgmental cues that in turn introduce biases in our thinking. We like closures to be permanent and will do much to keep them as such. Too much choice will lead to indecision and lower sales.

### Discussion

In psychology, people are described as having either a high- or low need for closure. People with a high need for closure prefers order and predictability and is thus decisive and close minded. People with a low need for closure will emit more creative acts1.

Either way, we all have a need to have an answer and to escape the feeling of doubt and uncertainty.

When designing for people with a high need for closure, utilize the fact that these people are searching for an end and has an innate motivation to “finish up”. People with a high need for closure are likely to quickly grasp closure by relying on early cues, cognitive heuristics, or the first answer they come across. This creates a fine balance: people with a high need for closure can quickly loose all motivation if a solution is not provided or can be found in due time.

People with a low need for closure can better deal with living in ambiguity for longer periods of time. They are more productive and their solutions are rated as more creative.