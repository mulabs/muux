+++
title = "Optimism Bias"
template = "patterns.html"
[extra]
type = "loss aversion"
icontype = "fas fa-thumbs-up"
+++

### Problem summary

We consistently overstate expected success and downplay expected failure

### Usage

* Use to avoid being too optimistic about the future
* Use to help users downplay their optimistic thoughts about their own future accomplishments to instead focus on and set realistic goals.

### Solution

* *Limit effect using other biases* Use other biases to guide decision making. For instance, use Loss Aversion to highlight potential negative effects of critical actions clear to the user and offer a straightforward and safer alternative.
* *Use anchoring to force strategic thinking* To mitigate the Optimism Bias, it’s worthwhile to anchor in a pessimistic future first in order to keep the optimistic scenario more realistic. Simulate pre-mortems or “the future, backwards” exercises.
* *Think strategically in both directions* Provide a more full perspective of future scenarios – positive ones and the negative ones as well.

### Rationale

As we compare ourselves to our peers, we tend to overestimate our future success and downplay our future failure. We tend to believe in the best case scenario and ignore conflicting data as we look at what has worked in the past to predict the future, but often neglect to examine if something has been overlooked. Disclaimer: the Optimism Bias doesn’t seem to kick in for people suffering with depression.