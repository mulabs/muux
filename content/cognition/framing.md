+++
title = "Framing"
template = "patterns.html"
[extra]
type = "loss aversion"
icontype = "fas fa-object-ungroup"
+++

### Problem summary

The way a fact is presented greatly alters our judgement and decisions

### Usage

Use to influence decisions involving factual evidence.

### Solution

* *Create a frame* Frame evidence as either a loss (10% fat) or a gain (90% lean) to help others see things the same way as you.
* *Reframe statistics through comparison* Evidence are reinterpreted when compared and contrasted with other data. Reframe facts, that in one light might seem negative to something positive, by comparing and contrasting to industry standards or similar. Your product might have a 3 out of 5 star rating, but the competition has a 2 star rating.
* *Losses loom larger than gains* We are more likely to act on an offer when the marketing message is framed as a loss rather than a gain. “Don’t lose your offer” works better than “Here’s an offer”.

### Rationale

The same piece of information can be reframed substantially to influence decision-making through words, numbers, and imagery. The same fact can generally be framed as a loss or gain and as we try to avoid risks when a choice is framed positively and seek risk when framed negatively, decisions will also vary. An implied story makes more desirable choices seem obvious.

We determine value by comparing and contrasting things. The value of particular items can seem very different in various situations, depending on the comparison. An implied story makes more desirable choices seem obvious.

We tend to avoid risk when a positive frame is presented but seek risks when a negative frame is presented