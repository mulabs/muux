+++
title = "Temptation bundling"
template = "patterns.html"
[extra]
type = "other biases"
icontype = "fas fa-cookie-bite"
+++

### Problem summary

Engaging in hard tasks is more likely when coupled with something tempting

### Usage

* Use to get users engage in behaviour or chores that they “should” do, but that they don’t “want” to do

### Solution

* *Bundle with instantly gratifying wants* Bundle a “want” activity (e.g. watching the next episode of a habit-forming television show, checking Facebook, receiving a pedicure, eating an indulgent meal) with engagement in a “should” behaviour that provides long-term benefits but requires the exertion of willpower (e.g., exercising at the gym, completing a paper review, spending time with a difficult relative) to promote target behaviour.
* *Geo-fence features to boost attendance* Restricting access to something desirable (for instance a desirable audiobook) only within the compound of a physical event or gym boosts attendance. Unlock the wanted activity by being on the right location.
* *Make chores more bearable* Assess what unique, sustainable and meaningful benefits you can use to make complying with work-place practices more attractive. For example, letting the person manning the shared support phone receive a free coffee card for the week.

### Rationale

Bundle something users enjoy with something they dread to push to take action. When struggling to find enough internal motivation to tackle something you hate, an extrinsic reward can be the needed push to stop avoiding the task. Let users do the right thing and reward them for it.