+++
title = "Limited choice"
template = "patterns.html"
[extra]
type = "scarcity"
icontype = "fas fa-bone"
+++

### Problem summary

We are more likely to make a decision when there are fewer options to choose from

### Usage

* Use when you want to ensure that users make a choice, now
* Use to push users through a tunnel or funnel
* Use when you want to reduce the complexity of your interface

### Solution

How many choices do you offer? Can this be reduced? Every time you make it easier for users to think, they are more likely to make a decision.

Simplify decision paths and present the more complex choices first.

* *Reduce choices available* How many choices do you offer? Can this be reduced? Every time you make it easier for users to think, they are more likely to make a decision.
* *Find the right amount of choices* At first, more choices lead to more satisfaction, but as number of choices increases, satisfaction peaks whereafter people tend to feel more pressure, confusion, and dissatisfaction with their choice.
* *Avoid analysis paralysis* When confronted with too many choices especially under a time constraint, many people prefer to make no choice at all, even if making a choice would lead to a better outcome.

### Rationale

Making a decision becomes overwhelming when we aare faced with many options due to the many potential outcomes and risks that may result from making the wrong choice. Having too many approximately equally good options is mentally draining. Although larger choice sets can be initially appealing, smaller choice sets lead to increased satisfaction and reduced regret.