+++
title = "Limited duration"
template = "patterns.html"
[extra]
type = "scarcity"
icontype = "fas fa-stopwatch-20"
+++

### Problem summary

Use time limitations to push users to take action

### Usage

* Use limited duration offers to push users to make a decision now rather than later
* Use to force decision making to happen now rather than later
* Use to force comparison of possible options, based on the facts known during the limited time rather than after.

### Solution

Introduce time-constraints to enforce the feeling of a product, service, or item being scarce. Time-based scarcity invokes a feeling of urgency – that we better hurry up and make our decision to buy or use before the opportunity is over.

The thought of a missed opportunity that we will not be able to obtain at a later time makes us act now. We would rather act now and not miss out on an opportunity – even though we are not totally sure if the decision will be of actual value. Time-based scarcity invoked effectively can make people make a decision that they might not have made if they had better time to evaluate alternatives.

### Rationale

We hate to loose the freedoms we already have. As opportunities become less available we tend to desire them significantly more. We put more value into the freedom of choice, which is why we react to time-based scarcity by making quick and sometimes uninformed decisions. We react against lost freedom of choice by wanting and trying to possess scarce items more than before.