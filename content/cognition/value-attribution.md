+++
title = "Value attribution"
template = "patterns.html"
[extra]
type = "other biases"
icontype = "fas fa-truck-monster"
+++

### Problem summary

The perceived value of things increases with their cost and appearance

### Usage

* Use to increase the perceived value of your product
* Use to stand out from your competition

### Solution

We tend to attribute the value, goodness, or authenticity of something to its context instead of the thing itself

* *Stand out* Consider positioning core traits of your product to significantly stand out from the competition to increase the perceived value of your product. It could be with a much higher price (or price one plan of many higher to let it act as a decoy), remarkablel brand, or acting with attitudes contrary to the established market.
* *Invested time as cost* Consider whether you can get users to invest their time in your product before paying to use it. This might increase its perceived monetary value.
* *Attribution decays* The perceived value we infer from value attribution decays over time as we both have time to compare and contrast with more evidence. A conversion to sale is most likely to happen as the value attribution is assigned.

### Rationale

We tend to assign unobservable traits to people and things based on its context and a few predominant cues rather than objective data. A high price may induce perceived qualities not seen if priced lower. An attractive appearance may infer a perception that a product works better. As we evaluate the value and traits of a product, cues like price, brand, appearance, and behaviour have significant influence.

### Discussion

We also use attribution theory to explain why a particular event or outcome has occurred, why an experience met a users experiential needs, or why the experience was to the users’s liking

If we itemised the stages of such an experience and the process of value attribution, it would look something like this:

* What happened? – outside events do not exist unless we are aware of them
* Moods, emotions, feelings – how we interpret the event
* Perceived duration – involvement
* Cognitive components – intentions move our minds and focus
* Sense of competence/control – we use to perfect our skills
* Sense of freedom – objective and subjective perception of control