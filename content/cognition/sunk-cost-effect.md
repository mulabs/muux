+++
title = "Sunken Cost Effect"
template = "patterns.html"
[extra]
type = "loss aversion"
icontype = "fas fa-faucet-drip"
+++

### Problem summary

We are hesitant to pull out of something we have put effort into

### Usage

* Use to boost usage
* Use to upsell
* Use to post-sell
* Make people aware of bias to guide people into making rational decisions, compaaring the actual costs to the actual benefits.

### Solution

The Sunk Cost Effect can be utilised in product design in several ways.

* *Boost upselling* Once users are invested in a subscription-based service they’re more likely to upgrade or make an additional purchase.
* *Boost usage* Paying for the right to use a good or service will increase the rate at which the good will be utilised.
* *Remind to boost usage* Tired of last minute cancellations? Notify customers about an the event they have booked and its value in its run-up to boost their desire to attend or gift admission to someone else.
* *Remind to post-sell* Offer check-up services on durable goods to remind them of what they have already paid to increase post-sell purchases of complimentary goods.

### Rationale

Why are we likely to continue with an investment even if it would be rational to give it up?

One explanation is our loss aversion. The fact that the impact of losses feels much worse to us than the impact of gains – we are much more likely to avoid losses than seek out wins. In this perspective, we might feel that our past investment will be ‘lost’ if we don’t follow through on the decision to continue investing. In this way we end up making a decision based on fear of loosing what we have already spent rather than consider the actual benefits that would be gained if we did not continue our commitment and proceeded with an alternative option.