+++
title = "Loss Aversion"
template = "patterns.html"
[extra]
type = "loss aversion"
icontype = "fas fa-person-circle-exclamation"
+++

### Problem summary

Our fear of losing motivates us more than the prospect of gaining something of equal value

### Usage

* Use when a decision can be framed as either a gain or loss
* Use when you want to motivate the user to continue
* Use when you want to motivate the user to start

### Solution

In the critical moment of making a decision, the user’s choices can be framed as either a gain or a loss.We are more willing to select a risky choice when we believe we are about to loose something than if we believe we are “just” gaining. This phenomenon is explained by the value function of prospect theory.

The value function states that people tend to overreact to small events, but under-react to medium and large events. It also states that if a user looks at respectively a gain and a loss of the same absolute size, the loss is perceived as having greater value than the gain.

#### Thaler’s 4 principles
The concepts of loss aversion, status-quo bias, the endowment effect, and fairness and reciprocity is explained by what Kahneman and Tversky call the value function. The center of the value function is the user’s point of reference from where any change will be seen as either a loss or a gain. The concave gain side and convex loss side illustrates loss aversion and diminishing sensitivity1.

The difference between $10 and $20 seem bigger than the difference between $1000 and $1010 and loosing $100 hurts more than gaining $100 yields pleasure.

To leverage these facts, Thaler lists 4 principles for framing.

1. *Segregate gains, as the gain function is concave*

	The perceived value of a series of gains as separate entities is greater than the perceived value of them combined. Split goodies up into multiple pieces and use them as bait to keep the user happy. A series of smaller successes is better than a few big ones. As gains are integrated, their combined perceived value diminishes.

1. *Integrate losses, as the loss function is convex*

	You are better off telling it all as it is. As losses are integrated, their combined negative value diminishes – so get it all out while you are at it!

1. *Integrate smaller losses with larger gains (to offset loss aversion)*

	It is possible to “cover up” losses by combining it with a gain that has a larger perceived positive value than the loss is negative.

1. *Segregate small gains from larger losses*

	Segregate small gains from larger losses, as the gain function is steepest at the origin segregating it can bring more value than using a small gain to reduce a large loss.

#### How to apply loss aversion to your product
Moving from theory to practice – here are a suggestions as to how you can apply the power of loss aversion to your product.

* *Offer trial offers* We are more likely to engage in free trial periods than paying up front. While some might not be willing to pay the market price up front, they may pay the market price to avoid it being taken away.
* *Delay payments* Shift consumer’s judgement from paying for a product to avoid loosing it by delaying paying subscription fees.
* *Handle problems immediately* The emotional value of loss is both larger than that of gains, but also builds over time. To limit the pain experienced, replace faulty products immediately with an identical or superior model.
* *Split up good news* A series of smaller successes is has higher emotional value combined than a few big ones. The perceived value of a series of gains as separate entities is greater than the perceived value of them combined. Split goodies up into multiple pieces and dole them out over time to keep the user happy.

### Rationale

The displeasure of losses makes us go to greater lengths to avoid them rather than take risks to obtain gains. Frame gains and losses to make some options seem more desirable than others. What is lost by leaving your service? Offer perceived value that can potentially be lost on closing an account.

When the outcomes of a decision are uncertain, emotions play a role in guiding it. Emotions are the tools we use to simplify the world into heuristics, or general rules of thumb, as they allow our brains to take shortcuts and approximate rational thinking. Fear and anxiety detect risky choices, why we need emotions to make rational decisions.

In some cases however, our emotions guide us beyond what is rational. One such situation is our divergent assessment of risk regarding respectively gains and losses. Kahneman and Tversky found that we are risk aversive in decisions regarding gains and risk seeking in decisions regarding losses; we have a tendency to strongly prefer avoiding losses to acquiring gains.

This means that our willingness to make a decision does not have anything to do with how much we already have (i.e. our state of wealth), but rather if we are to gain or loose. So even if we have a million dollars in our bank account, we will still be as aversive towards loosing $20 as we would if we had no money.

#### Example: Discounts and trial periods
Preferring avoiding losses to acquiring gains is why discounts and trial periods work. We buy a product with a discount in fear of loosing the opportunity to buy the product at an as low price again. Furthermore, we are more likely to engage in free trial periods than paying up front, as we tell ourselves that a free product does not cost us anything.

The truth however is that we pay with our time and effort in getting used and accustomed to the product so that when the trial period is over the cost of continuing is framed as a loss instead of a gain; we will loose all the time and effort invested in the product if we don’t continue.

### Discussion

This phenomenon is referred to as sunk cost dilemma in economics. Sunk costs are costs that have already been incurred and cannot be recovered. For this reason, we shall keep from letting sunk costs influence a decision, as doing so would not be rationally assessing a decision exclusively on its own merits. In a rational world, costs already incurred have nothing to do with money to be spent in the future.

Sunk costs greatly affect our decisions, as we as humans are inherently loss aversive. Being risk and loss aversive is letting sunk costs matter. We do not like â€œwastingâ€ resources.

### Example: Lazy registration

The Lazy Registration pattern utilizes loss aversion in that it lets the user invest time and effort into a product so that when time has come to quit, the user is aversive towards loosing what has been built. The time and effort spent is sunk cost – but as we feel awkward about letting it go to waste, we register for an account in order to be able to save our work.

The assessed value of registering for an account become larger than the cost of giving away private information and opening yet another account once time and effort has been put into the product, as it is now is valued as a loss and not a gain.