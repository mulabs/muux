+++
title = "Negativity Bias"
template = "patterns.html"
[extra]
type = "loss aversion"
icontype = "fas fa-thumbs-down"
+++

### Problem summary

We have a tendency to pay more attention and give more weight to negative than positive experiences or other kinds of information.

### Usage

* Use to anticipate and accommodate bad experiences before they become a problem
* Use to frame messages to your users to optimize for positivity

### Solution

Bad is stronger than good. Negative information will attract our attention more than positive information will.

When deciding on what information is presented to users of your system, consider the fact that negative information or design elements with a negative tone will ring more attention than positive information and design will.

You can utilize this fact in your design by paying great attention to what negative feedback is presented to the user. Is it really important? Does it bring the users closer to their goal(s)? If you want users to pay attention to positive information, be careful not to let negative feedback outshine the positive.

* *Adjust your frame* Negative messages will outweigh positive ones. Consider separating positive messages from negative ones to harvest the full value of them – or even integrating multiple negative messages into one: get it all out. Be careful not to let negative feedback to users outshine the positive.
* *Turn negative messages into positive experiences* If you do need to convey a negative message to your users, consider how you can turn the negative experience into a positive one by immediately providing quick and effortless ways to solve the pain. Give a helpful hand.
* *Anticipate bad experiences* Map out the emotional rollercoaster your users are going through in their customer journey and work to find solutions to accommodate users before confusion, lack of information, or misunderstandings becomes a problem.

### Rationale

We pay more attention and give more weight to negative feedback than positive.

As we’ve seen with loss aversion, we work much harder to avoid losing $100 than we will work to gain the same amount – and painful experiences (loss) are much more memorable than pleasurable ones (gain). But whereas loss aversion refers to negative values, the negativity bias refers to negative information.

Customers buy less as a reaction to bad news, but not more as a reaction to good news. When of equal intensity, things of a more negative nature (unpleasant thoughts, emotions, or social interactions) have a greater effect on a person’s psychological state than neutral or positive things. As humans, we give priority to bad news why only few bad moments can ruin a perfect reputation.

### Discussion

The negativity bias is also the reason political smear campaigns outpull positive ones. Nastiness just makes a bigger impact on our brains. Your brain is simply built with a greater sensitivity to unpleasant news.