+++
title = "Curiosity"
template = "patterns.html"
[extra]
type = "other biases"
icontype = "fas fa-circle-info"
+++

### Problem summary

We crave more when teased with a small bit of interesting information

### Usage

* Use to drive people toward action
* Use to convert users into leads

### Solution

* *Reveal enough to arouse interest* Consider when, and what, can you hold back and reveal just enough to arouse interest so you can supply a way to take the next step.
* *Do something unexpected* People will stick around long enough to figure out what is going on.
* *Drip-feed information* Set up multiple posts that encourage engagement, such as joining an email list for a first look at a new product.
* *Allow exploration* We get pleasure from recognizing and seeking out new knowledge and information, and the subsequent joy of learning and growing.

### Rationale

Reveal just a tiny bit to arouse interest and create a knwledge gap. As humans, we are driven to seek the information missing to closes the knowledge gap. Tease users with a fragment of the whole picture and let them take action to reveal more. You can delay filling in the missing pieces for quite a long time, but be aware of the point where you start introducing too much discomfort.