+++
title = "Choice closure"
template = "patterns.html"
[extra]
type = "other biases"
icontype = "fas fa-certificate"
+++

### Problem summary

We are more satisfied with decisions when we engage in physical acts of closure

### Usage

* Use to boost satisfaction with a purchase
* Use to imply a final decision
* Use to provide confidence in a “sealed deal”
* Use to reduce choice overload

### Solution

* *Separate the buying experience for closure* Clearly separating the billing process (or area) from the rest of the shopping experience will help consumers gain closure more easily at the time of billing. Distinguishing one part of the shopping experience from the next will increase the sense of closure as customers complete each component parts.
* *Let customers seal the deal* Take away the menus once food has been ordered, have customers put purchased wine into a 6x carrier box, offer a glass of champagne to customers after purchasing a luxury handbag, or have customers pick out their own gift box. Find physical forms of closure that imply a final decision and confidence in a “sealed deal”
* *Reduce choice overload* In situations where customers feel an overwhelming number of choices, let an assistant carefully guide the customer through the decision-making process, while using a physical closure ritual to give customers enough autonomy to create the sense of closure themselves.

### Rationale

We perceive a decision to be more final when we engage in specific physical acts metaphorically associated with the concept of closure – such as closing a menu after selecting food. Create distinguishable separate experiences for closure such as having billing separate from browsing, or having customers collect and seal their product in-store. This can in turn help both increase satisfaction.