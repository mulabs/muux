+++
title = "Cashless effect"
template = "patterns.html"
[extra]
type = "other biases"
icontype = "fas fa-credit-card"
+++

### Problem summary

We spend more when no cash is involved in a transaction

### Usage

Use to increase sales
Use to combat overspending

### Solution

* *Less friction results in more sales* Make paying effortless. The less tangible a payment is, the more we tend to consume. Coins and notes, which we can see, feel, and smell, is the most transparent form of payment. Credit cards or prepaid accounts are not transparent.
* *Be ethical* Due to the Cashless Effect, customers will possibly end up spending more money than they actually have and in turn cause them to fall into debt. This puts more responsibility on you as a designer. Be ethical.
* *Combat overspending* If you want to help customers from overspending, consider creating a budget, imposing a credit limit, or increasing friction for big expenses.

### Rationale

The more effortless a sale is, the less conscious effort it requires and the more revenue it will generate. Simplicity pays off. We are more aware of the exchange of value that occurs, when we pay with cash as it is visible and tangible and similarly lose it when we pay by credit card. The Cashless Effect disregards purchase size and influences both a $1 sale and a $1000 sale.