+++
title = "Inaction Inertia Effect"
template = "patterns.html"
[extra]
type = "other biases"
icontype = "fas fa-mountain"
+++

### Problem summary

We are less likely to buy having previously missed a more attractive offer

### Usage

* Use to make sure that you don’t optimise for short term gains when making promotional offers, but also account for how customers will buy after the promotion is over
* Use to adjust your promotions to make every offer unique

### Solution

You need to consider several factors when putting your product on a discount:

* *A discount needs a reason* We tend to be unwilling to grab the same product after we miss a discount as the discount reduces the perceived value of the product and anchors our expectations at a new, lower, baseline. Avoid short-term price wars as you risk setting a new inflexible expectation regarding the too good bargain. Customers need to understand why the product is discounted and why it shortly won’t be.
* *Change appearance from offer to offer* Change product characteristics or promotional formats to avoid product price comparison by changing how the product appears or is promoted. Think in season, size, quantity, and other things that can make the offer stand out from how it’s regularly represented.
* *Reduce comparability* When two promotions are directly comparable (for instance through price alone), consumers perceive the opportunities more comparable, which results in consumers expressing higher inaction inertia.

### Rationale

When missing an appealing offer, a less attractive but still desirable deal is likely to be forfeited. As the difference between the first and second offer grows, so does the prominence of the effect. The effect can be explained by regret, as missing is seen as a loss, and as a devaluation of the later offer, as the first offer seems to serve as an anchor against the current one.