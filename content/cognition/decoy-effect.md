+++
title = "Decoy Effect"
template = "patterns.html"
[extra]
type = "loss aversion"
icontype = "fas fa-chess-pawn"
+++

### Problem summary

Create a new option that is easy to discard

### Usage

* Use to make users pick one option over another
* Use to change what parameters users focus on to make a decision
* Use to make some options (target) more attractive than a competing option by adding a decoy option.

### Solution

* *Target option should asymmetric dominate* In an ideal decoy situation, there are three choices available: Target (what you want people to choose), competitor, and decoy. To be effective, the decoy must be asymmetrically dominated by the target and the competitor. In other words, the target should rate better than the decoy on all parameters (better on both A and B), where the competitor is only better than the decoy on a few parameters (better on A, but worse on B).

* *Make choice less overwhelming* The more options we have, the more difficulty we have making a decision (paradox of choice) and the more regret we have making the “wrong choice”. By manipulating what we focus on to make a decision and providing justification for our choices, the decoy effect can help us feel less anxiety from having too many options to choose from.
* *Decoys act as a new reference point for loss* According to the theory of Loss Aversion, it is more unpleasant to loose $10 than it is pleasant to gain $10. Loss aversion causes us to direct more focus toward the disadvantages when making decision. What constitutes as a loss and a gain is not set in stone and is defined relative to some reference point why decoy options can partially function by manipulating where the reference point is, changing what is perceived as a loss and what is perceived as a gain.

### Rationale

We tend to have a specific change in preference between two options when a third option is also presented. When the third option is asymmetric between the original two options, customer preference will lean toward the asymmetry. If the original two options are $5 and $10, then a third $9 option will make choice lean toward the $10 option. The decoy option is superior to the first option, but similar to the second.

### Discussion

Decoys are a type of behavioural nudge, an unobtrusive and, to the user unconscious, type of intervention that can subconsciously alter the way we make decisions.

Decoys capitalize on a number of weaknesses of our decision making processes:

1. they make it easier to rationalize our intuitive choices,
1. they make us feel less overwhelmed by choice overload, and
1. they prey upon our dislike of losing out.