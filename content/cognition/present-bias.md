+++
title = "Present Bias"
template = "patterns.html"
[extra]
type = "other biases"
icontype = "fas fa-gift"
+++

### Problem summary

What we want now is not what we aspire to in the future

### Usage

* Use to boost sales by exploiting our bias toward instant gratification over future rewards
* Use to separate users present-self from their future-self to help them in making healthier long-term choices.

### Solution

* *Facilitate future lock-in* Help users make healthier long-term choices and secure the best long-term consequences of their decisions by segregating making a decision from receiving the consequences. Encourage customers to order groceries several days in advance to separate their present self from their future self and in turn reduce impulse purchases.
* *Optimize sales for the near-future* It is more likely that customers will spend more money in the near future than in the more distant future.
* *Boost impulse sales* Using the Present Bias to boost sales, you could adapt product recommendations to include impulse purchases through time-restricted special offers.

### Rationale

Our short-term desires (wants) and longer intentions (should) differ wildly. We tend to focus on and over-value immediate rewards at the expense of long-term wishes and benefits. Allow customers to perform “future lock-in” to help them make healthier “should” choices rather than caving in to here-and-now “wants”.