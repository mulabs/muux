+++
title = "Playthrough"
category = "onboarding"
template = "patterns.html"
[extra]
type = "guidance"
icontype = "fas fa-forward-fast"
+++

### Problem summary

The user wants to know how to use the different features of the application.

### Usage

* Use when you want to introduce the core interactions of your product before letting them enter the full product
* Use when you want to let users learn and explore the core skills needed to use the product
* Use when you want to allow modeless assistance
* Use when you want to allow users to get out into the full product world without facing horrible failure if they hadn’t gone through this
* Use when you want to let users learn by doing
* Use when you want to create a risk-free space for users to build skills and earn starting achievements
* Use when you want users to develop a set of core competencies that help users transition to advanced actions
* Use when you want to provide a positive first impression through giving a person an early success
* Do not use when there is a chance that users will feel that you are providing too much instruction or hand-holding. At least make it possible to skip.

### Solution

A dedicated and authentic space where beginners can safely explore and learn core skills.

Spare users from facing horrible failure by introducing them to core interactions. Let users learn and explore core skills before they enter the full product experience. Provide modeless text assistance and basic goals so they can play the interface but also learn the interface as they play.

A Playthrough is something that you provide, before letting someone enter a full product experience, that introduces them to the core interactions and let’s them learn and explore those core skills. This pattern is known from games, where new players are encouraged to try out a starter level, where they get modeless text assistance and some basic goals, so that they can play the game, but also learn the game, as they play.

### *Let users learn through real tasks*

By asking users to make their first move on the application, an application can get users engaged right off the bat. This is common in applications that depend on curation by the user to get the app working.

### *Continue the experience seamlessly into the full product*
Carry any achievements earned in the Playthrough forward into the full, and normal, product experience. Make the transition from the Playthrough to the normal product experience as seamless as possible.

### *Provide a skip option*
Some people just want to get into deep waters form the start. Always allow escaping a Playthrough by skipping it.

### *Don’t drag it out*
Limit the playthrough to relevant core tasks and user test your way into finding out what length is comfortable for users. Some users might like longer playthroughs than others, so be sure to allow escaping the experience, but still letting users keep their achieved artifacts.

### Rationale

By allowing users to get out into the full product world early and safely, you can keep users from facing horrible failure if they hadn’t had an introduction.