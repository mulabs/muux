+++
title = "Lazy registration"
category = "onboarding"
template = "patterns.html"
[extra]
type = "registration"
icontype = "fas fa-thumbtack"
+++

### Problem summary

The user wants to immediately use you and try your website without conducting a formal registration beforehand.

### Usage

* Use when it is critical for your website to let the user browse around and interact with your website before actually registering an account
* Use when giving away the personal information required for a formal account registration is a big step for your users
* Use when you want to allow your users to try out your website (and in turn compare it to alternatives) before making the decision to register an account with your website
* Use when registering an account requires an exchange of money, why the user might want to “browse around” your product first in order to make a decision.
* Use when you don’t want to force your users to register at first – when you don’t want to put too much pressure on your users
* Use to make your users start entering data into your system right away. When they have already invested time in your system, the step to registering an account is smaller than if no time has been invested.

### Solution
Allow users to access a limited set of features, functionality, or content before or without registrering.

Let users interact enough with your system so that an actual registration is just another small step in a larger process: a small step, not an obligation.

The light version of this pattern is the shopping cart of an e-commerce site, where the user can accumulate relevant products in a cart and then in turn register an account if he or she chooses to make a purchase.

In the heavier version of this pattern, an anonymous user account is immediately created for the user – full with an auto-generated database ID and a complimenting cookie with the account’s ID that will ensure that the user’s details and the information he or she has entered will be remembered upon the next visit. With appropriate intervals, inactive anonymous accounts are cleared from the database in order to not clutter it up.

As the user interacts with the system, data is accumulated to the account. While some data might not be shown to the user other kinds of data will. It is the latter kind of data that in turn will make the user register – the visible evidence that the user has invested energy into using the system. A smart way to gather such data is to expose holes of data that the user can populate.

Two such holes are the username and password: the two bits of information that will allow the user to log into his account from more than one computer.

### Rationale
Deliver value before prompting for conversion. Shopping carts are a classic pattern that plays on Lazy Registration: users can browse and choose products, but only have to register when they proceed to check out.x

The Lazy Registration pattern allow users to use your system and take action before or without registrering. The idea is to let users interact enough with your system so that the actual registration is just another small step in the larger process. It’s a small step – not an obligation. The classic Shopping Cart pattern is a good example of Lazy Registration: users can browse and choose products and only have to register when they proceed to check out.

For this pattern to work, you need to provide the users with an incentive to give you the registration data you are looking for. You need to provide a worthwhile service to your users for them to give you their data back in return. You want to use classic Carrot and stick motivation – and just as important: communicate the benefit you are providing. If the registration data you are looking for with the user is sensitive, you must be able to assure your users that their data will be safe and secure.

### Discussion
Questions to ask when planning your Lazy Registration

* Consider how you can avoid sign up forms in favor of a more gradual engagement.
* If you choose to use the Lazy Registration pattern, make sure that you focus on showing off how you deliver value: how potential customers can use your service and why they should care.
* Auto-generated accounts are great, but if you choose to use them, then make sure to design a clear way for users to access their accounts. Chances are that people will either ignore or not see your account creation mails – and might be uncertain if they even have an account or not.