+++
title = "Guided Tour"
category = "onboarding"
template = "patterns.html"
[extra]
type = "guidance"
icontype = "fas fa-person-walking"
+++

### Problem summary

The user wants to learn about new or unfamiliar interface features.

### Usage

* Use when you want to help users along getting accustomed to your user interface.
* Use when you want to notify your users of new or unfamiliar features
* Use when your user interface is not self-explanatory

### Solution

“Just-in-time” guidance is triggered as the user explores.

Allow users to learn at their own pace using tooltips, overlays, models, and alerts hinting optimal use of an interface within the context of everyday use. Connect hints with clear completion states. Too many and too plain hints lead to frustration. Don’t overdo it and be sure to allow escape.

Create a series of individual hints that progressively appear during the first-time use of a product or interface. Hints can be anything from tooltips, overlays, to modal alerts.

Some users appreciate the help and others don’t. Be sure to always allow escape; to dismiss your guided tour.

### *Product-guided vs User-guided*

Decide whether you want to let the tour be product guided or user-guided. In a product-guided tour, hints are automatically progressed in linear succession, while hints in a user-guided tour are triggered as the user reaches appropriate points in their experience. For user-guided tours, hints may appear in different orders for different users.