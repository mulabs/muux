+++
title = "Paywall"
category = "onboarding"
template = "patterns.html"
[extra]
type = "registration"
icontype = "fas fa-ticket"
+++

### Problem summary

The user needs to pay to get access to a restricted area on a website.

### Usage

* Use when you need a way of directly making money on the content you produce
* Use when your users are willing to pay for the content you produce
* Use when the perceived value of your content is greater than the amount of money you are requesting for it
* Do not use when your website is solely dependent on making money on banner impressions, as the highest possible number of pageviews is then the most important.

### Solution

Restrict access to users who have paid.

There are a variety of Paywall strategies found in use today. vary. Some strategies close block all content until payment is made, some lure you in with bait and asks for money after viewing an amount of articles, other websites cherry pick content that’s not free for all. Below is an overview of the main variations.

### *Strategies*

* Paywall: All content is behind one big paywall that surrounds the entire site.
* Freemium: In the freemium model, some content is free for all and some are behind a pay wall.
* Taxometer: The first few articles are free to view whereafter the paywall kicks in.
* Time limits: You buy a day, week, month or year pass or access to the archive for a number of days.
* Bulk sales: Upsale and sale to companies.Sale by the piece: Purchase of single stories and services.


### *Payment doesn’t have to be monetary*
Most paywalls include a monetary exchange, however there are several ways a user can pay to get behind a paywall:

* *Monetary exchange*: The traditional paywall includes some kind of monetary exchange – it can payment for a single piece of information or by subscription
* *Subscription to print media*: Instead of buying a subscription to use the website only, require users to own a subscription of the print edition of the media in order to enforce both revenue streams: online and offline.
* *Permission*: Let your users give you permission to call them up, send them emails, to get your information from facebook, to contact your friends, etc.. Getting permission to build a long term relationship can sometimes be more worth than a simple monetary exchange.
* *Lead*: You could also let users give their permission for an advertiser or other third party to contact them.
* *Time*: Let your users take a questionnaire – do you want to know about the profile of your users and their behaviour – or could an advertiser or other third party be interested in knowing about your users? You could also let your users watch a video commercial from an advertiser or otherwise grow from their valuable time.
* *Social sharing*: Let users gain admission to content by first socially sharing a link. Some websites and applications use “pay with tweet” and “pay with like” services.


### Rationale

Paywalls are used as an alternative income source for online media websites, where banner advertising has been the tradition. As users move their habits from print to online, media companies find it harder to base their business on advertising revenue only. Paywalls has been widely introduced to make up for the lost revenue.