+++
title = "Breadcrumbs"
category = "navigate"
template = "patterns.html"
[extra]
type = "hierarchy"
icontype = "fas fa-bread-slice"
+++

### Problem summary
The user needs to know their location in the website's hierarchical structure in order to possibly browse back to a higher level in the hierarchy.

### Usage
Use when the structure of the website follows a strict hierarchical structure of similar formatted content.
Use when the structure of the site is partitioned in to sections which can be divided into more subsections and so on.
Use when the user is most likely to have landed on the page from an external source (another site deep linking to the web page in question). For instance from a blog or a search engine.
Use when the page in question is placed fairly deep into the hierarchy of pages and when no other form of visual navigation can show the details of the same deep level.
Use together with some sort of main navigation.
Do not use on the topmost level of the hierarchy (typically the welcome page)
Do not use alone as the main navigation of the website.

### Solution
Reveal the user’s hierarchical location and provide links to higher levels.

Show the labels of the sections in the hierarchical path that lead to the current page being viewed.
Each label is a link to a section.
The label of the current page is at the end of the breadcrumb and is not linked.
Each label is separated with a special character. Popular characters are » (&raquo;) or >.
The separating characters and the spaces between the links and the labels are not linked.
The labels of each section preferably match the titles of that section.

### Rationale

Breadcrumbs serve as an effective visual aid, indicating the location of the user within the website’s hierarchy, making them a great source of contextual information for landing pages. Also, breadcrumbs allow for easy navigation to higher-level pages.

Breadcrumbs inform users of their location in relation to the entire sites hierarchy.
The structure of the website is more easily understood when it is laid out in a breadcrumb than if it is put into a menu.
Breadcrumbs take up minimal space and even though not all users use them, they still hint the structure of the website and the current location of the page in question.
The term ‘breadcrumb’ is deceptive, as it implies the history of how the user got to that page. A more correct term would describe the current location’s place in the hierarchy of the website.