+++
title = "Home Link"
category = "navigate"
template = "patterns.html"
[extra]
type = "hierarchy"
icontype = "fas fa-house-user"
+++

### Problem summary
The user needs to go back to a safe start location of the site.

### Usage

* Use when users frequently enter the website through a page other than the home page. The user needs to be able to easily navigate to the starting point or front page of the website.

### Solution
Create a link to the starting point or front page of the website on the site’s logo on every single page on the website.
If the site does not have a logo, then create a link to the front page of the website with the text ‘Home’.
The link and/or linked images should always be in the same location on all pages.
If the website has more than one home, then be sure to make the distinction in linking between the root home and the local home.

### Rationale

It has become a standard in webdesign, that the site’s logo is always linked to a safe start location for the user. Normally, this is the front page of the site, but it could also be the front page of a section in the site, or some other safe start location for the user.