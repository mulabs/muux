+++
title = "Navigate Tabs"
category = "handle-data"
template = "patterns.html"
[extra]
type = "tabs"
icontype = "fas fa-ellipsis"
+++

### Problem summary
Content needs to be separated into sections and accessed using a flat navigation structure that gives a clear indication of current location.


### Usage
* Use when there are between 2 – 9 sections of content that need a flat navigation mode.
* Use when section names are relatively short
* Use when you want the navigation to fill the entire width of a page
* Use when you want to provide a list of the highest available sections/subsections of the website
* Do not use when wanting to show content-specific data. For instance for showing latest articles
* Do not use when there is no need to single-out the currently selected option
* Do not use when the list of sections or categories call for a “more…” link. Then consider another navigation pattern.

### Solution
A horizontal bar contains the different sections or categories of your website.
Each section or category is represented by a tab that most commonly resembles a button. This is why the whole button should be clickable, and not just the text that labels the section.
Optionally, a bar below the top bar can contain subsections of the currently selected item in the top bar
The navigation tab is persistent across all pages that the tabs link to.
The same structure (order) of the navigation tabs should be maintained from page to page, so that the user can relate the navigation of the different pages to each other.
The selected tab should be highlighted to indicate current location.
If subsections are used (a second horizontal bar below the top bar) there should be a clear visual connection between the currently selected top tab and the bar below showing subsections.

### Rationale
The Navigation tabs pattern is an extension of the desktop metaphor in which physical objects are represented as GUI elements. Navigation tabs are derived from the idea of folders in a file-cabinet and are thus familiar to the end user

Navigation tabs provide a clear visual indication of what content can be found on a website and places the current location in context by highlighting it.