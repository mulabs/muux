+++
title = "Modal"
category = "navigate"
template = "patterns.html"
[extra]
type = "hierarchy"
icontype = "fas fa-layer-group"
+++

### Problem summary
The user needs to take an action or cancel the overlay until he can continue interacting with the original page

### Usage
* Use when you want to interrupt a user’s current task to catch the user’s full attention to something more important.
* Do not use to show error, success, or warning messages

### Solution
Introduce a mode in which users cannot interact with your application until the mode is closed. Interrupt the user’s attention and halt all other actions until a message is dealt with or dismissed.

#### Matching titles
Matching button text with the title of the modal increases the feeling of familiarity. As modals introduce a new inturrupting mode, chances are that users won’t connect the action they just performed with the modal popping up. Make sure they know where the modal is coming from.

#### Allow escape
Allow users to escape the mode by letting them close the modal window when they need to. Popular conventions for close buttons is an ‘X’ icon in the top right corner and/or a ‘Close’ or ‘Cancel’ button at the bottom of the modal window. The ESC key is also often a conventional keyboard shortcut to closing modals – so is clicking outside the modal window.

### Rationale
Although effective in focusing attention, introducing multiple modes comes with the risk of introducing mode errors where the user forgets the state of the interface and tries to perform actions appropriate to a different mode.

### Discussion
Modals have been considered a UI anti-pattern. The main reason lies in the definition of a modal window: A window that prevents the user from interacting with your application until she closes the window1. Modal windows interrupt users and force them into doing a specific action. Arguably, in most cases, there is no need to force users into specific actions. As a result, modal windows introduce unnecessary pain points for your users.

The alternative is modeless interfaces. Interfaces that allow users to change their mind at any point. Interfaces that do not force users into a specific set of actions.

Also, modals do not work well on mobile devices as they take up screen space and introduce additional user interface elements.