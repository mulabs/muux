+++
title="Continuous Scrolling"
category = "navigate"
template = "patterns.html"
[extra]
type = "content"
icontype = "fas fa-scroll"
+++


### Problem summary

The user needs to view a subset of data that is not easily displayed on a single page. Content needs to be presented to users as a subset of a much larger seemingly endless set, in a way that will aid them in consuming content without effort.

### Usage

* Use when there are more data to show than what would fit on a normal page
* Use when navigating to a second page of data takes away too much attention from the content

### Solution

Automatically load the next set or page of content as the user reaches the bottom of the current page

In contrast to the Pagination patterns, the Continuous Scrolling pattern has no natural break. When using pagination patterns, a decision to only show a subset of data at a time and then let the user request more data if wanted is chosen. With the Continuous Scrolling, new data is automatically retrieved as the user has scrolled to the bottom of the page. It thus appears as if the page has no end, as more data will be loaded and inserted into the page each time the user scrolls to the bottom of page.

### Rationale

Eliminate the need for clicking “next page” by creating the effect of an infinitely scrolling page by constantly loading in new content as the user scrolls to the bottom of a page. Though great for the user experience, this pattern introduces bookmarking issues.

The problem with using pagination for browsing between subsets of data is that the user is pulled from the world of content to the world of navigation, as the user is required to click to the next page. The user is then no longer thinking about what they are reading, but about how to get more to read. This breaks the user’s train of thought and forces them to stop reading. Using pagination creates a natural pause that lets the user reevaluate if he or she wants to keep going on or leave the site, which they a lot of the time do.

It can be argued that Continuous Scrolling can be frustrating for the user, as there is no natural pause. The user will ask himself: When am I done reading?