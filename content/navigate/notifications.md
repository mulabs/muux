+++
title = "Notifications"
category = "navigate"
template = "patterns.html"
[extra]
type = "hierarchy"
icontype = "fas fa-bell"
+++

### Problem summary
The user wants to be informed about important updates and messages

### Usage

* Use when you want to draw attention to important updates or messages.
* Do notify users about about time-sensitive information directed specifically at them.
* Do not notify users about information that is currently on screen (e.g. active chat conversations)
* Do not notify users about technical operations that do not require user involvement (such as syncing)
* Do not notify users about error messages that can be resolved without user action

### Solution
Inform your users about relevant and timely events.

Notify users about important updates while they are focused elsewhere. Adjust the rate and relevancy of notifications aptly, as they can be interruptive. Empower users to disable or change notifications in their settings. Create personalized, summarized, and timely notifications that may serve as entry points to more detailed information.

Use notifications to draw attention to important updates: messages from friends, new friend requests, relevant nearby offers, and many more.

#### Across devices
Once a users has consumed a notification, he or she should not see it again. Similarly, users should be able to retrieve already consumed notifications on another device more suitable for consuming the content the user was notified about. Notifications should be synced to all of a user’s devices.

#### Minimize interruption
Notifications are obtrusive and interruptive in its nature. It is used to direct the user’s attention to important events while being focused elsewhere. Make careful considerations as to when to interrupt users. Do not notify users about information already on screen (e.g. active chat conversations), technical operations not requiring user involvement, and error states that can be resolved without user action.

#### Allow escape
Make notifications dismissible and let users disable or change the rate of notifications in your products settings.

#### Provide summaries
Combine multiple notifications of the same type into a single summary notification showing how many notifications of a particular kind are pending. Consider expanding the notification, providing detailed information of the summarized notifications, once clicked.

#### Provide actions
Bundle action buttons with notifications, for users to quickly handle the most common tasks for a particular notification, without opening the originating screen. Let actions be clear and unambiguous and only provide them if they do not duplicate the default action. Actions should be meaningful and time-sensitive, suit the content, and allow the user to accomplish a task.