+++
title = "Module tabs"
category = "navigate"
template = "patterns.html"
[extra]
type = "tabs"
icontype = "fas fa-table-columns"
+++

### Problem summary
Content needs to be separated into sections and accessed via a single content area using a flat navigation structure that does not refresh the page when selected.

### Usage
* Use when there is limited visual space and content needs to be separated into sections
* Use when there are between 2 – 9 sections of content that need a flat navigation mode.
* Use when you need to keep user attention by circumventing page refreshing.
* Use when section names are relatively short
* Use when the content of each tab can be viewed separate from each other, and not in context of each other.
* Use when the content for each tab has similar structure
* Use when you need to show what tab is currently being viewed
* Do not use when the content inside each pane would function just as well in its own separate page.

### Solution
* Present the content of one tab inside a box (content area)
* Place a horizontal bar on top of the content area with links representing tabs
* Refrain from having more than one line of links in the top horizontal tab bar
* Use color coding or other visual support to indicate what tab is currently being viewed
* Present the content of each tab in the same content area
* Only one content area should be visible at a time
* Maintain the same structure of the top horizontal tab bar after a new tab has been clicked
* Only the content area of the tabs and the horizontal tab bar should be changed when a user clicks a new tab
* If possible, the page is not refreshed when a tab is clicked.
* A new page is not loaded when a tab is clicked

### Rationale

The Navigation tabs pattern is an extension of the desktop metaphor in which physical objects are represented as GUI elements. Navigation tabs are derived from the idea of folders in a file-cabinet and are thus familiar to the end user

Module Tabs provide an easy way to show large amounts of similar structured data parted by categories

Tabs create a context for content, when a tab is selected the relevant content is loaded inside the content area.