+++
title = "Vertical Dropdown Menu"
category = "navigate"
template = "patterns.html"
[extra]
icontype = "fas fa-grip-vertical"
+++


### Problem summary
The user needs to navigate among sections of a website, but space to show such navigation is limited.


### Usage
Use when there are between 2 – 9 sections of content that need a hierarchical navigation structure.
Use when your functionality resembles one of a desktop application. Imitate the metaphor.
Do not use when there is a need to single out the location of the current section of the site. Then use the Navigation Tabs

### Solution
A list of main sections is listed on the same horizontal line. Once the user has his mouse cursor over one of the list items, a drop-down list of new options is shown below the list item the mouse cursor is pointing at. The user can then follow the now vertically extended list item down, to select the menu item he wants to click.

Once the user removes the cursor from the box of drop-down’ed options, the box disappears. He can then put his mouse cursor over another list item, whereafter the process starts over.

As humans, we do not always act perfectly as the system would like us to. To cope with human errors and to guide us to act as you would like us to, you can implement the following:

On mouseout events (when the user takes his mouse away from the drop-down’ed box), add a delay before hiding the drop-down’ed box (typically 200-300 ms.)
Make the area of each menu item wider than just the text of the menu item so that the user has more space to put his mouse cursor over.
Change the cursor image as the user hovers over a list item.
Other issues you want to take notice of:
There are many different kind of drop-down menus out there. Some work only – and is built purely with javascript. These kinds of drop-down menus do not work well with search engines. To let the search engines index your page, you would want to have the menu formatted in HTML from the beginning of the page load, rather than building it in javascipt client-side after the page has loaded.

### Rationale

Drop-down menus save space. This is the main reason for using them. Otherwise, drop-down menus are not regarded as a technique that increases usability, as they can often be difficult to use.

Flyout menus allow for only showing top levels of the page’s hierarchy permanently, while still giving the option to show deeper levels on mouse over.