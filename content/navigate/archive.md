+++
title = "Archive"
category = "navigate"
template = "patterns.html"
[extra]
type = "content"
icontype = "fas fa-file-zipper"
+++

### Problem summary

All the items in a collection need to be organized in a chronological order.

### Usage

* Use when you have more than 10 to 20 items to display
* Use when you have a data set that spans over a long time frame and want to let the user browse the items in the dataset by chronological order.
* Use when displaying all items in a dataset confuses the user rather than giving an overview
* Use when it makes sense to order items in a dataset by dates
* Use when you want to provide an easy way to browse your entire database of articles.

### Solution

ist the items in your dataset in chronological order and provide suitable headlines to match the amount of items. If you for instance have 10 items per year, it does not make much sense to partition these 10 items into months. If you have 100 items a year, but also have months without any items, it might not make sense to list all months.

Either you can provide links to pages that shows all items per time period, or simply make a list of links to each item directly on the main archive page.

### Rationale

Use the archive pattern when it makes sense to list items in chronological order. Listing items in an archive format, makes it easy for the user to explore how a website has evolved over time and what has influenced the most current items.