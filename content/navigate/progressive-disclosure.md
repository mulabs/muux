+++
title = "Progressive Disclosure"
category = "navigate"
template = "patterns.html"
[extra]
type = "content"
icontype = "fas fa-battery-half"
+++

### Problem summary

The user wants to focus on the task at hand with as few distractions as possible while still being able to dig deeper in details if necessary.

### Usage

* Use when you want to reduce feelings of being overwhelmed

### Solution

Present only the minimum data required for the task at hand.

Move complex and less frequently used options out of the main interface. Reveal only essential information and help manage complexity by disclosing information and options progressively.

Examples of Progressive Disclosure are plentiful. A simple “Show more” link, revealing more information about something, is one of the simplest forms of Progressive Disclosure.

### Rationale

Maintain the focus and attention of users by reducing clutter, confusion, and cognitive workload. Ramp up the experience, moving from simple to complex, from abstract to specific. Progressive Disclosure defers advanced or rarely used features to a secondary screen, reducing cognitive load on the current task at hand. This will help making your application easier to learn and less error-prone due to fewer distractions.

By showing only the information or features relevant to the user’s current activity and delaying other information until it is requested, the user can focus on the main task at hand. By hiding more complex or infrequently used, the interface is de-cluttered; by revealing them only as they are needed, you help users perform a complex, multi-step process on a single page2.

You want to show only essential information in the first step, but still invite to take the next. When a user completes a step, reveal information needed for the next step, keeping all previous steps visible. By keeping previous steps visible, you allow users to change what has been entered. Like in a Wizard, data entered in the current step can affect the behaviour of the next.