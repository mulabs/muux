+++
title = "Pagination"
category = "navigate"
template = "patterns.html"
[extra]
type = "content"
icontype = "fas fa-border-top-left"
+++

### Problem summary

The user needs to view a subset of sorted data in a comprehensible form.

### Usage

* Use when it is unsuitable to display all the data on a single page/screen.
* Use when the dataset is in some way ordered.
* Do not use when you don’t want the user to pause for navigating to the next page.

### Solution
Break a complete dataset into smaller sequential parts and provide separate links to each.

Provide pagination control to browse from page to page. Let the user browse to the previous and next pages by providing links to such actions. Also, provide links to the absolute start and end of the dataset (first and last).

If the dataset has a known size then show a link to the last page. If the dataset’s size is variable then do not show a link to the last page.

### Rationale

Reduce perceived complexity by parting large datasets into smaller chunks that are more manageable for the user. Significant technical performance can be achieved by only having to return subsets of the overall data.

First and foremost, pagination parts large datasets into smaller bits that are manageable for the user to read and cope with. Secondly, pagination controls conveys information to the user about, how big the dataset is, and how much is left to read or view and how much have they already viewed.

Pagination provides the user with a natural break from reading or scanning the contents of the dataset, and allows them to re-evaluate whether they wish to continue looking through more data, or navigate away from the page. This is also why pagination controls are most often placed below the list: to provide the user with an option to continue reading through the larger dataset.