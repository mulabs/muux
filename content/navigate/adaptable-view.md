+++
title = "Adaptable View"
category = "navigate"
template = "patterns.html"
[extra]
type = "content"
icontype = "fas fa-maximize "
+++

### Problem summary

You want to let the site's presentation of content fit the specific needs of the user.

### Usage

* Use when a considerable part portion of your potential users have specific technical needs concerning how content is presented. Examples are mobile browsers, small screen resolutions, and monochrome monitors.
* Use when a considerable part of your potential users have specific needs to regarding accessibility and how content is presented due to disability. Examples are colour-blindness or poor vision.
* Use when your users need to control font size but may not know how to use the browser’s built in font resizing settings.
* Use when you want to give users the ability to switch between from a mobile version of a site to the full featured version. It is for instance not all iPhone users who actually like to use tailored iPhone versions of websites instead of the full-featured browser version.

### Solution
Provide a mechanism to switch or alter the default style of a page so that it fits the specific needs of the user.

When catering to alternative browsers such as mobile phone browsers, the view to present can often be found looking at the incoming user agent. In this case, a manual mechanism to switch styles might seem obsolete, but it is good practice to allow access to all views of a site regardless of how the user is browsing it.

Provide a manual control to allow users to switch/alter the default style of a page so that it better fits their specific needs. It is for instance not all iPhone users who actually like to use tailored iPhone versions of websites instead of the full-featured browser version.

It is a good practice to allow for permanence of the user’s preferred configuration. This will prevent the user from having to make the same adjustment each time a page reloads.

### Rationale
By providing a mechanism to present different views of content to the user, you can tailor usability and the experience you want to give your users to their specific needs.

### Discussion
At first it may seem that a style adjuster is a superfluous feature that falls one step short of showing off. After all, don’t users already have control over the presentation of content through the means of user stylesheets and the browser’s built in font resizing? Well, just because a user has the ability to use these tools, doesn’t mean that they have the knowledge or willingness to get their hands dirty with them.

Enter the on page adjustable style control. It can give a web designer the ability to extend a browser’s accessibility support and provide them in a much more convenient way.

Beyond accessibility concerns, style adjusters can also cure some of the common annoyances that almost everyone deals with. One example of this is when a site forces a user to use the mobile version. A simple button that switches the site to the full featured version is enough to alleviate the feeling of being trapped.