+++
title = "Accordion Menu"
category = "navigate"
template = "patterns.html"
[extra]
type = "menus"
icontype = "fas fa-folder-tree"
+++

### Problem summary
User needs to navigate among a website's main sections while still being able to quickly browse to the subsection of another.

### Usage
Use when you want the benefits of a normal sidebar menu, but do not have the space to list all options.
Use when there are more than 2 main sections on a website each with 2 or more subsections.
Use when you have less than 10 main sections
Use when you only have two levels to show in the main navigation.

### Solution

Each headline / section has a panel, which upon clicking can be expanded either vertically or horizontally into showing its subsections. Vertical Accordion menus are the most frequently used.
The transition from showing no options of a headline to showing a headline’s list of options can be done either with a page refresh or with a javascript DHTML animation.
When one panel is clicked it is expanded, while other panels are collapsed.

### Rationale

Accordion menus are often used as a website’s main navigation. In this way, it acts much like Navigation Tabs, as menu items are collapsed when a new panel is clicked. Where the Navigation Tabs are most often used horizontally, Accordion menus are most often used vertically.

Accordion menus can however also function quite well as sub-navigation for a specific section of a website.