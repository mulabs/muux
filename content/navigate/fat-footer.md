+++
title = "Fat Footer"
category = "navigate"
template = "patterns.html"
[extra]
type = "hierarchy"
icontype = "fas fa-sitemap"
+++

### Problem summary

Users need a mechanism that will enable them to quickly access specific sections of a site or application bypassing the navigational structure.

### Usage

* Use to shortcut an otherwise hierarchical structure of a website.
* Use when there are specific pages or functions that are more frequently used than others parts of the website. Use the shortcut box to show these choices in order to shorten the path for the users.
* Use when you want shortcuts to pages that are possibly on different hierarchical levels of the page.
* Can also be used as navigation when short on space, although it is not advised.

### Solution
End a page by providing relevant links to other sections of your site.

Add the same footer on all pages of a website – with the same layout in the footer on all pages. Typically, these things are included in fat footer designs:

* About us link: Link to your “about us” section, which then includes basic information about your company.
* Terms of service: If you provide a service or a product, placing a link to it in the footer is a standard location, why users anticipate it being there.
* Privacy policy: As with the terms of service, users expect to always be able to find privacy policies in the footer of a website.
* Site map: Provide quick links the most important pages of your website.
* Contact us link: Make sure your users have a way to get in hold of you. If you have a “Contact us” page, users expect to find a link to it in the footer. This is also a critical point in building the trustworthiness of your site as it is displaying your physical address.
* Address and phone number: Show that you are real. If you have a physical business, offer phone support, or have a reason for people to mail you things, putting that information in the footer is an anticipated and appropriate location.
* Social links: Link to any social presences you might have on Facebook, Twitter, Pinterest, Instagram, and the likes.

### Rationale
Keep visitors on your site for longer: end one experience by starting a new one. Provide easy and natural ways for users to continue their journey. By adding a shortcut to the most frequently used pages and functions, the path can be shortened and confusion can be decreased.

The hierarchical structure of a website can at times impede the path to specific page or function of a website. By adding a shortcut to the most frequently used pages and functions, the path can be shortened: the number of clicks can be lessened and the confusion decreased.

### Discussion
A fat footer isn’t right for every site or for every company. Consider asking these when creating a footer:

* What does my user expect to see here?
* What is the next logical step in the user journey?
* What questions are my secondary or tertiary users still asking at the bottom of the site?
* How can I continue the experience beyond the content on this page?