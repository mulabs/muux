+++
title = "Tagging"
category = "navigate"
template = "patterns.html"
[extra]
type = "content"
icontype = "fas fa-tags"
+++

### Problem summary
Items need to be labelled, categorized, and organized using keywords that describe them.

### Usage

Use when the content on your website is possibly mapped into multiple categories and does not necessarily only fit into one hierarchical category.
Use when you want users to contribute data to your website and let them organize their contributed data themselves.

### Solution
Let users associate multiple topics with a piece of content. Allow users to add appropriate keywords to categorize their own content in a non-hierarchical way. Let users use hashtags to integrate tagging into the content itself.

Allow keywords to be associated with items on a website/application such as blog articles, ecommerce products and media. Use terms that categorically describe these items. Permit these items to be found in a search using these keywords. Let contributors of information add keywords to the content they submit. Keywords can be displayed as links that aid in finding items with matching keywords.

### Rationale
Tagging helps make it easier for users to find their own content and for their peers to discover content related to their interests.

Tags are relevant keywords associated with or assigned to a piece of information. Tags are often used on social websites, where users can upload their own content. Here, tags are used to let users organize and categorize their own data in the public sphere. In this way, tags can be seen as a bottom-up categorization of data rather than a top-down categorization of data, where the creators of the site define the hierarchy data is submitted to.