+++
title = "Laws of UX"
description = ""
template = "gallery.html"

[extra]
category = "laws"
type = ""
assets = ['aesthetic-usability-effect','millers-law',
'doherty-threshold','occams-razor']
+++
