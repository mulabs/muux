+++
title = "Aesthetic Usability Effect"
description = "Users often perceive aesthetically pleasing design as design that’s more usable."
template = "laws.html"
[extra]
category = "laws"
type = "heuristic"
icontype = "fas fa-camera"
+++

