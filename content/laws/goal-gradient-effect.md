+++
title = "Goal-Gradient Effect"
description = "The tendency to approach a goal increases with proximity to the goal."
template = "laws.html"
[extra]
category = "laws"
type = "heuristic"
icontype = "fas fa-camera"
+++