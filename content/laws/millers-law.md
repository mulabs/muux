+++
title = "Miller's Law"
description = "The average person can only keep 7 (plus or minus 2) items in their working memory."
template = "laws.html"
[extra]
category = "laws"
type = "heuristic"
icontype = "fas fa-camera"
+++