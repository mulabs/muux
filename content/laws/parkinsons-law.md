+++
title = "Parkinson's Law"
description = "Any task will inflate until all of the available time is spent."
template = "laws.html"
[extra]
category = "laws"
type = "heuristic"
icontype = "fas fa-camera"
+++