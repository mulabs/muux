+++
title = "Serial Position Effect"
description = "Users have a propensity to best remember the first and last items in a series."
template = "laws.html"
[extra]
category = "laws"
type = "cognitive-bias"
icontype = "fas fa-camera"
+++