+++
title = "Hick's Law"
description = "The time it takes to make a decision increases with the number and complexity of choices."
template = "laws.html"
[extra]
category = "laws"
type = "heuristic"
icontype = "fas fa-camera"
+++