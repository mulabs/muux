+++
title = "Law of Common Region"
description = "Elements tend to be perceived into groups if they are sharing an area with a clearly defined boundary."
template = "laws.html"
[extra]
category = "laws"
type = "gestalt"
icontype = "fas fa-camera"
+++