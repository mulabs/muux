+++
title = "Von Restorff Effect"
description = "The Von Restorff effect, also known as The Isolation Effect, predicts that when multiple similar objects are present, the one that differs from the rest is most likely to be remembered."
template = "laws.html"
[extra]
category = "laws"
type = "cognitive-bias"
icontype = "fas fa-camera"
+++