+++
title = "Law of Uniform Connectedness"
description = "Elements that are visually connected are perceived as more related than elements with no connection"
template = "laws.html"
[extra]
category = "laws"
type = "gestalt"
icontype = "fas fa-camera"
+++