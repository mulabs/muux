+++
title = "Zeigarnik Effect"
description = "People remember uncompleted or interrupted tasks better than completed tasks."
template = "laws.html"
[extra]
category = "laws"
type = "cognitive-bias"
icontype = "fas fa-camera"
+++