+++
title = "Doherty Threshold"
description = "Productivity soars when a computer and its users interact at a pace (<400ms) that ensures that neither has to wait on the other."
template = "laws.html"
[extra]
category = "laws"
type = "principle"
icontype = "fas fa-camera"
+++