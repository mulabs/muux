+++
title = "Law of Similarity"
description = "The human eye tends to perceive similar elements in a design as a complete picture, shape, or group, even if those elements are separated."
template = "laws.html"
[extra]
category = "laws"
type = "gestalt"
icontype = "fas fa-camera"
+++