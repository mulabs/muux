+++
title = "Postel's Law"
description = "Be liberal in what you accept, and conservative in what you send."
template = "laws.html"
[extra]
category = "laws"
type = "principle"
icontype = "fas fa-camera"
+++