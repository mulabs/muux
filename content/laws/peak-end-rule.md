+++
title = "Peak End Rule"
description = "People judge an experience largely based on how they felt at its peak and at its end, rather than the total sum or average of every moment of the experience."
template = "laws.html"
[extra]
category = "laws"
type = "cognitive-bias"
icontype = "fas fa-camera"
+++