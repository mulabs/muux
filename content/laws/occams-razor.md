+++
title = "Occam's Razor"
description = "Among competing hypotheses that predict equally well, the one with the fewest assumptions should be selected."
template = "laws.html"
[extra]
category = "laws"
type = "principle"
icontype = "fas fa-camera"
+++