+++
title = "Law of Prägnanz"
description = "People will perceive and interpret ambiguous or complex images as the simplest form possible, because it is the interpretation that requires the least cognitive effort of us"
template = "laws.html"
[extra]
category = "laws"
type = "gestalt"
icontype = "fas fa-camera"
+++