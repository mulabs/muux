+++
title = "Tesler's Law"
description = "Tesler's Law, also known as The Law of Conservation of Complexity, states that for any system there is a certain amount of complexity which cannot be reduced."
template = "laws.html"
[extra]
category = "laws"
type = "principle"
icontype = "fas fa-camera"
+++