+++
title = "Pareto Principle"
description = "The Pareto principle states that, for many events, roughly 80% of the effects come from 20% of the causes."
template = "laws.html"
[extra]
category = "laws"
type = "principle"
icontype = "fas fa-camera"
+++