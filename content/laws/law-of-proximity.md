+++
title = "Law of Proximity"
description = "Objects that are near, or proximate to each other, tend to be grouped together."
template = "laws.html"
[extra]
category = "laws"
type = "gestalt"
icontype = "fas fa-camera"
+++