+++
title = "Jakob's Law"
description = "Users spend most of their time on other sites. This means that users prefer your site to work the same way as all the other sites they already know."
template = "laws.html"
[extra]
category = "laws"
type = "heuristic"
icontype = "fas fa-camera"
+++